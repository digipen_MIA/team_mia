/******************************************************************************/
/*!
\file   Button.h
\project ColorLeon
\author primary : Kim Minsuk

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "CustomBaseObject.h"

enum class ButtonType { DRAG_DROP, CLICK };
enum class BlockType { STATIC, DYNAMIC };
enum class ObjectType { PLATFORM = 1, PLAYER, GRAV, DANGER, GOAL, NONE };

class Button : public CustomBaseObject
{
public:
    ~Button() override;
    State * world;
    ButtonType buttontype = ButtonType::CLICK;
    BlockType blocktype = BlockType::STATIC;
    ObjectType objecttype = ObjectType::NONE;
    void(*ClickEvent)(void *);
    void * Param;
    bool IsDrag = false;
    bool Life = true;
    Camera * camera = NULL;
    b2Vec2 local_mousePos = b2Vec2(0, 0);
    b2Vec2 MousePos = b2Vec2(0, 0);
    SDL_Color Color;

    void SetColor(SDL_Color colour);
    void SetWorld(State * world_param, Camera * temp_camera);
    void Update(float dt) override;
    void UpdateColor();
    bool IsOnMouse();
    void SetType(ButtonType type);
    void SetObjectType(ObjectType type);
    void SetClickEvent(void(*event)(void*), void * param = NULL);

    float * DragAndDropLimit = NULL;
    b2Vec2 * Center = NULL;
    void SetDragAndDropLimit(float limit);

    int * stash = NULL;
    b2Vec2 store_range = b2Vec2(0.0f, 0.0f);
    void SetStore(int * value_address, b2Vec2 range);
    void StoreInfo();
};

