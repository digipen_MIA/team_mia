/******************************************************************************/
/*!
\file   CustomBaseObject.cpp
\project ColorLeon
\author primary : Jung Juyong, secondary : Kim Minsuk

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "CustomBaseObject.h"
#include "CustomBaseObject.h"
#include "Box2D\Dynamics\b2World.h"
#include <iostream>
#include <engine/State.h>
#include "EngineSupport.h"
#include "Player.h"

CustomBaseObject::CustomBaseObject()
	:Object()	// Call base class' constructor
{}

CustomBaseObject::~CustomBaseObject()
{}

/**
 * \brief 
 * Remove custom physics component
 * 
 * \param world 
 * Address of custom box2d world
 */
void CustomBaseObject::RemoveCustomPhysicsComp(b2World* world)
{
	// Check either if physics is allocated before
	if (customPhysics.m_body) {
		// Deregister rigid body from the physics world
		world->DestroyBody(customPhysics.m_body);
		// Set pointer to null
		customPhysics.m_body = nullptr;
	}
}

/**
* \brief
* Remove custom physics component
* *New and polished way
*/
void CustomBaseObject::RemoveCustomPhysicsComponent()
{
	auto customPhysicsList = customPhysics.m_pList;
	b2World* customPhysicsWorld = customPhysics.m_world;
        if(!IsPhysics)
        {
            return;
        }
	for (auto cPhysics = customPhysicsList->begin();
		cPhysics != customPhysicsList->end(); ++cPhysics)
	{
		if (customPhysics.m_body == (*cPhysics)->m_body)
		{
			// Erase component from the list
			customPhysicsList->erase(cPhysics);
			// Deregister rigid body from the physics world
			customPhysicsWorld->DestroyBody(customPhysics.m_body);
			// Set pointer to null
			customPhysics.m_body = nullptr;
			customPhysics.m_allocatedBody = false;
			break;
		}
	}
}

void CustomBaseObject::SetData()
{
    customPhysics.Getb2Body()->SetUserData(this);
}

void CustomBaseObject::Update(float dt)
{
	if (IsTimeDeath)
	{
		Time += dt;
		//sprite.color.a = 128 * (2.0f - (Time / lifetime));
		using namespace Support;
		//float scalerate = std::pow(30.0f, dt);
		//this->transform.SetScale(this->transform.GetScale() * scalerate);
		if (Time > lifetime)
		{
			Life = false;
		}
	}
    if(!IsPhysics)
    {
        return;
    }
    if(Support::Color::Compare_Color(Support::Color::GREEN, this->sprite.color))
    {   
        if(customPhysics.Getb2Body()->GetType() == b2BodyType::b2_dynamicBody)
        {
			if (State::m_input->IsPressed(SDL_SCANCODE_LSHIFT))
			{
			    const b2Vec2 FloatingLandPos = customPhysics.Getb2Body()->GetPosition();
			    b2Vec2 initialPoint(FloatingLandPos.x, FloatingLandPos.y);
			    b2Vec2 floatingPoint(FloatingLandPos.x, FloatingLandPos.y + 15.0f);
			    b2Vec2 diff = b2Vec2(floatingPoint.x - initialPoint.x, floatingPoint.y - initialPoint.y);
			    customPhysics.Getb2Body()->SetGravityScale(0.0f);
			    customPhysics.Getb2Body()->SetLinearVelocity(diff);
			}
                        else
			{
			    customPhysics.Getb2Body()->SetGravityScale(1.0f);
			}
		}
    }
    else
    {
        if (customPhysics.Getb2Body()->GetType() == b2BodyType::b2_dynamicBody)
        {
            customPhysics.Getb2Body()->SetGravityScale(1.0f);
        }
    }
}

void CustomBaseObject::SetLifeTime(float life_time)
{
    IsTimeDeath = true;
    lifetime = life_time;
}
