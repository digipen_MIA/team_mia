/******************************************************************************/
/*!
\file   LevelBase.h
\project ColorLeon
\author primary : Kim Minsuk

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "engine\State.h"
#include "CustomBaseObject.h"
#include "Player.h"
#include "CustomCollision.h"

class LevelBase : public State
{
	friend class Game;

public:
	SDL_Rect rect;

	void MoveNext();

	std::vector<CustomBaseObject *> Object;
        static int LevelNum;
protected:

	LevelBase() : State() {};
	~LevelBase() override {};

	// Derived initialize function
	void Initialize() override;
	// Derived Update function
	void Update(float dt) override;
	// Derived Close function
	void Close() override;


	void LoadFile();

private:
	CustomCollision myCustomCollision;
	Player* player;
        CustomBaseObject hud;
        bool ShouldQuit = false;

        CustomBaseObject * Cursor;
};
