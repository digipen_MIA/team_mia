/******************************************************************************/
/*!
\file   MainMenu.h
\project ColorLeon
\author primary : Kim Minsuk

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include <engine/State.h>
#include "CustomBaseObject.h"
#include "Button.h"

class MainMenu : public State
{
    friend class Game;

public:

    SDL_Rect rect;


protected:

    MainMenu() : State() {};
    ~MainMenu() override {};

    // Derived initialize function
    void Initialize() override;
    // Derived Update function
    void Update(float dt) override;
    // Derived Close function
    void Close() override;
private:
    std::vector<CustomBaseObject *>   Object;
    CustomBaseObject	    wallPaper;
    CustomBaseObject	    blank_paper;

    std::vector<CustomBaseObject *> AchievmentObj;

    CustomBaseObject * Cursor;

    int ShouldGo = 0;
    int LevelNum = 1;

    bool HowToPlay = false;
    bool Credit = false;
    bool Achievement = false;

    bool MoveLevel = false;

    bool IsJump = false;
};
