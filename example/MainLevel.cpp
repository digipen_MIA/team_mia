/******************************************************************************/
/*!
\file   MainLevel.cpp
\project ColorLeon
\author primary : Kim Hyunseok, secondary : Kim Minsuk, Choi Jinhyun

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "CommonLevel.h"
#include "Rope.h"
#include "CustomCollision.h"
#include "EngineSupport.h"
#include "RayCast.h"
#include "Item.h"
#include "Application.h"

CustomCollision myCustomCollision;

static int tick;

void MainLevel::Initialize()
{
    m_backgroundColor = { 0, 0, 0 };

    camera.Initialize(int(State::m_width), int(State::m_height));
    camera.position.Set(0, 0, 0.0f);

    m_useBuiltInPhysics = false;

    b2Vec2 gravity(0.f, -40.0f);      // 0, 200
    SetCustomPhysicsWorld(gravity);

    //* Here we predefine some info to create a body
    // Set the object's unique name
    player = new Player();
    player->SetName("Player");
    player->transform.position.Set(0.0f, 20.0f, 0.0f);
    player->transform.SetScale(32, 32);
    player->sprite.color = Support::Color::WHITE;
    player->sprite.LoadImage("texture/leon.png", m_renderer);
    player->customPhysics.bodyType = CustomPhysics::DYNAMIC;
    player->customPhysics.bodyShape = CustomPhysics::BOX;
    player->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &player->transform);
    player->SetState(this);
    player->Set_Player_Camera(&camera);

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Ground");
    Object.back()->transform.position.Set(0.0f, 0.0f, 0.0f);
    Object.back()->transform.SetScale(512, 32);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.SetRestitution(0.f);
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Arrow1");
    Object.back()->transform.position.Set(-120.0f, 50.0f, 0.0f);
    Object.back()->transform.SetScale(36, 12);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/Arrow_Right.png", m_renderer);
    Object.back()->sprite.activeAnimation = true;
    Object.back()->sprite.SetFrame(3);
    Object.back()->sprite.SetSpeed(8.0f);
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Wall");
    Object.back()->transform.position.Set(-224.0f, 416.0f, 0.0f);
    Object.back()->transform.SetScale(64, 800);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Dynamic_Ball");
    Object.back()->transform.position.Set(128.0f, 64.0f, 0.0f);
    Object.back()->transform.SetScale(64.0f, 64.0f);
    Object.back()->customPhysics.radius = 32;
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/circle.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::DYNAMIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::CIRCLE;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Rope");
    Object.back()->transform.position.Set(Object.back()->transform.position.x - player->transform.position.x, Object.back()->transform.position.y - player->transform.position.y, 0.0f);
    Object.back()->transform.SetScale(0.5f, 30.0f);//rope_active->GetRopeLenght());
    Object.back()->sprite.color = SDL_Color{ 255,255,255,0 };
    Object.back()->sprite.pTransform = &Object.back()->transform;
    Object.back()->sprite.LoadImage("texture/tongue.png", m_renderer);
    Object.back()->IsPhysics = false;

    Object.push_back(new Item());
    Object.back()->SetName("Hazard");
    Object.back()->transform.position.Set(1028.0f, -32.0f, 0.0f);
    Object.back()->transform.SetScale(5000, 32);
    Object.back()->sprite.color = Support::Color::OBSTACLE_RED;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->customPhysics.ActiveGhostCollision(false);
    Object.back()->SetData();
    static_cast<Item *>(Object.back())->SetEvent(Support::Event::Mark_Lose, player);

    Object.push_back(new Item());
    Object.back()->SetName("Goal");
    Object.back()->transform.position.Set(2100.0f, 55.0f, 0.0f);
    Object.back()->transform.SetScale(64, 64);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/GateAnimation.png", m_renderer);
    Object.back()->sprite.activeAnimation = true;
    Object.back()->sprite.SetFrame(8);
    Object.back()->sprite.SetSpeed(15.0f);
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->customPhysics.ActiveGhostCollision(false);
    Object.back()->SetData();
    static_cast<Item *>(Object.back())->SetEvent(Support::Event::Mark_Win, player);

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Ground2");
    Object.back()->transform.position.Set(320.0f, 48.0f, 0.0f);
    Object.back()->transform.SetScale(128, 128);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.SetRestitution(0.f);
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Ground3");
    Object.back()->transform.position.Set(564.0f, 48.0f, 0.0f);
    Object.back()->transform.SetScale(128, 128);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.SetRestitution(0.f);
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Ground4");
    Object.back()->transform.position.Set(884.0f, 0.0f, 0.0f);
    Object.back()->transform.SetScale(512, 32);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.SetRestitution(0.f);
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Ground5");
    Object.back()->transform.position.Set(2056.0f, 0.0f, 0.0f);
    Object.back()->transform.SetScale(512, 32);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.SetRestitution(0.f);
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Wall2");
    Object.back()->transform.position.Set(2280.0f, 416.0f, 0.0f);
    Object.back()->transform.SetScale(64, 800);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("FloatingLand");
    Object.back()->transform.position.Set(1470.0f, 600.0f, 0.0f);
    Object.back()->transform.SetScale(660, 660);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("wasdSprite");
    Object.back()->transform.position.Set(20.0f, 150.0f, 0.0f);
    Object.back()->transform.SetScale(96, 64);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/wasd_animated.png", m_renderer);
    Object.back()->sprite.activeAnimation = true;
    Object.back()->sprite.SetFrame(4);
    Object.back()->sprite.SetSpeed(10.0f);
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("PressM");
    Object.back()->transform.position.Set(440.0f, 200.0f, 0.0f);
    Object.back()->transform.SetScale(150, 18);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/PressM.png", m_renderer);
    Object.back()->sprite.activeAnimation = true;
    Object.back()->sprite.SetFrame(2);
    Object.back()->sprite.SetSpeed(2.0f);
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("KeyM");
    Object.back()->transform.position.Set(440.0f, 250.0f, 0.0f);
    Object.back()->transform.SetScale(40, 40);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/keyM.png", m_renderer);
    Object.back()->sprite.activeAnimation = true;
    Object.back()->sprite.SetFrame(2);
    Object.back()->sprite.SetSpeed(10.0f);
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("MouseSprite");
    Object.back()->transform.position.Set(1350.0f, 150.0f, 0.0f);
    Object.back()->transform.SetScale(32, 64);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/Mouse [animated].png", m_renderer);
    Object.back()->sprite.activeAnimation = true;
    Object.back()->sprite.SetFrame(2);
    Object.back()->sprite.SetSpeed(5.0f);
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("RopeLengthControlSprite");
    Object.back()->transform.position.Set(1600.0f, 150.0f, 0.0f);
    Object.back()->transform.SetScale(96, 64);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/wasd_WS.png", m_renderer);
    Object.back()->sprite.activeAnimation = true;
    Object.back()->sprite.SetFrame(4);
    Object.back()->sprite.SetSpeed(5.0f);
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("ConsoleStyleHud");
    Object.back()->transform.SetScale(State::m_width, State::m_height);
    Object.back()->sprite.color = SDL_Color{ 255,255,255,255 };
    Object.back()->sprite.LoadImage("texture/ConsoleStyleHUD.png", State::m_renderer);
    Object.back()->sprite.isHud = true;
    Object.back()->IsPhysics = false;

    GetCustomPhysicsWorld()->SetContactListener(&myCustomCollision);

    for (auto i : Object)
    {
        AddObject(i);
        if (i->IsPhysics)
        {
            AddCustomPhysicsComponent(i);
        }
    }
    /****************NOTE********************
     *PLAYER MUST BE ADDED LAST, SO THAT IT IS
     *DRAWN ABOVE ANY OTHER OBJECT
     ***************************************/
    AddObject(player);
    AddCustomPhysicsComponent(player);
    player->SetData();
    player->SetMap(Object);

    Cursor = Support::MakeCursor(this);
}

void MainLevel::Update(float dt)
{
    Support::UpdateCursor(this, Cursor);

    /***************************************
    >>Dynamic Camera: reacts to player.y value
    ***************************************/
    if (player->transform.position.y > 500)
    {
        camera.position.Set(player->transform.position.x, player->transform.position.y, 125.0f);

    }
    else if (player->transform.position.y < -200)
        camera.position.Set(player->transform.position.x, player->transform.position.y, -50.0f);
    else
        camera.position.Set(player->transform.position.x, player->transform.position.y, player->transform.position.y*0.25f);//z = y for dramatic effect

    if(player->isWin())
    {
        if(Support::Color::Compare_Color(player->sprite.color, Support::Color::WHITE))
        {
            Application::data.isWhiteMan[0] = true;
        }
        if (Application::data.LockLevel < 2)
        {
            Application::data.LockLevel = 2;
        }
        m_game->Change(LV_Level1);
    }

    using namespace Support;

    /***************************************
    >>To Level 1
    ***************************************/
    if (m_input->IsTriggered(SDL_SCANCODE_N))
    {
        m_game->Change(LV_Level1);
    }

    /***************************************
    >>Bring Out HUD
    ***************************************/
    if (m_input->IsPressed((MouseCode)PLAYER_KEY::CHANGE_COLOR))
    {
        if (tick % 2 == 0)
        {
            ++tick;
            return;
        }
    }

    /***************************************
    >>Pause & Restart
    ***************************************/
    if (m_input->IsTriggered(SDL_SCANCODE_ESCAPE))
    {
        m_game->Pause();
    }
    if (m_input->IsTriggered(SDL_SCANCODE_R))
    {
        m_game->Restart();
    }
    /***************************************
    >>Reduce the Gravity for the Ball
    ***************************************/
    if (m_input->IsPressed(SDL_SCANCODE_Z))
    {
        player->customPhysics.Getb2Body()->SetGravityScale(0.1f);
    }
    else
    {
        player->customPhysics.Getb2Body()->SetGravityScale(1.0f);
    }

    ++tick;

    // In the player update, the update include the rope
    player->Update(dt);
    for (auto i : Object)
    {
        i->Update(dt);
    }

    // Must be one of the last functions called at the end of State Update
    UpdateCustomPhysics(dt);	// Update custom physics system here
    Render(dt);

    /***************************************
    >>Tick Start Over
    ***************************************/
    if (tick == 100)
    {
        tick = 0;
    }
}

void MainLevel::Close()
{
    player->Close_Player_Rope();
    backgroundMusic.Free();

    // Deallocate custom physics world
    RemoveCustomPhysicsWorld();

    // Wrap up state
    ClearBaseState(false);

    for (auto i : Object)
    {
        delete i;
        i = NULL;
    }
    Object.clear();
    delete player;

    delete Cursor;
}