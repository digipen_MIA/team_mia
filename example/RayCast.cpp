/******************************************************************************/
/*!
\file   RayCast.cpp
\project ColorLeon
\author primary : Kim Minsuk, secondary : Choi Jinhyun

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "RayCast.h"
#include <engine/State.h>
#include "Item.h"
#include <iostream>

RayCast::RayCast()
{
    callback = new MyRayCastCallBack();
}

RayCast::~RayCast()
{
    delete callback;
}

void RayCast::Cast_Ray(b2Vec2 start, b2Vec2 end, State * world)
{
    b2Vec2 diff = end - start;
    diff.Normalize();
    diff *= 100;
    world->GetCustomPhysicsWorld()->RayCast(callback, start, start + diff);
}

float32 MyRayCastCallBack::ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal,
    float32 fraction)
{
    m_fixture = fixture;
    m_point = point;
    m_normal = normal;
    m_fraction = fraction;

    if (m_fixture->GetBody())
    {
        if (m_fixture->GetBody()->GetUserData())
        {
            if (static_cast<CustomBaseObject *>(m_fixture->GetBody()->GetUserData())->GetName() == "Player")
            {
                m_fixture = NULL;
                return -1;
            }
            if(dynamic_cast<Item *>(static_cast<CustomBaseObject *>(m_fixture->GetBody()->GetUserData())))
            {
                m_fixture = NULL;
            }
        }
    }
    return fraction;
}
