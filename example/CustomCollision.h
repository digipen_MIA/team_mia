/******************************************************************************/
/*!
\file   CustomCollision.cpp
\project ColorLeon
\author primary : Kim Minsuk

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include <Box2D/Dynamics/b2WorldCallbacks.h>

class CustomCollision : public b2ContactListener
{
    void BeginContact(b2Contact* contact);

    void EndContact(b2Contact* contact);

    void DealBeginCollision(void * objA, bool IsFixture = true, void * objB = NULL);

    void DealEndCollision(void * objA, bool IsFixture = true, void * objB = NULL);

    void PreSolve(b2Contact* contact, const b2Manifold* oldManifold) override;
};
