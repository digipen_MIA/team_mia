/******************************************************************************/
/*!
\file   MainMenu.cpp
\project ColorLeon
\author primary : Kim Minsuk, secondary : Choi Jinhyun, Kim Hyunseok

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "CommonLevel.h"
#include "EngineSupport.h"
#include "Application.h"

//BACK_BUTTON should be at the end of enum value
enum OBJECT_INDEX : int { CENTER, TEXT, LEON, CREDIT, HOWTOPLAY, WSPRITE, ConsoleStyleHud, ACHIEVEMENT, BACK_BUTTON};

enum MENU : int { START, EDIT_OR_SETTING, QUIT, CREDIT_MENU, HOW_TO_PLAY, ACHIEVEMENT_MENU, MENUNUM };

void MainMenu::Initialize()
{
    LevelNum = 1;
    MoveLevel = false;
    IsJump = false;

    blank_paper.SetName("BlankPaper");
    blank_paper.transform.position.Set(0.0f, 0.0f, 0.0f);
    blank_paper.transform.SetScale(m_width, m_height);
    blank_paper.sprite.color = SDL_Color{ 0, 0, 0, 0 };
    blank_paper.sprite.LoadImage("texture/rect.png", m_renderer);
    blank_paper.IsPhysics = false;

    m_backgroundColor = { 0Xff, 0xff, 0xff };

    //You Should Declare this only once!!!!!!!
    m_pBGM->LoadMusic("sound/My Song.wav");
    m_pBGM->Play();

    camera.Initialize(int(State::m_width), int(State::m_height));
    camera.position.Set(0, 0, 0.0f);

    mainFont = TTF_OpenFont("font/FONTHYUNSUNG.ttf", 23);

    m_useBuiltInPhysics = false;

    b2Vec2 gravity(0.f, -250.f);
    SetCustomPhysicsWorld(gravity);

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Center_Rectangle");
    Object.back()->transform.position.Set(0.0f, -120.0f, 0.0f);
    Object.back()->transform.SetScale(280.0f, 280.0f);
    Object.back()->sprite.LoadImage("texture/DISK.png", m_renderer);
    Object.back()->sprite.color = Support::Color::RED;
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::CIRCLE;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Text");
    Object.back()->transform.position.Set(5.0f, -130.0f, 0.0f);
    Object.back()->transform.SetScale(230.0f, 180.0f);
    Object.back()->sprite.LoadImage("texture/Start.png", m_renderer);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Color-Leon");
    Object.back()->transform.position.Set(0.0f, 85.0f, 0.0f);
    Object.back()->transform.SetScale(100.0f, 120.0f);
    Object.back()->sprite.LoadImage("texture/LeonWalking_M.png", m_renderer);
    Object.back()->sprite.color = SDL_Color{ 255, 0, 0, 255 };
    Object.back()->customPhysics.bodyType = CustomPhysics::DYNAMIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();
    Object.back()->sprite.SetFrame(4);
    Object.back()->sprite.SetSpeed(20.0f);
    Object.back()->sprite.activeAnimation = true;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Credit");
    Object.back()->transform.position.Set(0.0f, 0.0f, 0.0f);
    Object.back()->transform.SetScale(m_width, m_height);
    Object.back()->sprite.color = SDL_Color{ 255, 255, 255, 0 };
    Object.back()->sprite.LoadImage("texture/Credit_Page.png", m_renderer);
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("How_To_Play");
    Object.back()->transform.position.Set(-200.0f, 0.0f, 0.0f);
    Object.back()->transform.SetScale(static_cast<float>(m_width / 1.5), static_cast<float>(m_height));
    Object.back()->sprite.color = SDL_Color{ 255, 255, 255, 0 };
    Object.back()->sprite.LoadImage("texture/HowToPlay.png", m_renderer);
    Object.back()->IsPhysics = false;

    wallPaper.SetName("WallPaper");
    wallPaper.transform.position.Set(0.0f, 0.0f, 0.0f);
    wallPaper.transform.SetScale(m_width - 125, m_height - 95);
    wallPaper.sprite.color = SDL_Color{ 255, 255, 255, 255 };
    wallPaper.sprite.LoadImage("texture/TitleImage[animated].png", m_renderer);
    wallPaper.sprite.activeAnimation = true;
    wallPaper.sprite.SetFrame(5);
    wallPaper.sprite.SetSpeed(10.0f);
    wallPaper.IsPhysics = false;
    //WallPaper.sprite.isHud = true;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("PressWSprite");
    Object.back()->transform.position.Set(0.0f, -275.0f, 0.0f);
    Object.back()->transform.SetScale(200.0f, 20.0f);
    Object.back()->sprite.color = SDL_Color{ 255, 255, 255, 255 };
    Object.back()->sprite.LoadImage("texture/PressW.png", m_renderer);
    Object.back()->sprite.activeAnimation = true;
    Object.back()->sprite.SetFrame(2);
    Object.back()->sprite.SetSpeed(8.0f);
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("ConsoleStyleHud");
    Object.back()->transform.SetScale(State::m_width, State::m_height);
    Object.back()->sprite.color = SDL_Color{ 255,255,255,255 };
    Object.back()->sprite.LoadImage("texture/ConsoleStyleHUD.png", State::m_renderer);
    Object.back()->sprite.isHud = true;
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Achievement");
    Object.back()->transform.SetScale(State::m_width, State::m_height);
    Object.back()->sprite.color = SDL_Color{ 0,0,0,255 };
    Object.back()->sprite.LoadImage("texture/rect.png", State::m_renderer);
    Object.back()->IsPhysics = false;

    AchievmentObj.push_back(new CustomBaseObject());
    AchievmentObj.back()->SetName("Achievement");
    AchievmentObj.back()->transform.position.Set(0.0f, 250.0f, 0.0f);
    AchievmentObj.back()->transform.SetScale(500.0f, 50.0f);
    AchievmentObj.back()->sprite.color = SDL_Color{ 255,255,255,255 };
    AchievmentObj.back()->sprite.LoadImage("texture/Achievement.png", State::m_renderer);
    AchievmentObj.back()->IsPhysics = false;

    AchievmentObj.push_back(new CustomBaseObject());
    AchievmentObj.back()->SetName("SnowWhite");
    AchievmentObj.back()->transform.position.Set(-500.0f, 100.0f, 0.0f);
    AchievmentObj.back()->transform.SetScale(128.f, 128.0f);
    AchievmentObj.back()->sprite.color = SDL_Color{ 255,255,255,255 };
    int proceed = 0;
    for (int i = 0; i < Application::LastLevel; ++i)
    {
        proceed += Application::data.isWhiteMan[i];
    }
    if(proceed == Application::LastLevel)
    {
        AchievmentObj.back()->sprite.LoadImage("texture/Achievement[SnowWhite].png", State::m_renderer);
    }
    else
    {
        AchievmentObj.back()->sprite.LoadImage("texture/Achievement[Locked].png", State::m_renderer);
    }
    AchievmentObj.back()->IsPhysics = false;
    AchievmentObj.push_back(new CustomBaseObject());
    AchievmentObj.back()->SetName("SnowWhite_Letter");
    AchievmentObj.back()->transform.position.Set(-250.0f, 100.0f, -1.0f);
    AchievmentObj.back()->transform.SetScale(350.f, 60.0f);
    std::string statement = "Can i be a snow white?";

    statement += "(" + std::to_string(proceed) + "/" + std::to_string(Application::LastLevel) + ")";
    AchievmentObj.back()->text.SetText(m_renderer, statement.c_str(), mainFont);
    AchievmentObj.back()->text.color = SDL_Color{ 255,255,255,255 };
    AchievmentObj.back()->IsPhysics = false;

    AchievmentObj.push_back(new CustomBaseObject());
    AchievmentObj.back()->SetName("Madman");
    AchievmentObj.back()->transform.position.Set(-500.0f, -200.0f, 0.0f);
    AchievmentObj.back()->transform.SetScale(128.f, 128.0f);
    AchievmentObj.back()->sprite.color = SDL_Color{ 255,255,255,255 };
    if(Application::data.isMadMan)
    {
        AchievmentObj.back()->sprite.LoadImage("texture/Achievement[SeeingRed].png", State::m_renderer);
    }
    else
    {
        AchievmentObj.back()->sprite.LoadImage("texture/Achievement[Locked].png", State::m_renderer);
    }
    AchievmentObj.back()->IsPhysics = false;

    AchievmentObj.push_back(new CustomBaseObject());
    AchievmentObj.back()->SetName("Madman_Letter");
    AchievmentObj.back()->transform.position.Set(-250.0f, -200.0f, -1.0f);
    AchievmentObj.back()->transform.SetScale(150.f, 60.0f);
    AchievmentObj.back()->text.SetText(m_renderer, "Seeing red", mainFont);
    AchievmentObj.back()->text.color = SDL_Color{ 255,255,255,255 };
    AchievmentObj.back()->IsPhysics = false;

    AchievmentObj.push_back(new CustomBaseObject());
    AchievmentObj.back()->SetName("Rainbow");
    AchievmentObj.back()->transform.position.Set(100.0f, 100.0f, 0.0f);
    AchievmentObj.back()->transform.SetScale(128.f, 128.0f);
    AchievmentObj.back()->sprite.color = SDL_Color{ 255,255,255,255 };
    if(Application::data.isRainbow)
    {
        AchievmentObj.back()->sprite.LoadImage("texture/Achievement[Rainbow].png", State::m_renderer);
    }
    else
    {
        AchievmentObj.back()->sprite.LoadImage("texture/Achievement[Locked].png", State::m_renderer);
    }
    AchievmentObj.back()->IsPhysics = false;

    AchievmentObj.push_back(new CustomBaseObject());
    AchievmentObj.back()->SetName("Rainbow_Letter");
    AchievmentObj.back()->transform.position.Set(350.0f, 100.0f, -1.0f);
    AchievmentObj.back()->transform.SetScale(150.f, 60.0f);
    AchievmentObj.back()->text.SetText(m_renderer, "So Colorful!!", mainFont);
    AchievmentObj.back()->text.color = SDL_Color{ 255,255,255,255 };
    AchievmentObj.back()->IsPhysics = false;

    AchievmentObj.push_back(new CustomBaseObject());
    AchievmentObj.back()->SetName("Undead");
    AchievmentObj.back()->transform.position.Set(100.0f, -200.0f, 0.0f);
    AchievmentObj.back()->transform.SetScale(128.f, 128.0f);
    AchievmentObj.back()->sprite.color = SDL_Color{ 255,255,255,255 };
    if (Application::data.DeadNum >= 100)
    {
        AchievmentObj.back()->sprite.LoadImage("texture/Achievement[PraiseTheSun].png", State::m_renderer);
    }
    else
    {
        AchievmentObj.back()->sprite.LoadImage("texture/Achievement[Locked].png", State::m_renderer);
    }
    AchievmentObj.back()->IsPhysics = false;

    AchievmentObj.push_back(new CustomBaseObject());
    AchievmentObj.back()->SetName("Undead_Letter");
    AchievmentObj.back()->transform.position.Set(350.0f, -200.0f, -1.0f);
    AchievmentObj.back()->transform.SetScale(300.f, 60.0f);
    statement = "ColorLeon NEVER die";

    statement += "(" + std::to_string(Application::data.DeadNum) + "/100)";
    AchievmentObj.back()->text.SetText(m_renderer, statement.c_str(), mainFont);
    AchievmentObj.back()->text.color = SDL_Color{ 255,255,255,255 };
    AchievmentObj.back()->IsPhysics = false;

    Object.push_back(new Button());
    Object.back()->SetName("Back_Button");
    Object.back()->transform.position.Set(520.0f, -330.0f, 1.0f);
    Object.back()->transform.SetScale(150.0f, 50.0f);  
    Object.back()->sprite.LoadImage("texture/BACK.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    dynamic_cast<Button *>(Object.back())->SetWorld(this, &camera);
    dynamic_cast<Button *>(Object.back())->SetColor(SDL_Color{ 255,0,0,0 });
    dynamic_cast<Button *>(Object.back())->SetClickEvent(Support::Event::MakeInvisible, Object.at(BACK_BUTTON));
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->sprite.isHud = true;
    Object.back()->SetData();

    AddObject(&wallPaper);
    AddObject(&blank_paper);
    for (auto i : Object)
    {
        AddObject(i);
        if (i->IsPhysics)
        {
            AddCustomPhysicsComponent(i);
        }
    }
    for(auto i : AchievmentObj)
    {
        AddObject(i);
    }

    Cursor = Support::MakeCursor(this);
}

void MainMenu::Update(float dt)
{
    Support::UpdateCursor(this, Cursor);

    //If Credit button is NOT visible
    if (dynamic_cast<Button *>(Object.at(BACK_BUTTON))->Color.a != 0)
    {
        if (Credit)
        {
            Object.at(CREDIT)->sprite.color = { 255,255,255,255 };
        }

        if (HowToPlay)
        {
            Object.at(HOWTOPLAY)->sprite.color = { 255,255,255,255 };
            blank_paper.sprite.color = { 0,0,0,255 };
        }
        if(Achievement)
        {   
            Object.at(ACHIEVEMENT)->sprite.color.a = 255;
        }


        Object.at(ConsoleStyleHud)->sprite.color.a = 0;
        Object.at(CENTER)->sprite.color.a = 0;
        Object.at(TEXT)->text.color.a = 0;
        Object.at(LEON)->sprite.color.a = 0;
        //Show credit & pressW image
        Object.at(WSPRITE)->sprite.color = { 255,255,255,0 };
    }
    //If Credit button is visible
    else
    {
        Object.at(ConsoleStyleHud)->sprite.color.a = 255;
        Object.at(CENTER)->sprite.color.a = 255;
        Object.at(TEXT)->text.color.a = 255;
        Object.at(LEON)->sprite.color.a = 255;

        Object.at(CREDIT)->sprite.color = { 255,255,255,0 };
        Object.at(HOWTOPLAY)->sprite.color = { 255, 255, 255, 0 };

        Object.at(ACHIEVEMENT)->sprite.color.a = 0;

        blank_paper.sprite.color = { 0,0,0,0 };
        Credit = false;
        HowToPlay = false;
        Achievement = false;
        //don't show credit and pressW image
        Object.at(WSPRITE)->sprite.color = { 255,255,255,255 };
    }
        
    for(auto i : AchievmentObj)
    {
        i->sprite.color.a = Object.at(ACHIEVEMENT)->sprite.color.a;
        i->text.color.a = Object.at(ACHIEVEMENT)->sprite.color.a;
    }

    if (ShouldGo != 0)
    {
        Object.at(CENTER)->transform.rotation += 9.0f * ShouldGo;
        Object.at(TEXT)->transform.rotation += 9.0f * ShouldGo;
        if (Object.at(CENTER)->transform.rotation == 180.0f ||
            Object.at(CENTER)->transform.rotation == -180.0f)
        {
            LevelNum += ShouldGo;
            if (LevelNum < 1)
            {
                LevelNum = MENUNUM;
            }
            if (LevelNum >= MENUNUM + 1)
            {
                LevelNum = 1;
            }
            std::string text;
            switch (LevelNum - 1)
            {
            case START:
                Object.at(TEXT)->sprite.LoadImage("texture/Start.png", m_renderer);
                break;
            case EDIT_OR_SETTING:
#ifdef _DEBUG                      //TODO change ifndef --> ifdef later
                Object.at(TEXT)->sprite.LoadImage("texture/EDIT.png", m_renderer);
#else
                Object.at(TEXT)->sprite.LoadImage("texture/Option.png", m_renderer);
#endif
                break;
            case QUIT:
                Object.at(TEXT)->sprite.LoadImage("texture/QUIT.png", m_renderer);
                break;
            case CREDIT_MENU:
                Object.at(TEXT)->sprite.LoadImage("texture/Credit.png", m_renderer);
                break;
            case HOW_TO_PLAY:
                Object.at(TEXT)->sprite.LoadImage("texture/HowToPlayButton.png", m_renderer);
                break;
            case ACHIEVEMENT_MENU:
                Object.at(TEXT)->sprite.LoadImage("texture/Award.png", m_renderer);
                break;
            }
            //Does not use text anymore
            //Object.at(TEXT)->text.SetText(m_renderer, text.c_str(), mainFont);
            ShouldGo = 0;
            Object.at(CENTER)->transform.rotation = 0.0f;
            Object.at(TEXT)->transform.rotation = 0.0f;
            SDL_Color color = Object.at(CENTER)->sprite.color;
            while (Support::Color::Compare_Color(Object.at(CENTER)->sprite.color, color))
            {
                Object.at(CENTER)->sprite.color = Support::Rand::GetRandomColor();
            }
            Object.at(LEON)->sprite.color = Object.at(CENTER)->sprite.color;
        }
    }
    else
    {
        Object.at(LEON)->sprite.SetCurrentFrame(true, 0);
    }

    for (auto i : Object)
    {
        i->Update(dt);
    }

    if (!HowToPlay && !Credit && !Achievement)
    {
        if (m_input->IsPressed(SDL_SCANCODE_D) && ShouldGo == 0 && !MoveLevel)
        {
            Object.at(LEON)->sprite.SetCurrentFrame(false, 0);
            Object.at(LEON)->sprite.flip = SDL_FLIP_NONE;
            ShouldGo = 1;
        }
        if (m_input->IsPressed(SDL_SCANCODE_A) && ShouldGo == 0 && !MoveLevel)
        {
            Object.at(LEON)->sprite.SetCurrentFrame(false, 0);
            Object.at(LEON)->sprite.flip = SDL_FLIP_HORIZONTAL;
            ShouldGo = -1;
        }

        if (m_input->IsTriggered(SDL_SCANCODE_W))
        {
            MoveLevel = true;
        }
    }

    //Set Leon's position's x to center
    Object.at(LEON)->customPhysics.AddForce(-Object.at(LEON)->transform.position.x * 0.2f, 0.0f);

    if (MoveLevel)
    {
        if (!IsJump)
        {
            Object.at(LEON)->customPhysics.SetVelocity(0.0f, 100.0f);
            Object.at(LEON)->customPhysics.pOwnerTransform->position.y += 1.0f;
            IsJump = true;
        }
        if (IsJump)
        {
            if (std::round(Object.at(LEON)->transform.position.y) <= 20)
            {
                switch (LevelNum - 1)
                {
                case START:
                    m_game->Change(LV_LevelSelect);
                    break;
                case EDIT_OR_SETTING:
#ifdef _DEBUG
                    m_game->Change(LV_EditLevel);
#else
                    m_game->Change(LV_SettingLevel);
#endif
                    break;
                case QUIT:
                    //m_game->Quit();
                    m_game->Quit();
                    //m_game->Close();
                    break;
                case CREDIT_MENU:
                    Credit = true;
                    dynamic_cast<Button *>(Object.at(BACK_BUTTON))->SetColor({ 255, 255, 0, 255 });
                    break;
                case HOW_TO_PLAY:
                    HowToPlay = true;
                    //Object.at(5)->sprite.color = { 255, 255, 255, 255 };
                    dynamic_cast<Button *>(Object.at(BACK_BUTTON))->SetColor({ 255, 255, 0, 255 });
                    break;
                case ACHIEVEMENT_MENU:
                    Achievement = true;
                    dynamic_cast<Button *>(Object.at(BACK_BUTTON))->SetColor({ 255, 255, 0, 255 });
                    break;
                default:
                    break;
                }
                MoveLevel = false;
                IsJump = false;
            }
        }
    }

    // Must be one of the last functions called at the end of State Update
    UpdateCustomPhysics(dt);	// Update custom physics system here
    Render(dt);
}

void MainMenu::Close()
{
    // Deallocate custom physics world
    RemoveCustomPhysicsWorld();
    backgroundMusic.Free();
    // Wrap up state fasle means that we don't intend to turn off BGM
    ClearBaseState(false);
    for(auto i : AchievmentObj)
    {
        delete i;
        i = NULL;
    }
    AchievmentObj.clear();
    for (auto i : Object)
    {
        delete i;
        i = NULL;
    }
    Object.clear();

    delete Cursor;
}

