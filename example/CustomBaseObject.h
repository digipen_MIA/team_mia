/******************************************************************************/
/*!
\file   CustomBaseObject.h
\project ColorLeon
\author primary : Kim Minsuk

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "engine/Object.h"
#include "CustomPhysics.h"

class Player;
class b2Worldl;
class CustomBaseObject : public Object
{

public:

	float Time = 0.0f;

	CustomBaseObject();
	~CustomBaseObject() override;

	bool IsPhysics = true;

	// This is not built-in remove function
	// So you have to call this function 
	// whenever you delete an object contains custom physics
	void RemoveCustomPhysicsComp(b2World* world);

	void RemoveCustomPhysicsComponent();

	CustomPhysics customPhysics;

	float lifetime = 0.0f;
	bool IsTimeDeath = false;

	void SetData();
	void SetDanger(bool danger);
	bool GetDanger();
	virtual void Update(float dt);

    CustomBaseObject * Cog = NULL;

	bool Life = true;

	void SetLifeTime(float life_time);
};