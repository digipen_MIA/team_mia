/******************************************************************************/
/*!
\file   Application.cpp
\project ColorLeon
\author primary : Jung Juyong secondary : Kim Minsuk

    All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/

#include "Application.h"
#include <sstream>
#include "SDL2/sdl_mixer.h"
#include "SDL2\SDL_ttf.h"
#include "SDL2/SDL_image.h"
#include "engine\Debug.h"

#include "iostream"
#include <fstream>

Application::Setting_Info Application::setting;
Application::Data_Info Application::data;

int Application::WheelEvent = 0;
int Application::LastLevel = 0;

/**
 * \brief
 * Initializes the application and all systems that the game requires
 *
 * \return
 * TRUE if successful, FALSE otherwise
 */
bool Application::CanStartUp()
{
    //SDL initialization, create window, create render
    auto init_result = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER);
    if (init_result < 0)
    {
        CheckSDLError(__LINE__);
        return false;
    }
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"), CheckSDLError(__LINE__);
    SDL_SetHint(SDL_HINT_RENDER_VSYNC, "1"), CheckSDLError(__LINE__);
    window = SDL_CreateWindow("GAM150 Example", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        Game::SCREEN_WIDTH, Game::SCREEN_HEIGHT, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE),
        CheckSDLError(__LINE__);
    if (window == nullptr)
        return false;
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC), CheckSDLError(__LINE__);
    if (renderer == nullptr)
        return false;
    // Pass SDL_Renderer to the base State class
    State::m_renderer = renderer;
    // Pass Input to the base State class
    State::m_input = &m_input;
    // Pass Game to the base State class
    State::m_game = &game;

    done = false;
    ticks_last = SDL_GetTicks();

    // Initialize png loading
    int imgFlag = IMG_INIT_PNG;
    if (!(IMG_Init(imgFlag) & imgFlag))
    {
        CheckSDLError(__LINE__);
        return false;
    }

    //Initialize SDL_mixer
    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
    {
        CheckSDLError(__LINE__);
        return false;
    }

    //Initializes text rendering
    if (TTF_Init() == -1)
    {
        CheckSDLError(__LINE__);
        return false;
    }

    // Get display screen size
    GetScreenSize();

    LastLevel = FindLastLevel();

    loadSetting();

    SDL_ShowCursor(false);

    return game.Initialize();
}

/**
 * \brief
 * Polling the events of the OS is done here and passed on to the game.
 * No game logic should be done here.
 */
void Application::Update()
{
    if (done)	//checks if the application has quit.
        return;

    if(State::m_pBGM == NULL)
    {
        State::m_pBGM = new Music();
    }
    else
    {
        State::m_pBGM->SetVolume(Application::setting.volume);
    }

    Input::ResetStatus(); // This must be called to properly update the store status of keys

    SDL_GetWindowSize(window, &game.m_width, &game.m_height);

    WheelEvent = 0;

    SDL_Event event;
    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
        case SDL_QUIT:
            game.Quit();
            break;
        case SDL_KEYDOWN:
            m_input.SetKeyPressed(event.key.keysym.scancode, SDL_KEYDOWN);
            break;
        case SDL_KEYUP:
            m_input.SetKeyPressed(event.key.keysym.scancode, SDL_KEYUP);
            break;
        case SDL_WINDOWEVENT:
            break;
        case SDL_MOUSEBUTTONDOWN:
            //fall through to next case
        case SDL_MOUSEBUTTONUP:
            m_input.SetMousePressed(event.button);
            break;
        case SDL_MOUSEMOTION:
            m_input.SetMousePos(static_cast<float32>(event.button.x), static_cast<float32>(event.button.y));
            break;
        case SDL_MOUSEWHEEL:
        {
            if (event.wheel.y > 0)
            {
                WheelEvent = 1;
            }
            else if (event.wheel.y < 0)
            {
                WheelEvent = -1;
            }
        }
        break;
        }
    }

    //Update toggle form game 
    done = game.IsQuit();

    // Still app is turned off by esc key
    //if (m_input.IsPressed(SDL_SCANCODE_ESCAPE))
        //done = true;

    if (m_input.IsTriggered(SDL_SCANCODE_G))
        SDL_SetWindowSize(window, game.m_width, game.m_height);

    SetFullscreen(setting.isFullscreen);

    //Clock
    Uint32 ticks = SDL_GetTicks();
    float dt = 0.001f * (ticks - ticks_last);
    ticks_last = ticks;

    // frame rate
    ++frame_count;
    frame_time += dt;
    if (frame_time >= 1.0)
    {
        auto fps = frame_count / frame_time;
        frame_count = 0;
        frame_time = 0;
        std::stringstream ss;
        ss << "COLOR LEON [fps=" << static_cast<int>(fps) << "]";
        SDL_SetWindowTitle(window, ss.str().c_str());
    }

    //This is where our game update() gets called
    game.Update(dt);
    SDL_Delay(1);
}

//Cleans up the systems that application was using
Application::~Application()
{
    delete State::m_pBGM;

    game.Close();

    // Wrap up sounds
    Mix_Quit();

    // Wrap up graphics
    TTF_Quit();
    IMG_Quit();

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void Application::CreateConsole()
{
    // Pop console window 
    DEBUG_CREATE_CONSOLE();

    // Check memory leak
    DEBUG_LEAK_CHECKS(-1);
}

void Application::DeleteConsole()
{
    // Delete console window
    DEBUG_DESTROY_CONSOLE();

    saveSetting();
}

void Application::SetFullscreen(bool toggle)
{
    setting.isFullscreen = toggle;
    SDL_SetWindowFullscreen(window, setting.isFullscreen);
}

int gcd(int a, int b)
{
    // Using euclidean algorithm
    if (a == 0)
        return b;
    else if (b == 0)
        return a;

    if (a < b)
        return gcd(a, b % a);
    else
        return gcd(b, a % b);
}

void Application::GetScreenSize()
{
    SDL_DisplayMode displayMode;

    if (SDL_GetDesktopDisplayMode(0, &displayMode) != 0)
        SDL_Log("SDL_GetDesktopDisplayMode failed: %s", SDL_GetError());

    m_screenWidth = displayMode.w;
    m_screenHeight = displayMode.h;

    int denominator = gcd(m_screenWidth, m_screenHeight);
    ratioWidth = m_screenWidth / denominator;
    ratioHeight = m_screenHeight / denominator;

    std::cout << "Screen size: " << m_screenWidth << " " << m_screenHeight << std::endl;
    std::cout << "Ratio: " << ratioWidth << " " << ratioHeight << std::endl;
}

void Application::loadSetting()
{
    std::string filename = "data/Setting.txt";
    const char *filenames = filename.c_str();

    FILE* pFile = 0;
    fopen_s(&pFile, filenames, "rt");
    if (pFile) {
        int temp_boolean;
        fscanf_s(pFile, "%d", &temp_boolean);
        setting.isFullscreen = (bool)temp_boolean;
        fscanf_s(pFile, "%d", &setting.volume);
        fscanf_s(pFile, "%d", &temp_boolean);
        setting.isMute = (bool)temp_boolean;
        fclose(pFile);
    }

    data.isWhiteMan = new bool[LastLevel];
    for(int i = 0; i < LastLevel; ++i)
    {
        data.isWhiteMan[i] = false;
    }

    filename = "data/Data.txt";
    const char *datafilenames = filename.c_str();

    pFile = 0;
    fopen_s(&pFile, datafilenames, "rt");
    if (pFile) 
    {
        fscanf_s(pFile, "%d", &data.LockLevel);
        int temp_boolean = 0;
        for(int i = 0; i < LastLevel; ++i)
        {
            fscanf_s(pFile, "%d", &temp_boolean);
            data.isWhiteMan[i] = (bool)temp_boolean;
        }
        fscanf_s(pFile, "%d", &temp_boolean);
        data.isMadMan = (bool)temp_boolean;
        fscanf_s(pFile, "%d", &temp_boolean);
        data.isRainbow = (bool)temp_boolean;
        fscanf_s(pFile, "%d", &data.DeadNum);
        fclose(pFile);
    }
}

void Application::saveSetting()
{
    std::ofstream FOut("data/Setting.txt", std::ios::trunc);

    FOut << setting.isFullscreen << "\n";
    FOut << setting.volume << "\n";
    FOut << setting.isMute << "\n";
    FOut.close();

    std::ofstream Fout("data/Data.txt", std::ios::trunc);

    Fout<<data.LockLevel<<"\n";
    for(int i = 0; i < LastLevel; ++i)
    {
        Fout<<data.isWhiteMan[i]<<"\n";
    }
    Fout<<data.isMadMan<<"\n";
    Fout<<data.isRainbow<<"\n";
    Fout<<data.DeadNum<<"\n";

    Fout.close();

    delete []data.isWhiteMan;
}

int Application::FindLastLevel()
{
    int number = 1;
    while (true)
    {
        int objNum;
        bool IsPlayerThere = false;
        std::string tostring = std::to_string(number);
        std::string filename = "map/LEVEL" + tostring + ".txt";
        const char *filenames = filename.c_str();
        float temp;
        int objecttype;
        FILE* pFile = 0;
        fopen_s(&pFile, filenames, "rt");
        if (pFile) {
            fscanf_s(pFile, "%d", &objNum);
            for (int i = 0; i < objNum; ++i)
            {
                fscanf_s(pFile, "%f", &temp);
                fscanf_s(pFile, "%f", &temp);
                fscanf_s(pFile, "%f", &temp);
                fscanf_s(pFile, "%f", &temp);
                fscanf_s(pFile, "%f", &temp);
                fscanf_s(pFile, "%f", &temp);
                fscanf_s(pFile, "%d", &objecttype);
                if (objecttype == (int)ObjectType::PLAYER)
                {
                    IsPlayerThere = true;
                }
            }
            fclose(pFile);
        }
        if (!IsPlayerThere)
        {
            break;
        }
        ++number;
    }
    return number + 1;
}
