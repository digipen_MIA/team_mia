/******************************************************************************/
/*!
\file   Button.cpp
\project ColorLeon
\author primary : Kim Minsuk

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "Button.h"
#include "engine/State.h"
#include "EngineSupport.h"
#include "Game.h"

Button::~Button()
{
    if(DragAndDropLimit)
    {
        delete DragAndDropLimit;
        delete Center;
    }
}

void Button::SetColor(SDL_Color colour)
{
    Color = colour;
}

void Button::SetWorld(State* world_param, Camera * temp_camera)
{
    world = world_param;
    camera = temp_camera;
}

void Button::Update(float /*dt*/)
{
    if (sprite.isHud)
    {
        MousePos = Support::GetMousePos(world);
    }
    else
    {
        MousePos = Support::GetWorldMousePos(world, camera);
    }
    UpdateColor();
    if(IsOnMouse())
    {
        if(buttontype == ButtonType::CLICK)
        {   
            if(world->m_input->IsTriggered(1))
            {
                ClickEvent(Param);
            }
        }
        if(buttontype == ButtonType::DRAG_DROP)
        {
            if(world->m_input->IsTriggered(1))
            {
                IsDrag = true;
                local_mousePos = - MousePos + customPhysics.Getb2Body()->GetPosition();
            }
        }
    }
    if(IsDrag)
    {        
        using namespace Support;
        float sizeScale = 10.f;
        float sizeRotate = 3.f;
        if(world->m_input->IsPressed(SDL_SCANCODE_LSHIFT))
        {
            sizeScale = 1.f;
            sizeRotate = 1.f;
        }
        if(world->m_input->IsPressed(SDL_SCANCODE_W))
        {
            this->transform.SetScale(b2Vec2(this->transform.GetScale().x, this->transform.GetScale().y + sizeScale));
            std::cout<<"Block Scale : "<<transform.GetScale()<<std::endl;
        }
        if (world->m_input->IsPressed(SDL_SCANCODE_S))
        {
            this->transform.SetScale(b2Vec2(this->transform.GetScale().x, this->transform.GetScale().y - sizeScale));
            std::cout<<"Block Scale : "<<transform.GetScale()<<std::endl;
        }
        if (world->m_input->IsPressed(SDL_SCANCODE_D))
        {
            this->transform.SetScale(b2Vec2(this->transform.GetScale().x + sizeScale, this->transform.GetScale().y));
            std::cout<<"Block Scale : "<<transform.GetScale()<<std::endl;
        }
        if (world->m_input->IsPressed(SDL_SCANCODE_A))
        {
            this->transform.SetScale(b2Vec2(this->transform.GetScale().x - sizeScale, this->transform.GetScale().y));
            std::cout<<"Block Scale : "<<transform.GetScale()<<std::endl;
        }
        if (world->m_input->IsPressed(SDL_SCANCODE_LSHIFT))
        {
            transform.rotation += sizeRotate;
            std::cout<<"Block Rotate : "<<transform.rotation<<std::endl;
        }
        if (world->m_input->IsPressed(SDL_SCANCODE_E))
        {
            transform.rotation -= sizeRotate;
            std::cout<<"Block Rotate : "<<transform.rotation<<std::endl;
        }
        if(world->m_input->IsPressed(SDL_SCANCODE_DELETE))
        {
            Life = false;
            std::cout<<"Block Delete"<<std::endl;
            return;
        }
        if(world->m_input->IsTriggered(SDL_SCANCODE_SPACE))
        {
            if(blocktype == BlockType::DYNAMIC)
            {
                blocktype = BlockType::STATIC;
                std::cout<<"Block becomes static"<<std::endl;
            }
            else if (blocktype == BlockType::STATIC)
            {
                blocktype = BlockType::DYNAMIC;
                std::cout<<"Block becomes dynamic"<<std::endl;
            }
        }
        customPhysics.Getb2Body()->SetTransform(local_mousePos + MousePos, 0.0f);
        if(DragAndDropLimit)
        {
            b2Vec2 adjustPos = local_mousePos + MousePos;
            adjustPos.y = Center->y;
            if(adjustPos.x <= Center->x - *DragAndDropLimit)
            {
                adjustPos.x = Center->x - *DragAndDropLimit;
            }
            if(adjustPos.x >= Center->x + *DragAndDropLimit)
            {
                adjustPos.x = Center->x + *DragAndDropLimit;
            }
            customPhysics.Getb2Body()->SetTransform(adjustPos, 0.0f);
        }
    }
    StoreInfo();
    if(world->m_input->IsReleased(1))
    {
        IsDrag = false;
    }
}

void Button::UpdateColor()
{
    Uint8 a = Color.a / 2;
    if (IsOnMouse() || IsDrag)
    {
        sprite.color = SDL_Color{Color.r, Color.g, Color.b, a};
    }
    else
    {
        sprite.color = Color;
    }
}

bool Button::IsOnMouse()
{
    float ButtonScale_x = std::abs(customPhysics.pOwnerTransform->GetScale().x);
    float ButtonScale_y = std::abs(customPhysics.pOwnerTransform->GetScale().y);

    b2PolygonShape Shape;
    Shape.SetAsBox(ButtonScale_x * .5f / PIXELS_TO_METER, ButtonScale_y * .5f / PIXELS_TO_METER);
    return Shape.TestPoint(customPhysics.Getb2Body()->GetTransform(), MousePos);
}

void Button::SetType(ButtonType type)
{
    buttontype = type;
}

void Button::SetObjectType(ObjectType type)
{
	objecttype = type;
}

void Button::SetClickEvent(void(* event)(void*), void * param)
{
    Param = param;
    ClickEvent = event;
}

void Button::SetDragAndDropLimit(float limit)
{
    DragAndDropLimit = new float;
    *DragAndDropLimit = limit;
    Center = new b2Vec2;
    *Center = b2Vec2(customPhysics.Getb2Body()->GetTransform().p.x, customPhysics.Getb2Body()->GetTransform().p.y);
}

void Button::SetStore(int* value_address, b2Vec2 range)
{
    stash = value_address;
    store_range = range;
    float scale = store_range.y - store_range.x;
    float unit = ((*value_address) / scale) * 2.0f * (*DragAndDropLimit);
    float pos =  Center->x - *DragAndDropLimit + unit;

    customPhysics.Getb2Body()->SetTransform(b2Vec2(pos, Center->y), 0.0f);
}

void Button::StoreInfo()
{
    if(!stash)
    {
        return;
    }
    if(!DragAndDropLimit)
    {
        return;
    }
    float upper = (customPhysics.Getb2Body()->GetTransform().p.x - Center->x + *DragAndDropLimit);
    float lower = (*DragAndDropLimit * 2.0f);
    float scale = store_range.y - store_range.x;
    *stash = (int)(store_range.x + (upper / lower) * scale);
}
