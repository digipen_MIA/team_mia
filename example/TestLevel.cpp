/******************************************************************************/
/*!
\file   TestLevel.cpp
\project ColorLeon
\author primary : Kim Hyunseok, secondary : Kim Hyunsung

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "CommonLevel.h"
#include "Rope.h"
#include "CustomCollision.h"
#include "EngineSupport.h"
#include "RayCast.h"
#include "Item.h"

static int tick;

void TestLevel::Initialize()
{
    //b2JointDef check1;

    m_backgroundColor = { 0X00, 0x00, 0x00 };

    camera.Initialize(int(State::m_width), int(State::m_height));
    camera.position.Set(0, 0, 0.0f);

    m_useBuiltInPhysics = false;

    b2Vec2 gravity(0.f, -40.0f);      // 0, 200
    SetCustomPhysicsWorld(gravity);

    LoadFile();

    AddObject(player);
    AddCustomPhysicsComponent(player);
    for (auto i : Object)
    {
        AddObject(i);
        if (i->IsPhysics)
        {
            AddCustomPhysicsComponent(i);
        }
    }

    GetCustomPhysicsWorld()->SetContactListener(&myCustomCollision);
    player->SetMap(Object);

    Cursor = Support::MakeCursor(this);
}

void TestLevel::LoadFile()
{
    int objNum;
    bool IsPlayerThere = false;

    std::string tostring = std::to_string(LevelNum);
    std::string filename = "map/LEVEL" + tostring + ".txt";
    const char *filenames = filename.c_str();

    b2Vec3 position;
    b2Vec2 scale;

    float rotation = 0;
    int ObjectType = 0;
    FILE* pFile = 0;
    fopen_s(&pFile, filenames, "rt");
    if (pFile) {

        fscanf_s(pFile, "%d", &objNum);

        for (int i = 0; i < objNum; ++i)
        {
            fscanf_s(pFile, "%f", &position.x);
            fscanf_s(pFile, "%f", &position.y);
            fscanf_s(pFile, "%f", &position.z);
            fscanf_s(pFile, "%f", &scale.x);
            fscanf_s(pFile, "%f", &scale.y);
            fscanf_s(pFile, "%f", &rotation);
            fscanf_s(pFile, "%d", &ObjectType);

            //PLATFORM--------------------------------------------------------------
            if (ObjectType == (int)ObjectType::PLATFORM)
            {
                Object.push_back(new CustomBaseObject());
                Object.back()->SetName("PLATFORM");
                Object.back()->transform.position.Set(position.x, position.y, position.z);
                Object.back()->transform.SetScale(scale.x, scale.y);
                Object.back()->transform.rotation = rotation;

                Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
                Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
                Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
                Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
                Object.back()->SetData();
            }

            //PLAYER----------------------------------------------------------------
            else if (ObjectType == (int)ObjectType::PLAYER)
            {
                player = new Player();
                player->SetName("Player");
                player->transform.position.Set(position.x, position.y, position.z);
                player->transform.SetScale(scale.x, scale.y);
                player->transform.rotation = rotation;

                player->sprite.LoadImage("texture/leon.png", m_renderer);
                player->customPhysics.bodyType = CustomPhysics::DYNAMIC;
                player->customPhysics.bodyShape = CustomPhysics::BOX;
                player->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &player->transform);
                player->Set_Player_Camera(&camera);
                player->SetState(this);
                player->SetData();

                IsPlayerThere = true;
            }

            //GRAVITY OBJECT--------------------------------------------------------
            else if (ObjectType == (int)ObjectType::GRAV)
            {
                Object.push_back(new CustomBaseObject());
                Object.back()->SetName("Gravity");
                Object.back()->transform.position.Set(position.x, position.y, position.z);
                Object.back()->transform.SetScale(scale.x, scale.y);
                Object.back()->customPhysics.radius = scale.x / 2.0f;
                Object.back()->transform.rotation = rotation;

                Object.back()->sprite.LoadImage("texture/circle.png", m_renderer);
                Object.back()->sprite.color = Support::Color::GREEN;
                Object.back()->customPhysics.bodyType = CustomPhysics::DYNAMIC;
                Object.back()->customPhysics.bodyShape = CustomPhysics::CIRCLE;
                Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
                Object.back()->SetData();

            }

            else if (ObjectType == (int)ObjectType::DANGER)
            {
                Object.push_back(new Item());
                Object.back()->SetName("Danger");
                Object.back()->transform.position.Set(position.x, position.y, position.z);
                Object.back()->transform.SetScale(scale.x, scale.y);
                Object.back()->transform.rotation = rotation;

                Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
                Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
                Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
                Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
                Object.back()->SetData();
                Object.back()->sprite.color = Support::Color::OBSTACLE_RED;
                static_cast<Item *>(Object.back())->SetEvent(Support::Event::RestartEvent, this);
                Object.back()->customPhysics.ActiveGhostCollision(false);
            }

            //GOAL------------------------------------------------------------------
            else if (ObjectType == (int)ObjectType::GOAL)
            {
                Object.push_back(new Item());
                Object.back()->SetName("Goal");
                Object.back()->transform.position.Set(position.x, position.y, position.z);
                Object.back()->transform.SetScale(scale.x, scale.y);
                Object.back()->transform.rotation = rotation;

                Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
                Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
                Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
                Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
                Object.back()->sprite.color = Support::Color::WHITE;
                Object.back()->sprite.LoadImage("texture/GateAnimation.png", m_renderer);
                Object.back()->sprite.activeAnimation = true;
                Object.back()->sprite.SetFrame(8);
                Object.back()->sprite.SetSpeed(15.0f);
                Object.back()->SetData();
                static_cast<Item *>(Object.back())->SetEvent(Support::Event::GoNext, this);
                Object.back()->customPhysics.ActiveGhostCollision(true);
            }
        }
        fclose(pFile);
    }

    if (!IsPlayerThere)
    {
        m_game->Change(LV_MainMenu);
    }
}

void TestLevel::Update(float dt)
{
    Support::UpdateCursor(this, Cursor);

    using namespace Support;

    if (m_input->IsTriggered(SDL_SCANCODE_N))
    {
        Return();
    }

    /***************************************
    >>Dynamic Camera: reacts to player.y value
    ***************************************/
    if (player->transform.position.y > 500)
    {
        camera.position.Set(player->transform.position.x, player->transform.position.y, 125.0f);

    }
    else if (player->transform.position.y < -200)
        camera.position.Set(player->transform.position.x, player->transform.position.y, -50.0f);
    else
        camera.position.Set(player->transform.position.x, player->transform.position.y, player->transform.position.y*0.25f);//z = y for dramatic effect

/***************************************
>>Bring Out HUD
***************************************/
    if (m_input->IsPressed(SDL_SCANCODE_SPACE))
    {
        if (tick % 2 == 0)
        {
            ++tick;
            return;
        }
    }

    /***************************************
    >>Pause & Restart
    ***************************************/
    if (m_input->IsTriggered(SDL_SCANCODE_R))
    {
        m_game->Restart();
    }
    if (m_input->IsTriggered(SDL_SCANCODE_C))
    {
        m_game->editLevel.LevelNumber = LevelNum;
        m_game->Change(LV_EditLevel);
    }

    ++tick;

    // In the player update, the update include the rope
    player->Update(dt);
    for (auto i : Object)
    {
        i->Update(dt);
    }

    // Must be one of the last functions called at the end of State Update
    UpdateCustomPhysics(dt);	// Update custom physics system here
    Render(dt);

    /***************************************
    >>Tick Start Over
    ***************************************/
    if (tick == 100)
    {
        tick = 0;
    }
}

void TestLevel::Close()
{
    player->Close_Player_Rope();

    // Deallocate custom physics world
    RemoveCustomPhysicsWorld();

    // Wrap up state
    ClearBaseState(false);

    for (auto i : Object)
    {
        delete i;
        i = NULL;
    }
    Object.clear();
    delete player;

    delete Cursor;
}

void TestLevel::Return()
{
    m_game->Change(LV_EditLevel);
}
