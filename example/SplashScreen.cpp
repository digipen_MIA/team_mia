/******************************************************************************/
/*!
\file   SplashScreen.cpp
\project ColorLeon
\author primary : Choi Jinhyun

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "SplashScreen.h"
#include "Game.h"
#include "EngineSupport.h"
#include "Application.h"

void SplashScreen::Initialize()
{
    m_backgroundColor = { 0X00, 0x00, 0x00 };

    camera.Initialize(int(State::m_width), int(State::m_height));
    camera.position.Set(0, 0, 0.0f);

    m_useBuiltInPhysics = false;

    b2Vec2 gravity(0.f, 0.f);      // 0, 200
    SetCustomPhysicsWorld(gravity);

    pictureObject.SetName("ScreeenPicture");
    pictureObject.transform.position.Set(0.0f, -20.0f, 0.0f);
    pictureObject.transform.SetScale(1100.0f, 320.0f);
    pictureObject.sprite.LoadImage("texture/Digipen.png", m_renderer);
    pictureObject.sprite.color = SDL_Color{ 255,255,255,0 };
    pictureObject.IsPhysics = false;

    MIA_picture.SetName("MIA_LOGO");
    MIA_picture.transform.position.SetZero();
    MIA_picture.transform.SetScale(1000.0f, 500.0f);
    MIA_picture.sprite.LoadImage("texture/LOGO1.png", m_renderer);
    MIA_picture.sprite.color = SDL_Color{ 255,255,255,0 };
    MIA_picture.IsPhysics = false;
    MIA_picture.sound.LoadSound("sound/Intro.wav");
    MIA_picture.sound.SetVolume(128);
    MIA_picture.sound.Play(0);

    AddObject(&pictureObject);
    AddObject(&MIA_picture);

    Cursor = Support::MakeCursor(this);
}

void SplashScreen::Update(float dt)
{
    Support::UpdateCursor(this, Cursor);

    frame_count_A += 60*dt;
    if (frame_count_A < 255)
    {
        pictureObject.sprite.color = SDL_Color{ 255,255,255, (Uint8)frame_count_A };
    }
    else
    {
        pictureObject.sprite.color.a = 0;
        MIA_picture.sprite.color.a = 255;

        frame_count += dt;

        if (frame_count >= 3.0f)
        {
            m_game->Change(LV_MainMenu);
        }
    }


    Render(dt);
}

void SplashScreen::Close()
{
    RemoveCustomPhysicsWorld();
    ClearBaseState(false);

    delete Cursor;
}


