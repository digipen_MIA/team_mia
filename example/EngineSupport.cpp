/******************************************************************************/
/*!
\file   EngineSupport.cpp
\project ColorLeon
\author primary : Kim Minsuk, secondary : Choi Jinhyun, Kim Hyunseok

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "EngineSupport.h"
#include <engine/MKS.h>
#include "Game.h"
#include "CustomCollision.h"
#include "Application.h"
#include <chrono>
#include <random>
#include "OptionState.h"

namespace Support
{
    namespace Color
    {
        bool Compare_Color(SDL_Color lhs, SDL_Color rhs)
        {
            if (lhs.r == rhs.r)
            {
                if (lhs.b == rhs.b)
                {
                    if (lhs.g == rhs.g)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    int Rand::GetRand(int min, int max)
    {
        int current_time = static_cast<int>(std::chrono::system_clock::now().time_since_epoch().count());
        std::mt19937 rand(current_time);

        int temp = min + static_cast<unsigned int>(rand()) % (max - min + 1);

        return temp;
    }

    SDL_Color Rand::GetRandomColor()
    {
        int Color_Index = GetRand(1, 4);
        switch(Color_Index)
        {
        case 1:
            return Color::RED;
        case 2:
            return Color::BLUE;
        case 3:
            return Color::GREEN;
        case 4:
            return Color::YELLOW;
        }
        return Color::WHITE;
    }

    b2Vec2 GetMousePos(State * state)
    {
        b2Vec2 mousepos = state->m_input->GetMousePos();
        mousepos -= b2Vec2(state->m_width / 2.0f, state->m_height / 2.0f);
        mousepos *= 1.f / PIXELS_TO_METER;
        mousepos.y *= -1;
        return mousepos;
    }
    b2Vec2 GetWorldMousePos(State * state, Camera * camera)
    {
        b2Vec2 mousepos = state->m_input->GetMousePos();
        mousepos -= b2Vec2(state->m_width / 2.0f, state->m_height / 2.0f);
        mousepos *= 1.f / PIXELS_TO_METER;
        mousepos.y *= -1;

        mousepos *= (1.0f / (1.0f - camera->position.z / 500.0f));
        mousepos += b2Vec2(camera->position.x, camera->position.y) * (1.f / PIXELS_TO_METER);

        return mousepos;
    }
    bool operator<=(b2Vec2& lhs, b2Vec2& rhs)
    {
        if (lhs.x <= rhs.x)
        {
            if (lhs.y <= rhs.y)
            {
                return true;
            }
        }
        return false;
    }

    //this funtion simply compare vector2 and vector3 ignoring z of vector3
    bool operator<=(b2Vec2& vec2, b2Vec3& vec3)
    {
        if (vec2.x <= vec3.x)
        {
            if (vec2.y <= vec3.y)
            {
                return true;
            }
        }
        return false;
    }

    bool operator>=(b2Vec2& lhs, b2Vec2& rhs)
    {
        if (lhs.x >= rhs.x)
        {
            if (lhs.y >= rhs.y)
            {
                return true;
            }
        }
        return false;
    }

    bool operator>=(b2Vec2& vec2, b2Vec3& vec3)
    {
        if (vec2.x >= vec3.x)
        {
            if (vec2.y >= vec3.y)
            {
                return true;
            }
        }
        return false;
    }

    b2Vec2 operator+(b2Vec3& vec3, b2Vec2& vec2)
    {
        b2Vec2 result;
        result.x = vec3.x + vec2.x;
        result.y = vec3.y + vec2.y;
        return result;
    }

    b2Vec2 operator-(b2Vec3& vec3, b2Vec2& vec2)
    {
        b2Vec2 result;
        result.x = vec3.x - vec2.x;
        result.y = vec3.y - vec2.y;
        return result;
    }

    std::ostream& operator<<(std::ostream& os, const b2Vec2& vector)
    {
        os << "(" << vector.x << ", " << vector.y << ")";
        return os;
    }

    std::ostream& operator<<(std::ostream& os, const b2Vec3& vector)
    {
        os << "(" << vector.x << ", " << vector.y << ", " << vector.z << ")";
        return os;
    }

    b2Vec2 operator*(b2Vec2 vector, float scale)
    {
        b2Vec2 result;
        result.x = vector.x * scale;
        result.y = vector.y * scale;
        return result;
    }

    float RoundToEvenNumber(float value)
    {
        if(value == 0.0f)
        {
            return 0;
        }
        int result = (int)std::round(value);
        if(result % 2 == 0)
        {
            return (float)(result);
        }
        else
        {
            return (float)(result + 1);
        }
    }

    CustomBaseObject* MakeCursor(State* state)
    {
        CustomBaseObject * Cursor = new CustomBaseObject();
        Cursor->SetName("Cursor");
        Cursor->transform.SetScale(32, 32);
        Cursor->sprite.color = SDL_Color{ 255,255,255,255 };
        Cursor->sprite.LoadImage("texture/MouseCursor[animated].png", State::m_renderer);
        Cursor->sprite.SetFrame(6);
        Cursor->sprite.SetSpeed(6.0f);
        Cursor->sprite.activeAnimation = true;
        Cursor->sprite.isHud = true;
        Cursor->IsPhysics = false;
        state->AddObject(Cursor);
        return Cursor;
    }

    void UpdateCursor(State* state, CustomBaseObject* cursor)
    {
        b2Vec2 mousepos = State::m_input->GetMousePos();
        mousepos -= b2Vec2(state->m_width / 2.0f, state->m_height / 2.0f);
        mousepos.y *= -1;
        cursor->transform.position = b2Vec3(mousepos.x, mousepos.y, 0.0f);
    }

    namespace Event
    {
        void RestartEvent(void * param)
        {
            //Player::dead_hud_check = true;
            //if (!(Player::dead_hud_check))
            //{
                static_cast<State *>(param)->m_game->Restart();
            //}
        }

        void GoNext(void* param)
        {
            if (dynamic_cast<LevelBase *>(static_cast<State *>(param)))
            {
                dynamic_cast<LevelBase *>(static_cast<State *>(param))->MoveNext();
            }
        }

        void MoveMainLevel(void * param)
        {
            static_cast<State *>(param)->m_game->Change(LV_MainLevel);
        }

        void MoveEditLevel(void * param)
        {
            static_cast<State *>(param)->m_game->Change(LV_EditLevel);
        }

        void MoveSettingLevel(void* param)
        {
            static_cast<State *>(param)->m_game->Change(LV_SettingLevel);
        }

        void QuitGame(void * param)
        {
            static_cast<State *>(param)->m_game->Quit();
        }

        void MoveMainMenu(void * param)
        {
            static_cast<State *>(param)->m_game->Change(LV_MainMenu);
        }

        void MoveSelectLevel(void* param)
        {
            static_cast<State *>(param)->m_game->Change(LV_LevelSelect);
        }

        void ResumeEvent(void* param)
        {
            //*************************************
            //It can only use in pause level!!!!!!!
            //You should check this function is called only in option level
            //*************************************
            if(dynamic_cast<Option *>(static_cast<State *>(param)) == NULL)
            {
                return;
            }

            dynamic_cast<Option *>(static_cast<State *>(param))->DelayForResume();
        }

        void ResumeAndMainMenuEvent(void* param)
        {
            //*************************************
            //It can only use in pause level!!!!!!!
            //You should check this function is called only in option level
            //*************************************
            if (dynamic_cast<Option *>(static_cast<State *>(param)) == NULL)
            {
                return;
            }


            dynamic_cast<Option *>(static_cast<State *>(param))->DelayForResume();
            dynamic_cast<Option *>(static_cast<State *>(param))->DelayForMainMenu();
        }

        void MakeInvisible(void* param)
        {
            static_cast<Button *>(param)->SetColor({ static_cast<CustomBaseObject *>(param)->sprite.color.r,
                static_cast<CustomBaseObject *>(param)->sprite.color.b, static_cast<CustomBaseObject *>(param)->sprite.color.g, 0 });
        }

        void Makevisible(void* param)
        {
            static_cast<Button *>(param)->SetColor({ static_cast<CustomBaseObject *>(param)->sprite.color.r,
                static_cast<CustomBaseObject *>(param)->sprite.color.b, static_cast<CustomBaseObject *>(param)->sprite.color.g, 255 });
        }

        void Option_HowToPlay_Click(void* /*param*/)
        {
            Option::PrintHowToPlay = true;
        }

        void MakeInvisible_BUTTON(void* param)
        {
            Option::PrintHowToPlay = false;
            static_cast<Button *>(param)->SetColor({ static_cast<Button*>(param)->Color.r, static_cast<Button*>(param)->Color.g,
                static_cast<Button*>(param)->Color.b , 0 });
        }

        void Mark_Win(void* param)
        {
            //this function should get parameter as player *
            static_cast<Player *>(param)->SetWin(true);
            //static_cast<Player *>(param)->Win_Sound_check = true;
            Player::Win_Sound_check = true;
        }

        void Mark_Lose(void* param)
        {
            //this function should get parameter as player *
            static_cast<Player *>(param)->SetLose(true);
        }

        void ToggleFullScreen(void* param)
        {
            Application::setting.isFullscreen = !Application::setting.isFullscreen;
            if(Application::setting.isFullscreen)
            {
                static_cast<CustomBaseObject *>(param)->sprite.LoadImage("texture/On.png", State::m_renderer);
            }
            else
            {
                static_cast<CustomBaseObject *>(param)->sprite.LoadImage("texture/Off.png", State::m_renderer);   
            }
        }

        void ToggleMute(void* param)
        {
            Application::setting.isMute = !Application::setting.isMute;
            if (Application::setting.isMute)
            {
                static_cast<CustomBaseObject *>(param)->sprite.LoadImage("texture/On.png", State::m_renderer);
            }
            else
            {
                static_cast<CustomBaseObject *>(param)->sprite.LoadImage("texture/Off.png", State::m_renderer);
            }
        }
    }
}
