/******************************************************************************/
/*!
\file   TestLevel.h
\project ColorLeon
\author primary : Kim Hyunseok

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include <engine/State.h>
#include "CustomBaseObject.h"
#include "Button.h"
#include "Player.h"
#include "CustomCollision.h"

class TestLevel : public State
{
	friend class Game;

public:
	SDL_Rect rect;

	void Return();
	int LevelNum = 1;

protected:
	CustomCollision myCustomCollision;

	TestLevel() : State() {};
	~TestLevel() override {};

	// Derived initialize function
	void Initialize() override;
	// Derived Update function
	void Update(float dt) override;
	// Derived Close function
	void Close() override;


	void LoadFile();

private:
	Player* player;
	std::vector<CustomBaseObject *> Object;

    CustomBaseObject * Cursor;

};
