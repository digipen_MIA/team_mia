/******************************************************************************/
/*!
\file   Item.cpp
\project ColorLeon
\author primary : Kim Minsuk

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "Item.h"

Item::Item()
{

}

void Item::Update()
{
}

void Item::SetEvent(void(*event)(void*), void * param)
{
    Param = param;
    Event = event;
}

