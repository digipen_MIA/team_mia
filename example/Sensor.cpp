/******************************************************************************/
/*!
\file   Sensor.cpp
\project ColorLeon
\author primary : Kim Minsuk

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "Sensor.h"

Sensor::Sensor(std::string name, float x, float y, float w, float h, void * parent) : Name(name), Parent(parent)
{
    SensorShape.SetAsBox((w / 2.0f) / PIXELS_TO_METER, (h / 2.0f) / PIXELS_TO_METER, b2Vec2(x / PIXELS_TO_METER, y / PIXELS_TO_METER), 0);
    SensorFixtureDef.shape = &SensorShape;
    SensorFixtureDef.isSensor = true;
    SensorFixtureDef.userData = this;
    IsAlarm = false;
}

Sensor::~Sensor()
{
}

b2FixtureDef* Sensor::GetSensorFixtureDef()
{
    return &SensorFixtureDef;
}

void Sensor::SetSensorData(b2Body * body)
{
    SensorFixture = body->CreateFixture(&SensorFixtureDef);
    SensorFixture->SetUserData(this);
}
