/******************************************************************************/
/*!
\file   Item.h
\project ColorLeon
\author primary : Kim Minsuk

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "CustomBaseObject.h"

enum class ItemType{DANGER, ITEM};

class Item : public CustomBaseObject
{
public:
    Item();
    ItemType type;
    void * Param;
    void(*Event)(void *) = NULL;
    void Update();
    void SetEvent(void(*event)(void*), void * param = NULL);
};

