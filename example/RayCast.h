/******************************************************************************/
/*!
\file   RayCast.h
\project ColorLeon
\author primary : Kim Minsuk

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include <Box2D/Dynamics/b2WorldCallbacks.h>
#include "CustomBaseObject.h"

class MyRayCastCallBack;

class RayCast
{
public:
    RayCast();
    ~RayCast();
    MyRayCastCallBack * callback;
    void Cast_Ray(b2Vec2 start, b2Vec2 end, State * world);
};

class MyRayCastCallBack : public b2RayCastCallback
{
public:
    MyRayCastCallBack()
    {
        m_fixture = NULL;
    }

    float32 ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float32 fraction);

    b2Fixture * m_fixture;
    b2Vec2 m_point;
    b2Vec2 m_normal;
    float32 m_fraction;
};