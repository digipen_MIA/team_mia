/******************************************************************************/
/*!
\file   MainLevel.h
\project ColorLeon
\author primary : Kim Minsuk

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "engine\State.h"
#include "CustomBaseObject.h"
#include "Player.h"
#include "CustomCollision.h"

class MainLevel : public State
{
	friend class Game;

public:
	SDL_Rect rect;

protected:
	MainLevel() : State() {};
	~MainLevel() override {};

	// Derived initialize function
	void Initialize() override;
	// Derived Update function
	void Update(float dt) override;
	// Derived Close function
	void Close() override;
private:
	// Objects which contains custom physics
	Player*                         player;
    CustomBaseObject				hUD;
	std::vector<CustomBaseObject*>  Object;

        CustomBaseObject * Cursor;
};
