/******************************************************************************/
/*!
\file   Player.cpp
\project ColorLeon
\author primary : Kim Hyunseok, secondary : Choi Jinhyun, Kim Minsuk

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "Player.h"
#include <iostream>
#include "CommonLevel.h"
#include "EngineSupport.h"
#include "Item.h"
#include "RayCast.h"
#include "Rope.h"
#include "engine\State.h"
#include "Map.h"
#include "Application.h"

bool Player::Win_Sound_check = false;
Player::Player() : CustomBaseObject()
{}

Player::~Player()
{
    for (auto i : sensorList)
    {
        delete i;
    }
    sensorList.clear();
    Close_Player_Rope();
    std::vector<Sensor *>().swap(sensorList);

    for (auto i : afterImage)
    {
        delete i;
        i = NULL;
    }
    afterImage.clear();
    
    for (auto i : particle)
    {
        delete i;
        i = NULL;
    }
    particle.clear();

    for(auto i : sound_list)
    {
        delete i.second;
        i.second = NULL;
    }
    sound_list.clear();

    delete hud;
    delete ray;
    delete rope;
    delete rope_obj_;
    delete map;
    delete rope_detection;
    delete dead_hud;
    delete start_hud;
}

void Player::SetData()
{
    //sound_play_time = 0;
    dead_hud_check = true;
    //Win_Sound_check = false;

    Win = false;

    sensorList.push_back(new Sensor("Foot", 0, -transform.GetScale().y / 2.0f, transform.GetScale().x * 9.0f / 10.0f, 2.75f, this));
    sensorList.back()->SetSensorData(customPhysics.Getb2Body());
    sensorList.push_back(new Sensor("RightArm", transform.GetScale().x / 2.0f, 0.0f, 0.1f, transform.GetScale().y * 9.0f / 10.0f, this));
    sensorList.back()->SetSensorData(customPhysics.Getb2Body());
    sensorList.push_back(new Sensor("LeftArm", -(transform.GetScale().x / 2.0f), 0.0f, 0.1f, transform.GetScale().y * 9.0f / 10.0f, this));
    sensorList.back()->SetSensorData(customPhysics.Getb2Body());
    ray = new RayCast();

    hud = new CustomBaseObject();
    hud->SetName("Color_Wheel");
    hud->transform.position.Set(0, 0, 0);
    hud->transform.SetScale(State::m_width, State::m_height);
    hud->sprite.color = SDL_Color{ 255, 255, 255, 255 };
    hud->sprite.LoadImage("texture/ColorWheel.png", State::m_renderer);
    hud->sprite.isHud = true;
    hud->IsPhysics = false;

    dead_hud = new CustomBaseObject();
    dead_hud->SetName("Dead_Event");
    dead_hud->transform.position.Set(0,0,0);
    dead_hud->transform.SetScale(64.0f, 64.0f);
    dead_hud->sprite.color = SDL_Color{ 255,255,255, 0 };
    dead_hud->sprite.LoadImage("texture/Dying[animation].png", State::m_renderer);
    dead_hud->sprite.activeAnimation = true;
    dead_hud->sprite.SetFrame(4);
    dead_hud->sprite.SetSpeed(8.0f);
   //dead_hud->sprite.isHud = true;
    dead_hud->IsPhysics = false;

    start_hud = new CustomBaseObject();
    start_hud->SetName("Start_Event");
    start_hud->transform.position.Set(0,0, 0);
    start_hud->transform.SetScale(State::m_width, State::m_height);
    start_hud->sprite.color = SDL_Color{ 0,0,0, 0 };
    start_hud->sprite.LoadImage("texture/rect.png", State::m_renderer);
    start_hud->sprite.isHud = true;
    start_hud->IsPhysics = false;

    rope_obj_ = new CustomBaseObject();
    rope_obj_->SetName("Rope");
    rope_obj_->transform.position.Set(rope_obj_->transform.position.x - this->transform.position.x, rope_obj_->transform.position.y - this->transform.position.y, 0.0f);
    rope_obj_->transform.SetScale(0.5f, 30.0f);//rope_active->GetRopeLenght());
    rope_obj_->sprite.color = SDL_Color{ 255,255,255,0 };
    rope_obj_->sprite.pTransform = &rope_obj_->transform;
    rope_obj_->sprite.LoadImage("texture/tongue.png", State::m_renderer);
    rope_obj_->IsPhysics = false;

    rope_detection = new CustomBaseObject();
    rope_detection->SetName("Rope_Detection");
    rope_detection->transform.position.Set(this->customPhysics.Getb2Body()->GetTransform().p.x - 10, this->customPhysics.Getb2Body()->GetTransform().p.y-20, 0.0f);
    rope_detection->transform.SetScale(80.0f, 80.0f);
    rope_detection->sprite.color = SDL_Color{ 255,255,255,0 };
    rope_detection->sprite.pTransform = &rope_detection->transform;
    rope_detection->sprite.LoadImage("texture/ROPE.png", State::m_renderer);
    rope_detection->IsPhysics = false;

    sound_list["Bounce"] = MakeSound("Bounce");
    sound_list["Die"] = MakeSound("Die");
    sound_list["Clear"] = MakeSound("Clear");
    sound_list["HUD On"] = MakeSound("HUD On");
    sound_list["Connect"] = MakeSound("Connect");
    sound_list["Change Color"] = MakeSound("Change Color");
    sound_list["Jump"] = MakeSound("Jump");

    //sound_nextLevel = new CustomBaseObject;
    //sound_nextLevel->SetName("stageClearSound");
    //sound_nextLevel->sound.LoadSound("sound/Next Level.wav");

    for(auto i : sound_list)
    {
        state->AddObject(i.second);
    }

    //state->AddObject(sound_nextLevel);

    state->AddObject(hud);
    state->AddObject(rope_obj_);
    state->AddObject(dead_hud);
    state->AddObject(rope_detection);
    state->AddObject(start_hud);

    CustomBaseObject::SetData();
}

void Player::UpdateColor()
{
    //if Leon is red, increase restitution
    if (Support::Color::Compare_Color(sprite.color, Support::Color::RED))
    {
        customPhysics.GetFixture()->SetRestitution(1.5f);
    }
    else
    {
        customPhysics.GetFixture()->SetRestitution(0.0f);
    }

    //if Leon is blue, increase speed
    if (Support::Color::Compare_Color(sprite.color, Support::Color::BLUE))
    {
        Speed = 140.0f;
    }
    else
    {
        Speed = 50.0f;
    }

    //this is for color red
    for (int i = 0; i < (int)CollisionList.size(); ++i)
    {
        //This is list of object that is colliding with Leon (collision persist)
        CustomBaseObject * CollisionObject = static_cast<CustomBaseObject *>(CollisionList.at(i));

        //if Leon's color != colliding object's color...
        if (!Support::Color::Compare_Color(sprite.color, CollisionObject->sprite.color))
        {
            //change object's color to Leon's color
            CollisionObject->sprite.color = sprite.color;
            //if both Leon and colliding object is red, increase restitution.
            //else no restitution. This is done to debug color update delay
            if (Support::Color::Compare_Color(sprite.color, Support::Color::RED))
            {
                CollisionObject->customPhysics.GetFixture()->SetRestitution(2.0f);
            }
            else
            {
                CollisionObject->customPhysics.GetFixture()->SetRestitution(0.f);
            }
        }
    }
}

void Player::SetState(State* world)
{
    state = world;
    current_level_physics = world->GetCustomPhysicsWorld();
    level_ = world;
}

void Player::Set_Player_Camera(Camera* temp_camera)
{
    player_camera = temp_camera;
}

void Player::Rope_Draw()
{
    rope->Render(rope_obj_, this);
}

void Player::Close_Player_Rope()
{
    if (isRopeActive)
    {
        rope->DeleteRope(current_level_physics, &rope);
        isRopeActive = false;
    }
}

void Player::Rope_Detection()
{
    rope_detection->transform.position.Set(this->transform.position.x- 30, this->transform.position.y + 60, 0.0f);

    if (canRopeBeUsed)
    {
        rope_detection->sprite.color = SDL_Color{ 255, 255,255, 255 };
    }
    else
    {
        rope_detection->sprite.color = SDL_Color{ 255, 255, 255, 0 };
    }
}

bool Player::IsAlarm(const std::string & Name)
{
    for (auto i : sensorList)
    {
        if (i->IsAlarm)
        {
            if (i->Name == Name)
            {
                return true;
            }
        }
    }
    return false;
}

void Player::MakeDust(float offsetX, float offsetY, float sizeX, float sizeY)
{
    particle.push_back(new CustomBaseObject);
    particle.back()->SetName("Dust");
    particle.back()->transform.position.Set(transform.position.x + offsetX,
        transform.position.y + offsetY,
        0.0f);
    particle.back()->transform.SetScale(sizeX, sizeY);//75,100
    particle.back()->sprite.color = SDL_Color{ 180, 200, 255, 255 };
    particle.back()->sprite.LoadImage("texture/dust2.png", State::m_renderer);
    particle.back()->sprite.activeAnimation = true;
    particle.back()->sprite.SetFrame(4);
    particle.back()->sprite.SetSpeed(24.0f);
    particle.back()->IsPhysics = false;
    particle.back()->SetLifeTime(0.2f);

    state->AddObject(particle.back());
}

void Player::MakeAfterImage(float offsetX, float offsetY, float sizeX, float sizeY)
{
    afterImage.push_back(new CustomBaseObject);
    afterImage.back()->SetName("AfterImage");
    afterImage.back()->transform.position.Set(transform.position.x + offsetX,
        transform.position.y + offsetY,
        0.0f);
    afterImage.back()->transform.SetScale(sizeX, sizeY);
    afterImage.back()->sprite.color = SDL_Color{ 0   , 0   , 255 , 80 };
    afterImage.back()->sprite.LoadImage("texture/leon.png", State::m_renderer);
    if (shouldAfterimageFlip)
        afterImage.back()->sprite.flip = SDL_FLIP_HORIZONTAL;
    else
        afterImage.back()->sprite.flip = SDL_FLIP_NONE;
    afterImage.back()->IsPhysics = false;
    afterImage.back()->SetLifeTime(0.05f);

    state->AddObject(afterImage.back());
}

void Player::SetMap(std::vector<CustomBaseObject *> objectlist)
{
    map = new Map();
    map->SetMapData(objectlist, state, this);
}

void Player::StartMapFunc()
{
    if(dead_hud_check)
    {
        start_hud_alpha -= 2;

        start_hud->sprite.color.a = (Uint8)start_hud_alpha;
        if(start_hud_alpha <= 0)
        {
            start_hud->sprite.color.a = 0;
            dead_hud_check = false;
        }
    }

}

bool Player::isWin()
{
    return Win;
}

void Player::SetWin(bool win_flag)
{
    Win = win_flag;
}

void Player::PlayWinSound(float dt)
{
    if(Win_Sound_check)
    {
        sound_play_time += dt;
        if (sound_play_time >= 1.0f)
        {
            sound_list["Clear"]->sound.SetVolume(Application::setting.volume);
            sound_list["Clear"]->sound.Play();
            Win_Sound_check = false;
            sound_play_time = 0.0f;
        }
    }
}

void Player::SetLose(bool lose_flag)
{
    this->customPhysics.ActivePhysics(false);
    dead_animation = lose_flag;
    ++Application::data.DeadNum;
    if(Application::data.DeadNum >= 100)
    {
        Application::data.DeadNum = 100;
    }
    sound_list["Die"]->sound.SetVolume(Application::setting.volume);
    sound_list["Die"]->sound.Play();
}

void Player::PlayBounce()
{
    sound_list["Bounce"]->sound.SetVolume(Application::setting.volume);
    sound_list["Bounce"]->sound.Play();
}

CustomBaseObject* Player::MakeSound(std::string file_name)
{
    CustomBaseObject * result = new CustomBaseObject();
    result->SetName(file_name.c_str());
    std::string path = "sound/" + file_name + ".wav";
    result->sound.LoadSound(path.c_str());
    result->IsPhysics = false;
    return result;
}

/*******************************
 Desc: Player movement control
 Param: dir
        >>keyboard input
 ******************************/
void Player::Move(int dir)
{
    if (dir == UP)
    {
        IsJump = true;
        IsGround = false;
        customPhysics.Getb2Body()->SetLinearVelocity(b2Vec2(customPhysics.Getb2Body()->GetLinearVelocity().x, 35.0f));
        MakeDust(0, 4, 75, 100);
    }
    if (dir == UP_CANCEL)
    {
        if (customPhysics.Getb2Body()->GetLinearVelocity().y > 0)
        {
            //std::cout<<"HI"<<std::endl;
            customPhysics.Getb2Body()->SetLinearVelocity(b2Vec2(customPhysics.Getb2Body()->GetLinearVelocity().x,
                customPhysics.Getb2Body()->GetLinearVelocity().y / 2));
        }
    }
    if (dir == RIGHT)
    {
        sprite.flip = SDL_FLIP_NONE;
        shouldAfterimageFlip = false;
        b2Vec2 velocity = customPhysics.Getb2Body()->GetLinearVelocity();
        if (!IsGround && Support::Color::Compare_Color(sprite.color, Support::Color::BLUE))
        {
            customPhysics.Getb2Body()->ApplyForceToCenter(b2Vec2((Speed - velocity.x) / 2.0f, 0.0f), true);
        }
        else
        {
            customPhysics.Getb2Body()->ApplyForceToCenter(b2Vec2(Speed - velocity.x, 0.0f), true);
        }
    }
    if (dir == LEFT)
    {
        sprite.flip = SDL_FLIP_HORIZONTAL;
        shouldAfterimageFlip = true;
        b2Vec2 velocity = customPhysics.Getb2Body()->GetLinearVelocity();
        if (!IsGround && Support::Color::Compare_Color(sprite.color, Support::Color::BLUE))
        {
            customPhysics.Getb2Body()->ApplyForceToCenter(b2Vec2((-Speed - velocity.x) / 2.0f, 0.0f), true);
        }
        else
        {
            customPhysics.Getb2Body()->ApplyForceToCenter(b2Vec2(-Speed - velocity.x, 0.0f), true);
        }
    }

}

void Player::Update(float dt)
{
    if (dead_animation)
    {
        for (auto i : afterImage)
        {
            i->sprite.color.a = 0;
        }

        rope_obj_->sprite.color.a = 0;

        dead_hud->transform.position = this->transform.position;

        dead_sceen_second += dt;
        dead_hud->sprite.color.a = 255;
        this->sprite.color.a = 0;
        
        if (dead_sceen_second >= 1)
        {
            state->m_game->Restart();
            dead_animation = false;
        }

        return;
    }

    StartMapFunc();
    PlayWinSound(dt);


    map->Update(dt);

    Rope_Detection();

    Rope_Draw();
    // using ray making the rope
    MousePos = Support::GetWorldMousePos(level_, player_camera);

    ray->callback->m_fixture = NULL;
    ray->Cast_Ray(this->customPhysics.Getb2Body()->GetPosition(), MousePos, level_);

    rope_attach = false;
    canRopeBeUsed = false;
    if (ray->callback->m_fixture && !isRopeActive)
    {
        if (ray->callback->m_fixture->GetBody())
        {
            if (ray->callback->m_fixture->GetBody()->GetUserData())
            {
                b2Vec2 distance_vector = this->customPhysics.Getb2Body()->GetTransform().p - ray->callback->m_point;
                float32 distance = std::sqrt((distance_vector.x*distance_vector.x) + (distance_vector.y*distance_vector.y));
                if (distance <= 15 && distance >= 3)
                {
                    if (this->customPhysics.Getb2Body()->GetTransform().p.y < ray->callback->m_point.y)
                    {
                        if (rope == NULL)
                        {
                            canRopeBeUsed = true;
                            if (State::m_input->IsTriggered(SDL_BUTTON_LEFT) && !State::m_input->IsPressed((MouseCode)PLAYER_KEY::CHANGE_COLOR))
                            {
                                sound_list["Connect"]->sound.SetVolume(Application::setting.volume);
                                sound_list["Connect"]->sound.Play();
                                CustomBaseObject* temp_obj = static_cast<CustomBaseObject *>(ray->callback->m_fixture->GetBody()->GetUserData());
                                b2Vec2 temp_position = ray->callback->m_point - temp_obj->customPhysics.Getb2Body()->GetPosition();

                                rope = new Rope(this, temp_obj, temp_position, distance, current_level_physics);

                                rope->save_rope_obj = temp_obj;
                                rope->rope_set_anchor_position = temp_position;

                                b2Vec2 temp = rope->save_rope_obj->customPhysics.Getb2Body()->GetTransform().p + rope->rope_set_anchor_position;

                                if ((int)temp.y >= (int)this->customPhysics.Getb2Body()->GetTransform().p.y)
                                {
                                    rope->rope_render_position.x = rope->rope_set_anchor_position.x * PIXELS_TO_METER;
                                    rope->rope_render_position.y = rope->rope_set_anchor_position.y * PIXELS_TO_METER;

                                    map->IsActive = false;
                                    rope_attach = true;
                                    isRopeActive = true;
                                }
                                else
                                {
                                    rope->DeleteRope(current_level_physics, &rope);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    customPhysics.GetFixture()->SetRestitution(0.0f);    //box2d makes restitution 0.1 after update
    customPhysics.Getb2Body()->SetLinearDamping(0.9f);
    /***************************************
    >>Color Change
    ***************************************/
    //color change done by calculating the coordinate of the mouse location
    //and which quadrant that it is located.

    if (State::m_input->IsPressed((MouseCode)PLAYER_KEY::CHANGE_COLOR))
    {
        rope_attach = true;
        //FIX THIS. SOUND IS TOO LONG AND THERE IS NO WAY TO PAUSE THE MUSIC
                //WITHOUT LARGE FIX AND NO SOUND COMES OUT WHEN THIS IS PLAYING.
        //sound_hudOn->sound.Play();
        hud->sprite.color.a = 150;
    }
    else
    {
        hud->sprite.color.a = 0;
    }
    if(State::m_input->IsReleased((MouseCode)PLAYER_KEY::CHANGE_COLOR))
    {
        b2Vec2 mousePos = Support::GetMousePos(state);

		//*****************Does not need anymore
        customPhysics.ActivePhysics(false);
        customPhysics.ActivePhysics(true);

        if (mousePos.x > 0 && mousePos.y > 0)
        {
            sprite.color = Support::Color::YELLOW;
            Rainbow[0] = true;
        }
        if (mousePos.x < 0 && mousePos.y > 0)
        {
            sprite.color = Support::Color::RED;
            Rainbow[1] = true;
            customPhysics.Getb2Body()->SetLinearVelocity(customPhysics.Getb2Body()->GetLinearVelocity());
        }
        if (mousePos.x < 0 && mousePos.y < 0)
        {
            sprite.color = Support::Color::BLUE;
            Rainbow[2] = true;
        }
        if (mousePos.x > 0 && mousePos.y < 0)
        {
            sprite.color = Support::Color::GREEN;
            Rainbow[3] = true;
        }
        sound_list["Change Color"]->sound.SetVolume(Application::setting.volume);
        sound_list["Change Color"]->sound.Play();
    }

    if(!Application::data.isRainbow)
    {
        for(int i = 0; i < 4; ++i)
        {
            if(!Rainbow[i])
            {
                break;
            }
            if(i == 3)
            {
                Application::data.isRainbow = true;
            }
        }
    }

    for (auto i = CollisionList.begin(); i != CollisionList.end();)
    {
        Item * itemptr = dynamic_cast<Item *>(static_cast<CustomBaseObject *>(*i));
        if (itemptr)
        {
            if (itemptr->Event)
            {
                itemptr->Event(itemptr->Param);
                return;
            }
            i = CollisionList.erase(i);
        }
        else
        {
            ++i;
        }
    }

    if (rope != NULL && map->IsActive == false)
    {
        if (State::m_input->IsPressed((SDL_Scancode)PLAYER_KEY::ROPE_INCREASE))
        {
            if (rope->GetRopeInfo() != NULL)
            {
                if (rope->GetRopeInfo()->GetLength() >= 3)
                {
                    rope->GetRopeInfo()->SetLength(rope->GetRopeInfo()->GetLength() - 0.2f);
                }
            }
        }
        if (State::m_input->IsPressed((SDL_Scancode)PLAYER_KEY::ROPE_DECREASE))
        {
            if (rope->GetRopeInfo() != NULL)
            {

                if (rope->GetRopeInfo()->GetLength() <= 15)
                {
                    rope->GetRopeInfo()->SetLength(rope->GetRopeInfo()->GetLength() + 0.2f);
                }
            }
        }

    }

    if (!rope_attach)
    {
        if (isRopeActive)
        {
            if (State::m_input->IsTriggered(SDL_BUTTON_LEFT))
            {
                if (rope->GetRopeInfo() != NULL)
                {
                    rope->DeleteRope(current_level_physics, &rope);
                    isRopeActive = false;
                }
            }
        }
    }

    /***************************************
    >>Player Movement Control
    ***************************************/
    if (!map->IsActive)
    {
        if (customPhysics.Getb2Body()->GetLinearVelocity().y <= 0)
        {
            IsJump = false;
        }

        //std::cout<<IsGround<<std::endl;

        //when jumping...
        if (State::m_input->IsPressed((SDL_Scancode)PLAYER_KEY::JUMP) && IsGround && !IsJump)
        {
            if (!isRopeActive)
            {
                Move(UP);
                sound_list["Jump"]->sound.SetVolume(Application::setting.volume);
                sound_list["Jump"]->sound.Play();
            }
        }
        //when jumping...
        if (State::m_input->IsTriggered((SDL_Scancode)PLAYER_KEY::JUMP))
        {
            //load jumping animation
            sprite.Free();
            sprite.LoadImage("texture/LeonJumping_M.png", State::m_renderer);
            sprite.activeAnimation = true;
            sprite.SetFrame(2);
            sprite.SetSpeed(15.0f);

            if (!isRopeActive)
            {	//Logic for wall jump
                if (Support::Color::Compare_Color(sprite.color, Support::Color::YELLOW))
                {
                    if (!IsAlarm("Foot"))
                    {
                        if (IsAlarm("LeftArm"))
                        {
                            IsJump = true;
                            customPhysics.Getb2Body()->SetLinearVelocity(b2Vec2(15.0f, 25.0f));
                        }
                        if (IsAlarm("RightArm"))
                        {
                            IsJump = true;
                            customPhysics.Getb2Body()->SetLinearVelocity(b2Vec2(-15.0f, 25.0f));
                        }
                    }
                }
            }
        }

        //use static sprite when leon is not jumping/moving
        if (!State::m_input->IsPressed((SDL_Scancode)PLAYER_KEY::JUMP)
            && !State::m_input->IsPressed((SDL_Scancode)PLAYER_KEY::MOVE_LEFT)
            && !State::m_input->IsPressed((SDL_Scancode)PLAYER_KEY::MOVE_RIGHT))
        {
            sprite.Free();
            sprite.LoadImage("texture/Leon_M.png", State::m_renderer);
            sprite.activeAnimation = false;
        }

        //jump cancel
        if (State::m_input->IsReleased((SDL_Scancode)PLAYER_KEY::JUMP) && IsJump)
        {
            if (!isRopeActive)
            {
                Move(UP_CANCEL);
            }
        }

        //use animation when moving
        if (State::m_input->IsTriggered((SDL_Scancode)PLAYER_KEY::MOVE_RIGHT) || State::m_input->IsTriggered((SDL_Scancode)PLAYER_KEY::MOVE_LEFT))
        {
            sprite.Free();
            sprite.LoadImage("texture/LeonWalking_M.png", State::m_renderer);
            sprite.activeAnimation = true;
            sprite.SetFrame(4);
            sprite.SetSpeed(25.0f);
        }
        //movement control
        if (State::m_input->IsPressed((SDL_Scancode)PLAYER_KEY::MOVE_LEFT)) { Move(LEFT); }
        if (State::m_input->IsPressed((SDL_Scancode)PLAYER_KEY::MOVE_RIGHT)) { Move(RIGHT); }

        UpdateColor();
        //is Leon touching the ground or not
        if (IsAlarm("Foot"))
        {
            if(Support::Color::Compare_Color(sprite.color, Support::Color::RED) && this->customPhysics.GetVelocity().y != 0)
            {
            }
            else
            {
                IsGround = true;
            }
        }
        else
        {
            IsGround = false;
        }

        if (IsAlarm("RightArm") || IsAlarm("LeftArm"))
        {
            //while Leon is yellow/wall climbing...
            if (Support::Color::Compare_Color(sprite.color, Support::Color::YELLOW))
            {
                //...if Leon is wallcliming on the left side of wall
                if (IsAlarm("RightArm") && !IsGround && state->m_input->IsTriggered((SDL_Scancode)PLAYER_KEY::JUMP))
                {
                    MakeDust(2, 0, 35, 100);
                }
                //...if Leon is wallcliming on the right side of wall
                if (IsAlarm("LeftArm") && !IsGround && state->m_input->IsTriggered((SDL_Scancode)PLAYER_KEY::JUMP))
                {
                    MakeDust(-2, 0, 35, 100);
                }
                //physics settings for wallclimb
            }
        }
        if (Support::Color::Compare_Color(sprite.color, Support::Color::YELLOW))
        {
            customPhysics.GetFixture()->SetFriction(1.0f);
        }
        else
        {
            //physics settings for not-wallclimbdddd
            customPhysics.GetFixture()->SetFriction(0.2f);
        }
    }
    //if color is blue, make afterImage
    if (Support::Color::Compare_Color(sprite.color, Support::Color::BLUE))
    {
        MakeAfterImage(0, 0, 32, 32);
    }

    //delete dust sprite after set time period
    for (auto i = particle.begin(); i != particle.end();)
    {
        (*i)->Update(dt);
        if (!(*i)->Life)
        {
            level_->RemoveObject(*i);
            delete *i;
            *i = NULL;
            i = particle.erase(i);
        }
        else
        {
            ++i;
        }
    }

    //delete after-image sprite after set time period
    for (auto i = afterImage.begin(); i != afterImage.end();)
    {
        (*i)->Update(dt);
        if (!(*i)->Life)
        {
            level_->RemoveObject(*i);
            delete *i;
            *i = NULL;
            i = afterImage.erase(i);
        }
        else
        {
            ++i;
        }
    }


}