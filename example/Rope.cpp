/******************************************************************************/
/*!
\file   Rope.cpp
\project ColorLeon
\author primary : Choi Jinhyun

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "Rope.h"
#include "engine/State.h"
#include <iostream>
#include "Player.h"

Rope::Rope(Player* player, CustomBaseObject* object, b2Vec2 set_anchor, float32 lenght, b2World* myWorld)
{

    rope_joint_def.bodyA = object->customPhysics.Getb2Body();
    rope_joint_def.bodyB = player->customPhysics.Getb2Body();
    rope_joint_def.length = lenght;

    //b2Vec2 temp = set_anchor; //- object->customPhysics.Getb2Body()->GetTransform().p;

    rope_joint_def.localAnchorA.Set(set_anchor.x, set_anchor.y);
    rope_joint_def.localAnchorB.Set(0.0f, 0.0f);
    rope_joint_def.collideConnected = true;

    rope_joint = (b2DistanceJoint*)myWorld->CreateJoint(&rope_joint_def);
}

void Rope::DeleteRope(b2World* myWorld, Rope** original)
{
    myWorld->DestroyJoint(rope_joint);
    delete *original;
    *original = NULL;
}


float32 Rope::Distance(Player* player, CustomBaseObject* object)
{
    float32 x_coordinate = player->customPhysics.Getb2Body()->GetTransform().p.x - object->customPhysics.Getb2Body()->GetTransform().p.x;
    float32 y_coordinate = player->customPhysics.Getb2Body()->GetTransform().p.y - object->customPhysics.Getb2Body()->GetTransform().p.y;
    float32 distance = std::sqrt((x_coordinate*x_coordinate) + (y_coordinate*y_coordinate));

    return distance;
}

b2Vec2 Rope::Specific_Point(Player* player, b2Vec2 point)
{
    //b2Vec2 point1 = player->customPhysics.Getb2Body()->GetTransform().p;
    //b2Vec2 point2 = object->customPhysics.Getb2Body()->GetTransform().p;
    //b2Vec2 specific_point = point1 - point2;
    b2Vec2 specific_point = b2Vec2(point.x - player->transform.position.x, point.y - player->transform.position.y);
    return specific_point;
}

float32 Rope::Theta(b2Vec2 point)
{
    float32 theta = std::atan2f(point.y , point.x);
    theta = Math::To_Degree(theta);
    return theta+90;
}

b2DistanceJoint* Rope::GetRopeInfo()
{
    return rope_joint;
}


void Rope::Render(CustomBaseObject* rope_obj, Player* player)
{
    if (player->isRopeActive)
    {

        rope_render_position.x = save_rope_obj->transform.position.x + rope_set_anchor_position.x * PIXELS_TO_METER;
        rope_render_position.y = save_rope_obj->transform.position.y + rope_set_anchor_position.y * PIXELS_TO_METER;

        b2Vec2 each_vector = Specific_Point(player, rope_render_position);
        float32 abc = each_vector.Length();

        rope_obj->sprite.color = SDL_Color{ 255,255,255,255 };
        rope_obj->transform.position.Set((rope_render_position.x + player->transform.position.x) / 2.0f
            , (rope_render_position.y + player->transform.position.y) / 2.0f, 0.0f);

        rope_obj->transform.SetScale(4.0f, abc);
        rope_obj->transform.rotation = Theta(each_vector);

    }
    else
    {
        rope_obj->sprite.color = SDL_Color{ 255,255,255,0 };
    }
}
