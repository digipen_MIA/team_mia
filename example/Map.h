/******************************************************************************/
/*!
\file   Map.h
\project ColorLeon
\author primary : Choi Jinhyun

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "CustomBaseObject.h"

class Player;

class Map : public CustomBaseObject
{
private:
    //bool IsActive = false;
    State * Level;
    Player * player_cog;
    b2Vec2 MapPos = b2Vec2(0, 0);
    float MapScale = 1.0f;
    float Time = 0.0f;
public:
    bool IsActive = false;
    ~Map();
    std::vector<CustomBaseObject *> map_object;
    void SetMapData(std::vector<CustomBaseObject *> objectlist, State * state, Player * player);
    void Update(float delta_time);
    bool GetActiveStatus();
};

