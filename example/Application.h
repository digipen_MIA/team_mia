/******************************************************************************/
/*!
\file   Application.h
\project ColorLeon
\author primary : Choi Jinhyun, secondary : Kim Minsuk

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "SDL2/SDL.h"
#include "Game.h"
#include "engine/Input.h"

void CheckSDLError(int line = -1);

class Application
{
public:
    Application() = default;
    bool CanStartUp();	//Initializes the application if no errors
    bool IsDone() const { return done; }
    void Update();
    ~Application();

    void CreateConsole();
    void DeleteConsole();
    void SetFullscreen(bool toggle);
    void GetScreenSize();

    void loadSetting();
    void saveSetting();

    // This prevents new call on more than 1 instance.
public:
    Application(const Application&) = delete;
    Application(Application&&) = delete;
    Application& operator=(const Application&) = delete;
    Application& operator=(Application&&) = delete;

    struct Data_Info
    {
        int LockLevel = 1;
        bool * isWhiteMan;
        bool isMadMan = false;
        bool isRainbow = false;
        int DeadNum = 0;
    };
    static Data_Info data;

    struct Setting_Info
    {
        bool isFullscreen = false;
        int volume = 64;
        bool isMute = false;
    };
    static Setting_Info setting;

    static int WheelEvent;
    static int LastLevel;
private:

    SDL_Window * window = nullptr;
    SDL_Renderer *renderer = nullptr;
    bool done = true;	//If the application is done
    int  ratioWidth, ratioHeight;

    //For keeping track of time
    Uint32 ticks_last = 0;
    int frame_count = 0;
    float frame_time = 0.0f;

    // Screen size
    int m_screenWidth = 0, m_screenHeight = 0;

    Game game{};	//Holds one instance of our game
    Input m_input{};//Input class

    int FindLastLevel();
};