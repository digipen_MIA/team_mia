/******************************************************************************/
/*!
\file   LevelSelect.cpp
\project ColorLeon
\author primary : Kim Minsuk, secondary : Choi Jinhyun, Kim Hyunsung, Kim Hyunseok

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "CommonLevel.h"
#include "Rope.h"
#include "CustomCollision.h"
#include "EngineSupport.h"
#include "RayCast.h"
#include "Item.h"
#include "Application.h"

enum class OBJNUM {CENTER_RECT, TEXT, LEON};

void LevelSelect::Initialize()
{
    LevelNum = 1;
    MoveLevel = false;
    IsJump = false;

    Application::LastLevel = FindLastLevel();

    m_backgroundColor = { 0Xff, 0xff, 0xff };

    camera.Initialize(int(State::m_width), int(State::m_height));
    camera.position.Set(0, 0, 0.0f);

    mainFont = TTF_OpenFont("font/FONTHYUNSUNG.ttf", 48);

    m_useBuiltInPhysics = false;

    b2Vec2 gravity(0.f, -250.f);
    SetCustomPhysicsWorld(gravity);

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Center_Rectangle");
    Object.back()->transform.position.Set(0.0f, -120.0f, 0.0f);
    Object.back()->transform.SetScale(280.0f, 280.0f);
    Object.back()->sprite.LoadImage("texture/DISK.png", m_renderer);
    Object.back()->sprite.color = Support::Color::RED;
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::CIRCLE;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Text");
    Object.back()->transform.position.Set(5.0f, -130.0f, 0.0f);
    Object.back()->transform.SetScale(230.0f, 180.0f);
    Object.back()->text.SetText(m_renderer, "LEVEL 1", mainFont);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Color-Leon");
    Object.back()->transform.position.Set(0.0f, 85.0f, 0.0f);
    Object.back()->transform.SetScale(100.0f, 120.0f);
    Object.back()->sprite.LoadImage("texture/LeonWalking_M.png", m_renderer);
    Object.back()->sprite.color = SDL_Color{ 255, 0, 0, 255 };
    Object.back()->customPhysics.bodyType = CustomPhysics::DYNAMIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.back()->sprite.SetFrame(4);
    Object.back()->sprite.SetSpeed(20.0f);
    Object.back()->sprite.activeAnimation = true;

    Object.push_back(new Button());
    Object.back()->SetName("Back");
    Object.back()->transform.position.Set(-450.f, 50.0f, 0.0f);
    Object.back()->transform.SetScale(80.0f, 80.0f);
    Object.back()->sprite.LoadImage("texture/BACK.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    dynamic_cast<Button *>(Object.back())->SetColor(SDL_Color{ 0xff, 0xff, 0, 0xff });
    Object.back()->SetData();
    dynamic_cast<Button *>(Object.back())->SetWorld(this, &camera);
    dynamic_cast<Button *>(Object.back())->SetClickEvent(Support::Event::MoveMainMenu, this);
    Object.back()->sprite.isHud = true;

    wallPaper.SetName("WallPaper");
    wallPaper.transform.position.Set(0.0f, 0.0f, 0.0f);
    wallPaper.transform.SetScale(m_width-125, m_height-95);
    wallPaper.sprite.color = SDL_Color{ 255, 255, 255, 255 };
    wallPaper.sprite.LoadImage("texture/TitleImage[animated].png", m_renderer);
    wallPaper.sprite.activeAnimation = true;
    wallPaper.sprite.SetFrame(5);
    wallPaper.sprite.SetSpeed(10.0f);
    wallPaper.IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("PressWSprite");
    Object.back()->transform.position.Set(0.0f, -275.0f, 0.0f);
    Object.back()->transform.SetScale(200.0f, 20.0f);
    Object.back()->sprite.color = SDL_Color{ 255, 255, 255, 255 };
    Object.back()->sprite.LoadImage("texture/PressW.png", m_renderer);
    Object.back()->sprite.activeAnimation = true;
    Object.back()->sprite.SetFrame(2);
    Object.back()->sprite.SetSpeed(8.0f);
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("ConsoleStyleHud");
    Object.back()->transform.SetScale(State::m_width, State::m_height);
    Object.back()->sprite.color = SDL_Color{ 255,255,255,255 };
    Object.back()->sprite.LoadImage("texture/ConsoleStyleHUD.png", State::m_renderer);
    Object.back()->sprite.isHud = true;
    Object.back()->IsPhysics = false;

    AddObject(&wallPaper);
    for (auto i : Object)
    {
        AddObject(i);
        if(i->IsPhysics)
        {
            AddCustomPhysicsComponent(i);
        }
    }

    Cursor = Support::MakeCursor(this);
}

void LevelSelect::Update(float dt)
{
    Support::UpdateCursor(this, Cursor);

    if(ShouldGo != 0)
    {
        Object.at((int)OBJNUM::CENTER_RECT)->transform.rotation += 9.0f * ShouldGo;
        Object.at((int)OBJNUM::CENTER_RECT)->transform.rotation = std::round(Object.at((int)OBJNUM::CENTER_RECT)->transform.rotation);
        Object.at((int)OBJNUM::TEXT)->transform.rotation = Object.at((int)OBJNUM::CENTER_RECT)->transform.rotation;
        if(Object.at((int)OBJNUM::CENTER_RECT)->transform.rotation == 180.0f || Object.at((int)OBJNUM::CENTER_RECT)->transform.rotation == -180.0f)
        {
            LevelNum += ShouldGo;
            if(LevelNum < 1)
            {
                LevelNum = Application::data.LockLevel;
            }
            if (LevelNum > Application::data.LockLevel)
            {
                LevelNum = 1;
            }
            Object.at((int)OBJNUM::TEXT)->text.SetText(m_renderer, GetString().c_str(), mainFont);
            ShouldGo = 0;
            Object.at((int)OBJNUM::CENTER_RECT)->transform.rotation = 0.0f;
            Object.at((int)OBJNUM::TEXT)->transform.rotation = 0.0f;
            SDL_Color color = Object.at((int)OBJNUM::CENTER_RECT)->sprite.color;
            while (Support::Color::Compare_Color(Object.at((int)OBJNUM::CENTER_RECT)->sprite.color, color))
            {
                Object.at((int)OBJNUM::CENTER_RECT)->sprite.color = Support::Rand::GetRandomColor();
            }
            Object.at((int)OBJNUM::LEON)->sprite.color = Object.at((int)OBJNUM::CENTER_RECT)->sprite.color;
        }
    }
    else
    {
        Object.at((int)OBJNUM::LEON)->sprite.SetCurrentFrame(true, 0);
    }

    for (auto i : Object)
    {
        i->Update(dt);
    }

    if(m_input->IsTriggered(SDL_SCANCODE_D) && !MoveLevel)
    {
        Object.at((int)OBJNUM::LEON)->sprite.SetCurrentFrame(false, 0);
        Object.at((int)OBJNUM::LEON)->sprite.flip = SDL_FLIP_NONE;
        ShouldGo = 1;
    }
    if (m_input->IsTriggered(SDL_SCANCODE_A) && !MoveLevel)
    {
        Object.at((int)OBJNUM::LEON)->sprite.SetCurrentFrame(false, 0);
        Object.at((int)OBJNUM::LEON)->sprite.flip = SDL_FLIP_HORIZONTAL;
        ShouldGo = -1;
    }

    if (m_input->IsTriggered(SDL_SCANCODE_W))
    {
        MoveLevel = true;
    }

    Object.at((int)OBJNUM::LEON)->customPhysics.AddForce(-Object.at(2)->transform.position.x * 0.2f, 0.0f);

    if (MoveLevel)
    {
        if (!IsJump)
        {
            Object.at((int)OBJNUM::LEON)->customPhysics.SetVelocity(0.0f, 100.0f);
            Object.at((int)OBJNUM::LEON)->customPhysics.pOwnerTransform->position.y += 1.0f;
            IsJump = true;
        }
        if (IsJump)
        {
            if (std::round(Object.at((int)OBJNUM::LEON)->transform.position.y) <= 20)
            {
                switch (LevelNum)
                {
                case 1:
                    m_game->Change(LV_MainLevel);
                    break;
                case 2:
                    m_game->Change(LV_Level1);
                    break;
                default:
                    m_game->Change(LV_LevelBase);
                    LevelBase::LevelNum = LevelNum - 2;
                    break;
                }
                MoveLevel = false;
                IsJump = false;
            }
        }
    }

    // Must be one of the last functions called at the end of State Update
    UpdateCustomPhysics(dt);	// Update custom physics system here
    Render(dt);
}

void LevelSelect::Close()
{
    RemoveCustomPhysicsWorld();
    // Wrap up state
    ClearBaseState(false);

    for (auto i : Object)
    {
        delete i;
        i = NULL;
    }
    Object.clear();

    delete Cursor;
}

std::string LevelSelect::GetString()
{
    std::string result;
    result = "LEVEL " + std::to_string(LevelNum);
    return result;
}

int LevelSelect::FindLastLevel()
{
    int number = 1;
    while (true)
    {
        int objNum;
        bool IsPlayerThere = false;
        std::string tostring = std::to_string(number);
        std::string filename = "map/LEVEL" + tostring + ".txt";
        const char *filenames = filename.c_str();
        float temp;
        int objecttype;
        FILE* pFile = 0;
        fopen_s(&pFile, filenames, "rt");
        if (pFile) {
            fscanf_s(pFile, "%d", &objNum);
            for (int i = 0; i < objNum; ++i)
            {
                fscanf_s(pFile, "%f", &temp);
                fscanf_s(pFile, "%f", &temp);
                fscanf_s(pFile, "%f", &temp);
                fscanf_s(pFile, "%f", &temp);
                fscanf_s(pFile, "%f", &temp);
                fscanf_s(pFile, "%f", &temp);
                fscanf_s(pFile, "%d", &objecttype);
                if (objecttype == (int)ObjectType::PLAYER)
                {
                    IsPlayerThere = true;
                }
            }
            fclose(pFile);
        }
        if (!IsPlayerThere)
        {
            break;
        }
        ++number;
    }
    return number + 1;
}
