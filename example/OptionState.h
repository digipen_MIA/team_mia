/******************************************************************************/
/*!
\file   OptionState.h
\project ColorLeon
\author primary : Choi Jinhyun

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "engine\State.h"
#include "CustomBaseObject.h"

class Option : public State
{
	friend class Game;

protected:

        Option() : State() {};
	~Option() override {};

	// Derived initialize function
	void Initialize() override;
	// Derived Update function
	void Update(float dt) override;
	// Derived Close function
	void Close() override;

public:
    void DelayForResume();
    void DelayForMainMenu();

    void DealTheResume();
    static bool PrintHowToPlay;

private:

    CustomBaseObject	    wallPaper;
    CustomBaseObject        volumebar;

    CustomBaseObject        fullscrean_text;
    CustomBaseObject        mute_text;
    CustomBaseObject        volume_text;

    CustomBaseObject        howtoplay_print;
    CustomBaseObject	    blank_paper;

    std::vector<Button *>   button;

    bool ShouldResume = false;
    bool ShouldGoMainMenu = false;


    CustomBaseObject * Cursor;
};

