/******************************************************************************/
/*!
\file   SettingLevel.cpp
\project ColorLeon
\author primary : Kim Minsuk, secondary : Choi Jinhyun

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "CommonLevel.h"
#include "Rope.h"
#include "CustomCollision.h"
#include "EngineSupport.h"
#include "RayCast.h"
#include "Item.h"

#include "Application.h"

static int tick;

void SettingLevel::Initialize()
{
    m_backgroundColor = { 0Xff, 0xff, 0xff };

    camera.Initialize(int(State::m_width), int(State::m_height));
    camera.position.Set(0, 0, 0.0f);

    //backgroundMusic.LoadMusic("sound/My Song.wav");
    //backgroundMusic.Play();
    //backgroundMusic.SetVolume(Application::setting.volume);

    m_useBuiltInPhysics = false;

    b2Vec2 gravity(0.f, 0.f);      // 0, 200
    SetCustomPhysicsWorld(gravity);

    button.push_back(new Button());
    button.back()->SetName("FullScreen");
    button.back()->transform.position.Set(-450.0f, -250.0f, 0.0f);
    button.back()->transform.SetScale(80.0f, 80.0f);
    if(Application::setting.isFullscreen)
    {
        button.back()->sprite.LoadImage("texture/On.png", m_renderer);
    }
    else
    {
        button.back()->sprite.LoadImage("texture/Off.png", m_renderer);
    }
    button.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    button.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    button.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &button.back()->transform);
    button.back()->SetColor(SDL_Color{ 0xff, 0xff, 0xff, 0xff });
    button.back()->SetData();
    button.back()->SetWorld(this, &camera);
    button.back()->SetClickEvent(Support::Event::ToggleFullScreen, button.back());

    fullscrean_text.SetName("fullscrean_text");
    fullscrean_text.transform.position.Set(-450.0f, -150.0f, 0.0f);
    fullscrean_text.transform.SetScale(320.0f, 80.0f);
    fullscrean_text.sprite.color = SDL_Color{ 255, 255, 255, 255 };
    fullscrean_text.sprite.LoadImage("texture/FULLSCREEN.png", m_renderer);
    fullscrean_text.IsPhysics = false;

    button.push_back(new Button());
    button.back()->SetName("VolumeBar");
    button.back()->transform.position.Set(0.0f, -250.0f, 1.0f);
    button.back()->transform.SetScale(40.0f, 80.0f);
    button.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    button.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    button.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    button.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &button.back()->transform);
    button.back()->SetDragAndDropLimit(10.0f);
    button.back()->SetType(ButtonType::DRAG_DROP);
    button.back()->SetColor(SDL_Color{ 255, 0, 0, 0xff });
    button.back()->SetData();
    button.back()->SetStore(&Application::setting.volume, b2Vec2(0.0f, 128.0f));
    button.back()->SetWorld(this, &camera);

    volume_text.SetName("volume_text");
    volume_text.transform.position.Set(0.0f, -150.0f, 0.0f);
    volume_text.transform.SetScale(320.0f, 40.0f);
    volume_text.sprite.color = SDL_Color{ 255, 255, 255, 255 };
    volume_text.sprite.LoadImage("texture/VOLUME.png", m_renderer);
    volume_text.IsPhysics = false;

    volumebar.SetName("VolumeBar_Long");
    volumebar.transform.position.Set(0.0f, -250.0f, 0.0f);
    volumebar.transform.SetScale(320.0f, 40.0f);
    volumebar.sprite.color = SDL_Color{ 255, 255, 255, 255 };
    volumebar.sprite.LoadImage("texture/rect.png", m_renderer);
    volumebar.IsPhysics = false;

    button.push_back(new Button());
    button.back()->SetName("MuteSounds");
    button.back()->transform.position.Set(450.0f, -250.0f, 0.0f);
    button.back()->transform.SetScale(80.0f, 80.0f);
    if (Application::setting.isMute)
    {
        button.back()->sprite.LoadImage("texture/On.png", m_renderer);
    }
    else
    {
        button.back()->sprite.LoadImage("texture/Off.png", m_renderer);
    }    
    button.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    button.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    button.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &button.back()->transform);
    button.back()->SetColor(SDL_Color{ 0xff, 0xff, 0xff, 0xff });
    button.back()->SetData();
    button.back()->SetWorld(this, &camera);
    button.back()->SetClickEvent(Support::Event::ToggleMute, button.back());

    mute_text.SetName("mute_text");
    mute_text.transform.position.Set(450.0f, -150.0f, 0.0f);
    mute_text.transform.SetScale(300.0f, 40.0f);
    mute_text.sprite.color = SDL_Color{ 255, 255, 255, 255 };
    mute_text.sprite.LoadImage("texture/MUTE.png", m_renderer);
    mute_text.IsPhysics = false;

    button.push_back(new Button());
    button.back()->SetName("Back");
    button.back()->transform.position.Set(0.0f, -40.0f, 0.0f);
    button.back()->transform.SetScale(80.0f, 80.0f);
    button.back()->sprite.LoadImage("texture/BACK.png", m_renderer);
    button.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    button.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    button.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &button.back()->transform);
    button.back()->SetColor(SDL_Color{ 0xff, 0xff, 0x00, 0xff });
    button.back()->SetData();
    button.back()->SetWorld(this, &camera);
    button.back()->SetClickEvent(Support::Event::MoveMainMenu, this);
    button.back()->sprite.isHud = true;

    wallPaper.SetName("WallPaper");
    wallPaper.transform.position.Set(0.0f, 0.0f, 0.0f);
    wallPaper.transform.SetScale(m_width, m_height);
    wallPaper.sprite.color = SDL_Color{ 255, 255, 255, 255 };
    wallPaper.sprite.LoadImage("texture/TitleImage.png", m_renderer);
    wallPaper.IsPhysics = false;
    //WallPaper.sprite.isHud = true;

    AddObject(&wallPaper);
    AddObject(&volumebar);
    AddObject(&fullscrean_text);
    AddObject(&mute_text);
    AddObject(&volume_text);
    for (auto i : button)
    {
        AddObject(i);
        AddCustomPhysicsComponent(i);
    }

    Cursor = Support::MakeCursor(this);
}

void SettingLevel::Update(float dt)
{
    Support::UpdateCursor(this, Cursor);

    if(Application::setting.isMute)
    {
        backgroundMusic.SetVolume(0);
    }
    else
    {
        backgroundMusic.SetVolume(Application::setting.volume);
    }

    if(State::m_input->IsTriggered(SDL_SCANCODE_A))
    {
        Application::setting.volume = 128;
        backgroundMusic.SetVolume(Application::setting.volume);
    }
    if (State::m_input->IsTriggered(SDL_SCANCODE_S))
    {
        Application::setting.volume = 64;
        backgroundMusic.SetVolume(Application::setting.volume);
    }
    if (State::m_input->IsTriggered(SDL_SCANCODE_D))
    {
        Application::setting.volume = 0;
        backgroundMusic.SetVolume(Application::setting.volume);
    }

    for (auto i : button)
    {
        i->Update(dt);
    }

    // Must be one of the last functions called at the end of State Update
    UpdateCustomPhysics(dt);	// Update custom physics system here
    Render(dt);
}

void SettingLevel::Close()
{
    // Deallocate custom physics world
    RemoveCustomPhysicsWorld();

    // Wrap up state
    ClearBaseState(false);

    for (auto i : button)
    {
        delete i;
        i = NULL;
    }
    button.clear();

    delete Cursor;
}