/******************************************************************************/
/*!
\file   Player.h
\project ColorLeon
\author primary : Kim Hyunseok, secondary : Choi Jinhyun, Kim Minsuk

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "third_party/engine/Object.h"
#include "CustomBaseObject.h"
#include "Sensor.h"
#include <engine/State.h>
#include "Map.h"
#include <map>

enum class PLAYER_KEY
{
    MOVE_LEFT = SDL_SCANCODE_A,
    MOVE_RIGHT = SDL_SCANCODE_D,
    JUMP = SDL_SCANCODE_W,

    ROPE_INCREASE = SDL_SCANCODE_W,
    ROPE_DECREASE = SDL_SCANCODE_S,

    CHANGE_COLOR = /*SDL_SCANCODE_LSHIFT*/SDL_BUTTON_RIGHT,

    SHOW_MAP = SDL_SCANCODE_M,
    MAP_MOVE_LEFT = SDL_SCANCODE_A,
    MAP_MOVE_RIGHT = SDL_SCANCODE_D,
    MAP_MOVE_UP = SDL_SCANCODE_W,
    MAP_MOVE_DOWN = SDL_SCANCODE_S,
    MAP_ZOOM_IN = SDL_SCANCODE_EQUALS,
    MAP_ZOOM_OUT = SDL_SCANCODE_MINUS
};

class DustSprite : public CustomBaseObject
{
public:
    bool isYouDead;

    void SetUp()
    {
        transform.position.Set(//customPhysics.Getb2Body()->GetPosition().x,
                               //customPhysics.Getb2Body()->GetPosition().y,
            0, 0, 0);
        transform.SetScale(200, 200);
        sprite.color = { 255,0,0,255 };
        sprite.LoadImage("texture/ColorWheel.png", State::m_renderer);
        IsPhysics = false;
        isYouDead = false;
    }
};

class Rope;
class RayCast;
class State;

class Player : public CustomBaseObject
{
private:
    bool Win = false;
    bool Rainbow[4];

    std::map <std::string, CustomBaseObject *> sound_list;
public:
    int levelnumber;

    bool IsJump;
    bool IsGround;

    State* state = NULL;

    std::vector<Sensor*> sensorList;

    Player();
    ~Player();
    void SetData();
    void Move(int dir);

    void Update(float dt);
    void UpdateColor();

    void SetState(State* world);

    float Speed = 70.0f;

    /* for the ray cast*/
    /*****************************/
    Camera* player_camera;
    void Set_Player_Camera(Camera* temp_camera);
    b2Vec2 MousePos;
    RayCast * ray;
    State* level_;
    /*****************************/

    /* for the rope*/
    /*****************************/
    b2World* current_level_physics;
    Rope* rope = NULL;
    bool rope_attach = false;
    bool isRopeActive = false;
    bool canRopeBeUsed = false;
    void Rope_Draw();
    void Close_Player_Rope();
    void Rope_Detection();
    /*****************************/

        /* for after-image*/
        /*****************************/
    bool shouldAfterimageFlip;
    /*****************************/

    bool isHudOut = false;

    std::vector<void *> CollisionList;

    bool IsAlarm(const std::string &Name);

    void MakeDust(float xoffset, float yoffset, float sizeX, float sizeY);
    void MakeAfterImage(float xoffset, float yoffset, float sizeX, float sizeY);

    void SetMap(std::vector<CustomBaseObject *> objectlist);

    enum ControlInput
    {
        UP,
        UP_CANCEL,
        DOWN,
        LEFT,
        RIGHT
    };

    CustomBaseObject* rope_detection;
    CustomBaseObject* rope_obj_;
    CustomBaseObject* hud;
    CustomBaseObject* dead_hud;
    CustomBaseObject* start_hud;

    bool dead_hud_check = true;
    bool dead_animation = false;
    void StartMapFunc();

    float dead_sceen_second = 0;
    int start_hud_alpha = 255;

    Map * map;
    std::vector<CustomBaseObject*> particle;
    std::vector<CustomBaseObject*> afterImage;

    bool isWin();
    void SetWin(bool win_flag = true);

    static bool Win_Sound_check;
    float sound_play_time = 0;
    void PlayWinSound(float dt);

    void SetLose(bool lose_flag = true);

    void PlayBounce();

    CustomBaseObject * MakeSound(std::string file_name);
};


