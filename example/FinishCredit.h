/******************************************************************************/
/*!
\file   FinishCredit.h
\project ColorLeon
\author primary : Choi Jinhyun

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once

#include "engine\State.h"
#include "CustomBaseObject.h"

class FinishCredit : public State
{
    friend class Game;

public:
    SDL_Rect rect;

protected:
    FinishCredit() : State() {};
    ~FinishCredit() override {};

    // Derived initialize function
    void Initialize() override;
    // Derived Update function
    void Update(float dt) override;
    // Derived Close function
    void Close() override;
private:
    float close_time;
    float camera_y;
    // Objects which contains custom physics
    CustomBaseObject	    Credit_Papaer;

    CustomBaseObject * Cursor;
};