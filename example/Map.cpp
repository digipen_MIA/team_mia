/******************************************************************************/
/*!
\file   Map.cpp
\project ColorLeon
\author primary : Kim Minsuk

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "Map.h"
#include <engine/State.h>
#include "LevelBase.h"
#include "Player.h"
#include <iostream>
#include "Application.h"

Map::~Map()
{
    for(auto i : map_object)
    {
        delete i;
        i = NULL;
    }
    map_object.clear();
}

void Map::SetMapData(std::vector<CustomBaseObject *> objectlist, State * state, Player * player)
{
    player_cog = player;
    Level = state;
    SetName("MiniMap");
    transform.position.Set(0, 0, 0);
    transform.SetScale(State::m_width, State::m_height);
    sprite.color = SDL_Color{ 0, 0, 0, 0 };
    sprite.LoadImage("texture/rect.png", State::m_renderer);
    sprite.isHud = true;
    IsPhysics = false;

    map_object.push_back(new CustomBaseObject());
    map_object.back()->SetName("Map Composition");
    map_object.back()->transform.position.Set(player_cog->transform.position.x, player_cog->transform.position.y, 0);
    map_object.back()->transform.SetScale(player_cog->transform.GetScale().x, player_cog->transform.GetScale().y);
    map_object.back()->sprite.color = SDL_Color{ 255, 255, 255, 255 };
    map_object.back()->sprite.LoadImage("texture/LeonHead.png", State::m_renderer);
    map_object.back()->sprite.isHud = true;
    map_object.back()->IsPhysics = false;
    map_object.back()->Cog = player_cog;

    for (auto i : objectlist)
    {
        if(!i->IsPhysics)
        {
            continue;
        }
        CustomBaseObject * temp;
        temp = new CustomBaseObject();
        temp->SetName("Map Composition");
        temp->transform.position.Set(i->transform.position.x, i->transform.position.y, 0);
        temp->transform.SetScale(i->transform.GetScale().x, i->transform.GetScale().y);
        temp->sprite.color = i->sprite.color;
        if(i->GetName() == "Goal")
        {
            temp->sprite.LoadImage("texture/GateAnimation.png", State::m_renderer);
            temp->sprite.activeAnimation = true;
            temp->sprite.SetFrame(8);
            temp->sprite.SetSpeed(15.0f);
        }
        else
        {    
            if(i->customPhysics.bodyShape == CustomPhysics::CIRCLE)
            {
                temp->sprite.LoadImage("texture/circle.png", State::m_renderer);
            }
            else
            {
                temp->sprite.LoadImage("texture/rect.png", State::m_renderer);
            }
        }
        temp->sprite.isHud = true;
        temp->IsPhysics = false;
        temp->Cog = i;
        map_object.push_back(temp);
    }

    map_object.push_back(new CustomBaseObject());
    map_object.back()->SetName("Map Board");
    map_object.back()->transform.position.Set(0, 0, 0);
    map_object.back()->transform.SetScale(State::m_width, State::m_height);
    map_object.back()->sprite.color = SDL_Color{ 255, 255, 255, 255 };
    map_object.back()->sprite.LoadImage("texture/MapMode.png", State::m_renderer);
    map_object.back()->sprite.isHud = true;
    map_object.back()->IsPhysics = false;

    Level->AddObject(this);
    for(auto i : map_object)
    {
        Level->AddObject(i);
    }
}

void Map::Update(float delta_time)
{
    if (State::m_input->IsTriggered((SDL_Scancode)PLAYER_KEY::SHOW_MAP))
    {
        MapPos = b2Vec2(-map_object.at(0)->Cog->transform.position.x, -map_object.at(0)->Cog->transform.position.y);
        MapScale = 1.0f;
        IsActive = !IsActive;
    }
    if (IsActive && State::m_input->IsPressed((SDL_Scancode)PLAYER_KEY::MAP_MOVE_LEFT))
    {
        MapPos += b2Vec2(10.0f, 0.0f);
    }
    if (IsActive && State::m_input->IsPressed((SDL_Scancode)PLAYER_KEY::MAP_MOVE_RIGHT))
    {
        MapPos += b2Vec2(-10.0f, 0.0f);
    }
    if (IsActive && State::m_input->IsPressed((SDL_Scancode)PLAYER_KEY::MAP_MOVE_UP))
    {
        MapPos += b2Vec2(0.0f, -10.0f);
    }
    if (IsActive && State::m_input->IsPressed((SDL_Scancode)PLAYER_KEY::MAP_MOVE_DOWN))
    {
        MapPos += b2Vec2(0.0f, 10.0f);
    }
    if (IsActive && Application::WheelEvent > 0)
    {
        if (MapScale < 3.f)
        {
            MapScale *= 1.1f;
        }
    }
    if (IsActive && Application::WheelEvent < 0)
    {
        if(MapScale > 0.3f)
        {
            MapScale *= 0.9f;
        }
    }

    if(!IsActive)
    {
        this->sprite.color.a = 0;
        for(auto i : map_object)
        {
            i->sprite.color.a = 0;
        }
    }
    else
    {
        this->sprite.color.a = 255;
        for (auto i : map_object)
        {
            i->sprite.color.a = 255;
            if(i->Cog != NULL)
            {
                i->transform.SetScale(i->Cog->transform.GetScale().x * MapScale, i->Cog->transform.GetScale().y * MapScale);
                i->transform.position.Set(MapScale * (MapPos.x + i->Cog->transform.position.x), MapScale * (MapPos.y + i->Cog->transform.position.y), 0);
            }
        }
        Time += delta_time;
        if (Time >= 0.5f)
        {
            map_object.at(0)->sprite.color.a = 0;
            if (Time >= 0.6f)
            {
                map_object.at(0)->sprite.color.a = 255;
                Time = 0.0f;
            }
        }
        else
        {
            map_object.at(0)->sprite.color.a = 255;
        }
    }
}

bool Map::GetActiveStatus()
{
    return IsActive;
}
