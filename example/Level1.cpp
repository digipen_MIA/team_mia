/******************************************************************************/
/*!
\file   Level1.cpp
\project ColorLeon
\author primary : Kim Hyunseok

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "CommonLevel.h"

//#include "CustomCollision.h"
#include "Level1.h"
#include "EngineSupport.h"
#include "RayCast.h"
#include "Item.h"
#include "Application.h"

/***************Global Variable***************/

static int tick;
//UPDATE THIS WHEN ADDING OBJECT
enum
{
    GROUND,
    WALL,
    FLOATING_LAND,
    GROUND2,
    GROUND3,
    GROUND4,
    OBSTACLE,
};
/*********************************************/

void Level_1::Initialize()
{
    m_backgroundColor = { 0, 0, 0 };

    camera.Initialize(int(State::m_width), int(State::m_height));
    camera.position.Set(0, 0, 0.0f);

    m_useBuiltInPhysics = false;

    b2Vec2 gravity(0.f, -40.0f);      // 0, 200
    SetCustomPhysicsWorld(gravity);

    //* Here we predefine some info to create a body
    // Set the object's unique name
    player = new Player();
    player->SetName("Player");
    player->transform.position.Set(-212.0f, 35.0f, 0.0f);
    player->transform.SetScale(32, 32);
    player->sprite.color = Support::Color::WHITE;
    player->sprite.Free();
    player->sprite.LoadImage("texture/leon.png", m_renderer);
    player->customPhysics.bodyType = CustomPhysics::DYNAMIC;
    player->customPhysics.bodyShape = CustomPhysics::BOX;
    player->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &player->transform);
    // Add this function for give physics 
    player->SetState(this);
    player->Set_Player_Camera(&camera);

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Ground1");
    Object.back()->transform.position.Set(0.0f, 0.0f, 0.0f);
    Object.back()->transform.SetScale(500, 32);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.SetRestitution(0.f);
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("ColorWheel");
    Object.back()->transform.position.Set(-400.0f, 100.0f, 0.0f);
    Object.back()->transform.SetScale(144, 144);
    Object.back()->sprite.color = SDL_Color{ 255,255,255,180 };
    Object.back()->sprite.LoadImage("texture/ColorWheel.png", m_renderer);
    //Object.back()->sprite.activeAnimation = true;
    //Object.back()->sprite.SetFrame(3);
    //Object.back()->sprite.SetSpeed(8.0f);
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Wall1");
    Object.back()->transform.position.Set(-250.0f, 234.0f, 0.0f);
    Object.back()->transform.SetScale(32, 500);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Floating__Wall");
    Object.back()->transform.position.Set(-250.0f, 550.0f, 0.0f);
    Object.back()->transform.SetScale(32, 32);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Wall2");
    Object.back()->transform.position.Set(234.0f, -16.0f, 0.0f);
    Object.back()->transform.SetScale(32, 64);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Ground2");
    Object.back()->transform.position.Set(650.0f, -32.0f, 0.0f);
    Object.back()->transform.SetScale(800, 32);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.SetRestitution(0.f);
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Arrow1");
    Object.back()->transform.position.Set(-120.0f, 50.0f, 0.0f);
    Object.back()->transform.SetScale(36, 12);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/Arrow_Right.png", m_renderer);
    Object.back()->sprite.activeAnimation = true;
    Object.back()->sprite.SetFrame(3);
    Object.back()->sprite.SetSpeed(8.0f);
    Object.back()->IsPhysics = false;

    Object.push_back(new Item());
    Object.back()->SetName("ObstacleGround1");
    Object.back()->transform.position.Set(634.0f, 0.0f, 0.0f);
    Object.back()->transform.SetScale(768, 32);
    Object.back()->sprite.color = Support::Color::OBSTACLE_RED;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    static_cast<Item *>(Object.back())->SetEvent(Support::Event::Mark_Lose, player);
    Object.back()->SetData();
    Object.back()->customPhysics.ActiveGhostCollision(false);

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Wall2");
    Object.back()->transform.position.Set(1034.0f, -16.0f, 0.0f);
    Object.back()->transform.SetScale(32, 64);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Ground3");
    Object.back()->transform.position.Set(1250.0f, 0.0f, 0.0f);
    Object.back()->transform.SetScale(400, 32);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.SetRestitution(0.f);
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Wall3");
    Object.back()->transform.position.Set(1434.0f, 184.0f, 0.0f);
    Object.back()->transform.SetScale(32, 400);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Ground4");
    Object.back()->transform.position.Set(1650.0f, 368.0f, 0.0f);
    Object.back()->transform.SetScale(400, 32);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.SetRestitution(0.f);
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Wall4");
    Object.back()->transform.position.Set(1834.0f, 184.0f, 0.0f);
    Object.back()->transform.SetScale(32, 400);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Ground5");
    Object.back()->transform.position.Set(1650.0f, 368.0f, 0.0f);
    Object.back()->transform.SetScale(400, 32);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.SetRestitution(0.f);
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Wall5");
    Object.back()->transform.position.Set(2184.0f, 0.0f, 0.0f);
    Object.back()->transform.SetScale(32, 2000);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Ground6");
    Object.back()->transform.position.Set(2000.0f, -984.0f, 0.0f);
    Object.back()->transform.SetScale(400, 32);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.SetRestitution(0.f);
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new Item());
    Object.back()->SetName("ObstacleWall1");
    Object.back()->transform.position.Set(1834.0f, -216.0f, 0.0f);
    Object.back()->transform.SetScale(32, 400);
    Object.back()->sprite.color = Support::Color::OBSTACLE_RED;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    static_cast<Item *>(Object.back())->SetEvent(Support::Event::Mark_Lose, player);
    Object.back()->SetData();
    Object.back()->customPhysics.ActiveGhostCollision(false);

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Wall6");
    Object.back()->transform.position.Set(1816.0f, -800.0f, 0.0f);
    Object.back()->transform.SetScale(32, 400);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Ground7");
    Object.back()->transform.position.Set(1600.0f, -616.0f, 0.0f);
    Object.back()->transform.SetScale(400, 32);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.SetRestitution(0.f);
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Wall6");
    Object.back()->transform.position.Set(1416.0f, -800.0f, 0.0f);
    Object.back()->transform.SetScale(32, 400);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new Item());
    Object.back()->SetName("ObstacleWall2");
    Object.back()->transform.position.Set(1016.0f, -400.0f, 0.0f);
    Object.back()->transform.SetScale(32, 400);
    Object.back()->sprite.color = Support::Color::OBSTACLE_RED;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    static_cast<Item *>(Object.back())->SetEvent(Support::Event::Mark_Lose, player);
    Object.back()->SetData();
    Object.back()->customPhysics.ActiveGhostCollision(false);

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Ground8");
    Object.back()->transform.position.Set(1000.0f, -984.0f, 0.0f);
    Object.back()->transform.SetScale(800, 32);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.SetRestitution(0.f);
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new Item());
    Object.back()->SetName("ObstacleWall3");
    Object.back()->transform.position.Set(616.0f, -790.0f, 0.0f);
    Object.back()->transform.SetScale(32, 352);
    Object.back()->sprite.color = Support::Color::OBSTACLE_RED;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    static_cast<Item *>(Object.back())->SetEvent(Support::Event::Mark_Lose, player);
    Object.back()->SetData();
    Object.back()->customPhysics.ActiveGhostCollision(false);

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Ground9");
    Object.back()->transform.position.Set(416.0f, -616.0f, 0.0f);
    Object.back()->transform.SetScale(432, 32);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.SetRestitution(0.f);
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Wall2");
    Object.back()->transform.position.Set(234.0f, -16.0f, 0.0f);
    Object.back()->transform.SetScale(32, 64);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Ground2");
    Object.back()->transform.position.Set(650.0f, -32.0f, 0.0f);
    Object.back()->transform.SetScale(800, 32);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.SetRestitution(0.f);
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new Item());
    Object.back()->SetName("ObstacleGround1");
    Object.back()->transform.position.Set(634.0f, 0.0f, 0.0f);
    Object.back()->transform.SetScale(768, 32);
    Object.back()->sprite.color = Support::Color::OBSTACLE_RED;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    static_cast<Item *>(Object.back())->SetEvent(Support::Event::Mark_Lose, player);
    Object.back()->SetData();
    Object.back()->customPhysics.ActiveGhostCollision(false);

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Wall2");
    Object.back()->transform.position.Set(1034.0f, -16.0f, 0.0f);
    Object.back()->transform.SetScale(32, 64);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Ground3");
    Object.back()->transform.position.Set(1250.0f, 0.0f, 0.0f);
    Object.back()->transform.SetScale(400, 32);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.SetRestitution(0.f);
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Wall3");
    Object.back()->transform.position.Set(1434.0f, 184.0f, 0.0f);
    Object.back()->transform.SetScale(32, 400);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Ground4");
    Object.back()->transform.position.Set(1650.0f, 368.0f, 0.0f);
    Object.back()->transform.SetScale(400, 32);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.SetRestitution(0.f);
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Wall4");
    Object.back()->transform.position.Set(1834.0f, 184.0f, 0.0f);
    Object.back()->transform.SetScale(32, 400);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Ground5");
    Object.back()->transform.position.Set(1650.0f, 368.0f, 0.0f);
    Object.back()->transform.SetScale(400, 32);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.SetRestitution(0.f);
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Wall5");
    Object.back()->transform.position.Set(2184.0f, 0.0f, 0.0f);
    Object.back()->transform.SetScale(32, 2000);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Ground6");
    Object.back()->transform.position.Set(2000.0f, -984.0f, 0.0f);
    Object.back()->transform.SetScale(400, 32);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.SetRestitution(0.f);
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new Item());
    Object.back()->SetName("ObstacleWall1");
    Object.back()->transform.position.Set(1834.0f, -216.0f, 0.0f);
    Object.back()->transform.SetScale(32, 400);
    Object.back()->sprite.color = Support::Color::OBSTACLE_RED;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    static_cast<Item *>(Object.back())->SetEvent(Support::Event::Mark_Lose, player);
    Object.back()->SetData();
    Object.back()->customPhysics.ActiveGhostCollision(false);

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Wall6");
    Object.back()->transform.position.Set(1816.0f, -800.0f, 0.0f);
    Object.back()->transform.SetScale(32, 400);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Ground7");
    Object.back()->transform.position.Set(1600.0f, -616.0f, 0.0f);
    Object.back()->transform.SetScale(400, 32);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.SetRestitution(0.f);
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Wall6");
    Object.back()->transform.position.Set(1416.0f, -800.0f, 0.0f);
    Object.back()->transform.SetScale(32, 400);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new Item());
    Object.back()->SetName("ObstacleWall2");
    Object.back()->transform.position.Set(1016.0f, -400.0f, 0.0f);
    Object.back()->transform.SetScale(32, 400);
    Object.back()->sprite.color = Support::Color::OBSTACLE_RED;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    static_cast<Item *>(Object.back())->SetEvent(Support::Event::Mark_Lose, player);
    Object.back()->SetData();
    Object.back()->customPhysics.ActiveGhostCollision(false);

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Ground8");
    Object.back()->transform.position.Set(1000.0f, -984.0f, 0.0f);
    Object.back()->transform.SetScale(800, 32);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.SetRestitution(0.f);
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new Item());
    Object.back()->SetName("ObstacleWall3");
    Object.back()->transform.position.Set(616.0f, -790.0f, 0.0f);
    Object.back()->transform.SetScale(32, 352);
    Object.back()->sprite.color = Support::Color::OBSTACLE_RED;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    static_cast<Item *>(Object.back())->SetEvent(Support::Event::Mark_Lose, player);
    Object.back()->SetData();
    Object.back()->customPhysics.ActiveGhostCollision(false);

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Ground9");
    Object.back()->transform.position.Set(416.0f, -616.0f, 0.0f);
    Object.back()->transform.SetScale(432, 32);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.SetRestitution(0.f);
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->SetData();

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("DynamicBall");
    Object.back()->transform.position.Set(800.0f, -800.0f, 0.0f);
    Object.back()->transform.SetScale(64.0f, 64.0f);
    Object.back()->customPhysics.radius = 32;
    Object.back()->sprite.color = Support::Color::GREEN;
    Object.back()->sprite.LoadImage("texture/circle.png", m_renderer);
    Object.back()->customPhysics.bodyType = CustomPhysics::DYNAMIC;
    Object.back()->customPhysics.bodyShape = CustomPhysics::CIRCLE;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->customPhysics.SetDensity(1000.0f);
    Object.back()->SetData();

    Object.push_back(new Item());
    Object.back()->SetName("Goal");
    Object.back()->transform.position.Set(416.0f, -568.0f, 0.0f);
    Object.back()->transform.SetScale(64, 64);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/GateAnimation.png", m_renderer);
    Object.back()->sprite.activeAnimation = true;
    Object.back()->sprite.SetFrame(8);
    Object.back()->sprite.SetSpeed(15.0f);
    Object.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    Object.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    Object.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &Object.back()->transform);
    Object.back()->customPhysics.ActiveGhostCollision(false);
    Object.back()->SetData();
    dynamic_cast<Item *>(Object.back())->SetEvent(Support::Event::Mark_Win, player);

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("MouseR");
    Object.back()->transform.position.Set(-700.0f, 100.0f, 0.0f);
    Object.back()->transform.SetScale(36, 64);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/MouseR.png", m_renderer);
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Arrow1");
    Object.back()->transform.position.Set(-576.0f, 100.0f, 0.0f);
    Object.back()->transform.SetScale(64, 32);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/ArrowWhite.png", m_renderer);
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("MouseR");
    Object.back()->transform.position.Set(-430.0f, 60.0f, 0.0f);
    Object.back()->transform.SetScale(36, 64);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/Mouse_R [animated].png", m_renderer);
    Object.back()->sprite.activeAnimation = true;
    Object.back()->sprite.SetFrame(2);
    Object.back()->sprite.SetSpeed(5.0f);
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("BlueGuide");
    Object.back()->transform.position.Set(58.0f, 100.0f, 0.0f);
    Object.back()->transform.SetScale(64, 64);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/ArrowBlue.png", m_renderer);
    //Object.back()->sprite.activeAnimation = true;
    //Object.back()->sprite.SetFrame(2);
    //Object.back()->sprite.SetSpeed(5.0f);
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("RedGuide");
    Object.back()->transform.position.Set(1250.0f, 100.0f, 0.0f);
    Object.back()->transform.SetScale(64, 128);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/JumpRed.png", m_renderer);
    //Object.back()->sprite.activeAnimation = true;
    //Object.back()->sprite.SetFrame(2);
    //Object.back()->sprite.SetSpeed(5.0f);
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("YellowGuide");
    Object.back()->transform.position.Set(1950.0f, -850.0f, 0.0f);
    Object.back()->transform.SetScale(64, 90);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/ClimbBody.png", m_renderer);
    //Object.back()->sprite.activeAnimation = true;
    //Object.back()->sprite.SetFrame(2);
    //Object.back()->sprite.SetSpeed(5.0f);
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("YellowGuide");
    Object.back()->transform.position.Set(1950.0f, -800.0f, 0.0f);
    Object.back()->transform.SetScale(64, 90);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/ClimbHead.png", m_renderer);
    //Object.back()->sprite.activeAnimation = true;
    //Object.back()->sprite.SetFrame(2);
    //Object.back()->sprite.SetSpeed(5.0f);
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Key_AW");
    Object.back()->transform.position.Set(2020.0f, -800.0f, 0.0f);
    Object.back()->transform.SetScale(90, 64);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/Key_AW.png", m_renderer);
    Object.back()->sprite.activeAnimation = true;
    Object.back()->sprite.SetFrame(2);
    Object.back()->sprite.SetSpeed(5.0f);
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("GreenGuide");
    Object.back()->transform.position.Set(930.0f, -850.0f, 0.0f);
    Object.back()->transform.SetScale(64, 64);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/Gravity.png", m_renderer);
    //Object.back()->sprite.activeAnimation = true;
    //Object.back()->sprite.SetFrame(2);
    //Object.back()->sprite.SetSpeed(5.0f);
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("Key_Q");
    Object.back()->transform.position.Set(850.0f, -850.0f, 0.0f);
    Object.back()->transform.SetScale(64, 64);
    Object.back()->sprite.color = Support::Color::WHITE;
    Object.back()->sprite.LoadImage("texture/Shift[animated].png", m_renderer);
    Object.back()->sprite.activeAnimation = true;
    Object.back()->sprite.SetFrame(2);
    Object.back()->sprite.SetSpeed(5.0f);
    Object.back()->IsPhysics = false;

    Object.push_back(new CustomBaseObject());
    Object.back()->SetName("ConsoleStyleHud");
    Object.back()->transform.SetScale(State::m_width, State::m_height);
    Object.back()->sprite.color = SDL_Color{ 255,255,255,255 };
    Object.back()->sprite.LoadImage("texture/ConsoleStyleHUD.png", State::m_renderer);
    Object.back()->sprite.isHud = true;
    Object.back()->IsPhysics = false;

    AddObject(player);
    //AddObject(&hUD);

    //this MUST be added to every level for hud

    AddObject(player);
    AddCustomPhysicsComponent(player);
    for (auto i : Object)
    {
        AddObject(i);
        if (i->IsPhysics)
        {
            AddCustomPhysicsComponent(i);
        }
    }

    GetCustomPhysicsWorld()->SetContactListener(&myCustomCollision1);
    player->SetData();
    player->SetMap(Object);

    Cursor = Support::MakeCursor(this);
}

void Level_1::Update(float dt)
{
    Support::UpdateCursor(this, Cursor);

    //std::cout << player->transform.position.x << "," << player->transform.position.y << std::endl;
    using namespace Support;
    //if (player->transform.position.y > 500)
    //{
    //    camera.position.Set(player->transform.position.x, player->transform.position.y, 50.0f);
    //    //std::cout << camera.position.z << std::endl;
    //}

    /***************************************
    >>Dynamic Camera: reacts to player.y value
    ***************************************/
    if (player->transform.position.y > 500)
    {
        camera.position.Set(player->transform.position.x, player->transform.position.y, 125.0f);

    }
    else if (player->transform.position.y < -200)
        camera.position.Set(player->transform.position.x, player->transform.position.y, -50.0f);
    else
        camera.position.Set(player->transform.position.x, player->transform.position.y, player->transform.position.y*0.25f);//z = y for dramatic effect

/***************************************
>>To Main Level
***************************************/
    if (m_input->IsTriggered(SDL_SCANCODE_N))
    {
        //std::cout << "level input2" << std::endl;
        m_game->Change(LV_LevelBase);
        LevelBase::LevelNum = 1;
    }

    if (player->isWin())
    {
        if (Support::Color::Compare_Color(player->sprite.color, Support::Color::WHITE))
        {
            Application::data.isWhiteMan[1] = true;
        }
        if(Application::data.LockLevel < 3)
        {
            Application::data.LockLevel = 3;
        }
        m_game->Change(LV_LevelBase);
        LevelBase::LevelNum = 1;
    }

    /***************************************
    >>Pause & Restart
    ***************************************/
    if (m_input->IsTriggered(SDL_SCANCODE_ESCAPE))
    {
        m_game->Pause();
    }
    if (m_input->IsTriggered(SDL_SCANCODE_R))
    {
        m_game->Restart();
    }

    /***************************************
    >>Slow motion during the HUD
    ***************************************/
    if (m_input->IsPressed((MouseCode)PLAYER_KEY::CHANGE_COLOR))
    {
        if (tick % 2 == 0)
        {
            ++tick;
            return;
        }
    }

    ++tick;
    player->Update(dt);
    for (auto i : Object)
    {
        i->Update(dt);
    }

    // Must be one of the last functions called at the end of State Update
    UpdateCustomPhysics(dt);	// Update custom physics system here
    Render(dt);

    /***************************************
    >>Tick Start Over
    ***************************************/
    if (tick == 100)
    {
        tick = 0;
    }
}

void Level_1::Close()
{
    // Deallocate custom physics world
    player->Close_Player_Rope();
    //backgroundMusic.Free();
    RemoveCustomPhysicsWorld();
    // Wrap up state
    ClearBaseState(false);

    for (auto i : Object)
    {
        delete i;
        i = NULL;
    }
    Object.clear();
    player->sprite.Free();
    delete player;

    delete Cursor;
}