/******************************************************************************/
/*!
\file   LevelSelect.h
\project ColorLeon
\author primary : Kim Minsuk

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "engine\State.h"
#include "CustomBaseObject.h"
#include "Player.h"
#include "CustomCollision.h"

class LevelSelect : public State
{
    friend class Game;

public:
    SDL_Rect rect;

    CustomCollision myCustomCollision;
protected:
    LevelSelect() : State() {};
    ~LevelSelect() override {};

    // Derived initialize function
    void Initialize() override;
    // Derived Update function
    void Update(float dt) override;
    // Derived Close function
    void Close() override;

    std::string GetString();
private:
    // Objects which contains custom physics
    std::vector<CustomBaseObject *> Object;
    CustomBaseObject	    wallPaper;

    int ShouldGo;
    int LevelNum = 0;

    int FindLastLevel();

    bool MoveLevel = false;

    bool IsJump = false;

    CustomBaseObject * Cursor;
};
