/******************************************************************************/
/*!
\file   EngineSupport.h
\project ColorLeon
\author primary : Kim Minsuk, secondary : Choi Jinhyun, Kim Hyunseok

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include <Box2D/Common/b2Math.h>
#include <engine/State.h>

class CustomCollision;

namespace Support
{
    namespace Color
    {
        const SDL_Color BLACK{ 0   , 0   , 0   , 255 };
        const SDL_Color WHITE{ 255 , 255 , 255 , 255 };

        const SDL_Color RED{ 255 , 0   , 0   , 255 };
        const SDL_Color BLUE{ 0   , 0   , 255 , 255 };
        const SDL_Color GREEN{ 25  , 155 , 0   , 255 };
        const SDL_Color YELLOW{ 254 , 255 , 0   , 255 };

        const SDL_Color OBSTACLE_RED{ 255, 0, 255, 128 };
        bool Compare_Color(SDL_Color lhs, SDL_Color rhs);
    }
    namespace Rand
    {
        int GetRand(int min, int max);

        SDL_Color GetRandomColor();
    }
    b2Vec2 GetMousePos(State * state);
    b2Vec2 GetWorldMousePos(State * state, Camera * camera);
    bool operator<=(b2Vec2 &lhs, b2Vec2 &rhs);
    bool operator<=(b2Vec2 &vec2, b2Vec3 &vec3);
    bool operator>=(b2Vec2 &lhs, b2Vec2 &rhs);
    bool operator>=(b2Vec2 &vec2, b2Vec3 &vec3);
    b2Vec2 operator+(b2Vec3 &vec3, b2Vec2 &vec2);
    b2Vec2 operator-(b2Vec3 &vec3, b2Vec2 &vec2);
    std::ostream& operator<<(std::ostream& os, const b2Vec2& vector);
    std::ostream& operator<<(std::ostream& os, const b2Vec3& vector);
    b2Vec2 operator*(b2Vec2 vector, float scale);

    float RoundToEvenNumber(float value);
    namespace Event
    {
        void RestartEvent(void * param);
        void GoNext(void * param);
        void MoveMainLevel(void * param);
        void MoveEditLevel(void * param);
        void MoveSettingLevel(void * param);
        void QuitGame(void * param);
        void MoveMainMenu(void* param);
        void MoveSelectLevel(void* param);
        void ResumeEvent(void* param);
        void ResumeAndMainMenuEvent(void* param);
        void MakeInvisible(void * param);
        void Makevisible(void * param);
        void Option_HowToPlay_Click(void * param);
        void MakeInvisible_BUTTON(void * param);

        void Mark_Win(void * param);
        void Mark_Lose(void * param);

        void ToggleFullScreen(void * param);
        void ToggleMute(void * param);
    }

    CustomBaseObject * MakeCursor(State * state);
    void UpdateCursor(State * state, CustomBaseObject * cursor);
}
