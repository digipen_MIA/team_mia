/******************************************************************************/
/*!
\file   EditLevel.cpp
\project ColorLeon
\author primary : Kim Hyunsung, secondary : Kim Minsuk

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "CommonLevel.h"
#include "EngineSupport.h"
#include <iostream>
#include <fstream>
#include <string>
#include "Application.h"

int  EditLevel::Check_Level_number = 0;

void EditLevel::Initialize()
{
    button.clear();
    m_backgroundColor = { 0X00, 0x00, 0x00 };

    camera.Initialize(int(State::m_width), int(State::m_height));
    camera.position.Set(0, 0, 0.0f);

    m_useBuiltInPhysics = false;

    b2Vec2 gravity(0.f, 0.f);
    SetCustomPhysicsWorld(gravity);

    button.push_back(new Button());
    button.back()->SetName("Back");
    button.back()->transform.position.Set(-600.f, 320.0f, 0.0f);
    button.back()->transform.SetScale(80.0f, 80.0f);
    button.back()->sprite.LoadImage("texture/BACK.png", m_renderer);
    button.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    button.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    button.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &button.back()->transform);
    button.back()->SetColor(SDL_Color{ 0xff, 0xff, 0, 0xff });
    button.back()->SetData();
    button.back()->SetWorld(this, &camera);
    button.back()->SetClickEvent(Support::Event::MoveMainMenu, this);
    button.back()->sprite.isHud = true;


    LoadFromFile();

    for (auto i : button)
    {
        AddObject(i);
        AddCustomPhysicsComponent(i);
    }

    Cursor = Support::MakeCursor(this);
}

void EditLevel::Update(float dt)
{
    Support::UpdateCursor(this, Cursor);

    for (auto i = button.begin(); i != button.end();)
    {
        (*i)->Update(dt);
        if (!((*i)->Life))
        {
            RemoveObject(*i);
            (*i)->RemoveCustomPhysicsComponent();
            delete *i;
            *i = NULL;
            i = button.erase(i);
            HowManyObject[LevelNumber - 1]--;

        }
        else
        {
            ++i;
        }
    }

    float cameraSpeed = 10.0f;
    float wheelSpeed = 20.0f;
    if (m_input->IsPressed(SDL_SCANCODE_LSHIFT))
    {
        cameraSpeed = 1.0f;
        wheelSpeed = 3.0f;
    }
    if (m_input->IsPressed(SDL_SCANCODE_UP))
    {
        camera.position.y += cameraSpeed;
    }
    if (m_input->IsPressed(SDL_SCANCODE_DOWN))
    {
        camera.position.y -= cameraSpeed;
    }
    if (m_input->IsPressed(SDL_SCANCODE_LEFT))
    {
        camera.position.x -= cameraSpeed;
    }
    if (m_input->IsPressed(SDL_SCANCODE_RIGHT))
    {
        camera.position.x += cameraSpeed;
    }
    if (Application::WheelEvent < 0)
    {
        camera.position.z += wheelSpeed;
    }
    if (Application::WheelEvent > 0)
    {
        camera.position.z -= wheelSpeed;
    }
    if (m_input->IsPressed(SDL_SCANCODE_0))
    {
        camera.position.z = 0.f;
    }

    //PLATFORM--------------------------------------------------------------
    if (m_input->IsTriggered(SDL_SCANCODE_1))
    {
        MakeButton(ButtonType::DRAG_DROP, ObjectType::PLATFORM);

    }

    //PLAYER----------------------------------------------------------------
    if (m_input->IsTriggered(SDL_SCANCODE_2))
    {
        MakeButton(ButtonType::DRAG_DROP, ObjectType::PLAYER);
        button.back()->sprite.LoadImage("texture/leon.png", m_renderer);
        button.back()->transform.SetScale(32.0f, 32.0f);
        button.back()->SetObjectType(ObjectType::PLAYER);
    }

    //GRAVITY OBJECT--------------------------------------------------------
    if (m_input->IsTriggered(SDL_SCANCODE_3))
    {
        MakeButton(ButtonType::DRAG_DROP, ObjectType::GRAV);
        button.back()->sprite.LoadImage("texture/circle.png", m_renderer);
        button.back()->transform.SetScale(32.0f, 32.0f);
        button.back()->SetObjectType(ObjectType::GRAV);
    }

    //OBSTACLE---------------------------------------------------------------
    if (m_input->IsTriggered(SDL_SCANCODE_4))
    {
        MakeButton(ButtonType::DRAG_DROP, ObjectType::DANGER);
        button.back()->sprite.LoadImage("texture/rect.png", m_renderer);
        button.back()->transform.SetScale(32.0f, 32.0f);
        button.back()->SetColor(Support::Color::OBSTACLE_RED);
        button.back()->SetObjectType(ObjectType::DANGER);
    }

    //GOAL------------------------------------------------------------------
    if (m_input->IsTriggered(SDL_SCANCODE_5))
    {
        MakeButton(ButtonType::DRAG_DROP, ObjectType::GOAL);
        //button.back()->sprite.LoadImage("texture/GateAnimation.png", m_renderer);
        //        button.back()->sprite.activeAnimation = true;
        //        button.back()->sprite.SetFrame(8);
        //        button.back()->sprite.SetSpeed(15.0f);
        button.back()->sprite.LoadImage("texture/rect.png", m_renderer);
        button.back()->SetColor(Support::Color::BLUE);
        button.back()->transform.SetScale(32.0f, 32.0f);
        button.back()->SetObjectType(ObjectType::GOAL);
    }

    //RESET THE LEVEL-------------------------------------------------------
    //if (m_input->IsTriggered(SDL_SCANCODE_5)) //modify click the button
    //{
    //    m_game->Restart();
    //    //LevelNumber = number...
    //    //State = Level;
    //    //Close();
    //    //Initialize();
    //}

    if (m_input->IsTriggered(SDL_SCANCODE_C))
    {
        SaveFromFile();
        m_game->testlevel.LevelNum = LevelNumber;
        m_game->Change(LV_TestLevel);
    }
    if (m_input->IsTriggered(SDL_SCANCODE_O))
    {
        State = Level;
        --LevelNumber;
        m_game->Restart();
    }
    if (m_input->IsTriggered(SDL_SCANCODE_P))
    {
        State = Level;
        ++LevelNumber;
        m_game->Restart();
    }
    //LEVEL SELECT----------------------------------------------------------------
    //LEVEL 1
    if (m_input->IsTriggered(SDL_SCANCODE_F1))
    {
        State = Level;
        LevelNumber = 1;
        m_game->Restart();
    }
    //LEVEL 2
    if (m_input->IsTriggered(SDL_SCANCODE_F2))
    {
        State = Level;
        LevelNumber = 2;
        m_game->Restart();
    }
    //LEVEL 3
    if (m_input->IsTriggered(SDL_SCANCODE_F3))
    {
        State = Level;
        LevelNumber = 3;
        m_game->Restart();
    }
    //LEVEL 4
    if (m_input->IsTriggered(SDL_SCANCODE_F4))
    {
        State = Level;
        LevelNumber = 4;
        m_game->Restart();
    }
    //LEVEL 5
    if (m_input->IsTriggered(SDL_SCANCODE_F5))
    {
        State = Level;
        LevelNumber = 5;
        m_game->Restart();
    }
    //LEVEL 6
    if (m_input->IsTriggered(SDL_SCANCODE_F6))
    {
        State = Level;
        LevelNumber = 6;
        m_game->Restart();
    }
    //LEVEL 7
    if (m_input->IsTriggered(SDL_SCANCODE_F7))
    {
        State = Level;
        LevelNumber = 7;
        m_game->Restart();
    }
    //LEVEL 8
    if (m_input->IsTriggered(SDL_SCANCODE_F8))
    {
        State = Level;
        LevelNumber = 8;
        m_game->Restart();
    }
    //LEVEL 9
    if (m_input->IsTriggered(SDL_SCANCODE_F9))
    {
        State = Level;
        LevelNumber = 9;
        m_game->Restart();
    }
    //LEVEL 10
    if (m_input->IsTriggered(SDL_SCANCODE_F10))
    {
        State = Level;
        LevelNumber = 10;
        m_game->Restart();
    }

    // Must be one of the last functions called at the end of State Update
    UpdateCustomPhysics(dt);	// Update custom physics system here
    Render(dt);


    if (m_input->IsTriggered(SDL_SCANCODE_S))
    {
        SaveFromFile();
    }
}

void EditLevel::Close()
{
    // Deallocate custom physics world
    RemoveCustomPhysicsWorld();

    // Wrap up state
    ClearBaseState(false);

    for (auto i : button)
    {
        delete i;
        i = NULL;
    }
    button.clear();

    delete Cursor;
}

void EditLevel::MakeButton(ButtonType type, ObjectType objecttype)
{
    button.push_back(new Button());
    button.back()->SetName("Button");
    button.back()->transform.position.Set(-128.0f, 0.0f, 0.0f);
    button.back()->transform.SetScale(32.0f, 32.0f);
    button.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    button.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    button.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    button.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &button.back()->transform);
    button.back()->SetColor(SDL_Color{ 0xff, 0xff, 0xff, 0xff });
    button.back()->SetData();
    button.back()->SetWorld(this, &camera);
    button.back()->SetType(type);
    button.back()->SetObjectType(objecttype);
    AddObject(button.back());
    AddCustomPhysicsComponent(button.back());
    HowManyObject[LevelNumber - 1]++;

}

//SAVING THE LEVEL INFORMATION------------------------------------------
void EditLevel::SaveFromFile()
{
    int nowlevel = LevelNumber;

    std::string tostring = std::to_string(nowlevel);

    std::ofstream FOut("map/LEVEL" + tostring + ".txt", std::ios::trunc);
    std::cin.ignore(LevelNumber, '\n');

    HowManyObject[LevelNumber - 1] = (int)button.size() - 1;

    FOut << HowManyObject[LevelNumber - 1] << "\n";
    for (auto i : button)
    {
        if (i->GetName() == "Back")
        {
            continue;
        }

        FOut << std::round(i->transform.position.x) << "\n";
        FOut << std::round(i->transform.position.y) << "\n";
        FOut << std::round(i->transform.position.z) << "\n";
        FOut << Support::RoundToEvenNumber(i->transform.GetScale().x) << "\n";
        FOut << Support::RoundToEvenNumber(i->transform.GetScale().y) << "\n";
        FOut << i->transform.rotation << "\n";

        //Todo : save type of object
        FOut << (int)i->objecttype << "\n";
    }
    FOut.close();
}

//LOAD LEVEL INFORMATION------------------------------------------------
void EditLevel::LoadFromFile()
{
    int nowlevel = LevelNumber;

    std::string tostring = std::to_string(nowlevel);
    std::string filename = "map/LEVEL" + tostring + ".txt";
    const char *filenames = filename.c_str();

    b2Vec3 position;
    b2Vec2 scale;

    float rotation = 0;
    int ObjectType = 0;
    FILE* pFile = 0;
    fopen_s(&pFile, filenames, "rt");
    if (pFile) {

        fscanf_s(pFile, "%d", &HowManyObject[LevelNumber - 1]);

        for (int i = 0; i < HowManyObject[LevelNumber - 1]; ++i)
        {
            fscanf_s(pFile, "%f", &position.x);
            fscanf_s(pFile, "%f", &position.y);
            fscanf_s(pFile, "%f", &position.z);
            fscanf_s(pFile, "%f", &scale.x);
            fscanf_s(pFile, "%f", &scale.y);
            fscanf_s(pFile, "%f", &rotation);
            fscanf_s(pFile, "%d", &ObjectType);

            //PLATFORM--------------------------------------------------------------
            if (ObjectType == 1) {
                button.push_back(new Button());
                button.back()->SetName("Button");
                button.back()->transform.position.Set(position.x, position.y, position.z);
                button.back()->transform.SetScale(scale.x, scale.y);
                button.back()->transform.rotation = rotation;

                button.back()->sprite.LoadImage("texture/rect.png", m_renderer);
                button.back()->customPhysics.bodyType = CustomPhysics::STATIC;
                button.back()->customPhysics.bodyShape = CustomPhysics::BOX;
                button.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &button.back()->transform);
                button.back()->SetColor(SDL_Color{ 0xff, 0xff, 0xff, 0xff });
                button.back()->SetData();
                button.back()->SetWorld(this, &camera);
                button.back()->SetType(ButtonType::DRAG_DROP);
                button.back()->SetObjectType(ObjectType::PLATFORM);
            }

            //PLAYER----------------------------------------------------------------
            else if (ObjectType == 2)
            {
                button.push_back(new Button());
                button.back()->transform.position.Set(position.x, position.y, position.z);
                button.back()->transform.SetScale(scale.x, scale.y);
                button.back()->transform.rotation = rotation;

                button.back()->sprite.LoadImage("texture/leon.png", m_renderer);
                button.back()->customPhysics.bodyType = CustomPhysics::STATIC;
                button.back()->customPhysics.bodyShape = CustomPhysics::CIRCLE;
                button.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &button.back()->transform);
                button.back()->SetColor(SDL_Color{ 0xff, 0xff, 0xff, 0xff });
                button.back()->SetData();
                button.back()->SetWorld(this, &camera);
                button.back()->SetType(ButtonType::DRAG_DROP);
                button.back()->SetObjectType(ObjectType::PLAYER);
            }

            //GRAVITY OBJECT--------------------------------------------------------
            else if (ObjectType == 3)
            {
                button.push_back(new Button());
                button.back()->transform.position.Set(position.x, position.y, position.z);
                button.back()->transform.SetScale(scale.x, scale.y);
                button.back()->transform.rotation = rotation;

                button.back()->sprite.LoadImage("texture/circle.png", m_renderer);
                button.back()->customPhysics.bodyType = CustomPhysics::STATIC;
                button.back()->customPhysics.bodyShape = CustomPhysics::CIRCLE;
                button.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &button.back()->transform);
                button.back()->SetColor(SDL_Color{ 0xff, 0xff, 0xff, 0xff });
                button.back()->SetData();
                button.back()->SetWorld(this, &camera);
                button.back()->SetType(ButtonType::DRAG_DROP);
                button.back()->SetObjectType(ObjectType::GRAV);

            }

            else if (ObjectType == 4)
            {
                button.push_back(new Button());
                button.back()->transform.position.Set(position.x, position.y, position.z);
                button.back()->transform.SetScale(scale.x, scale.y);
                button.back()->transform.rotation = rotation;

                button.back()->sprite.LoadImage("texture/rect.png", m_renderer);
                button.back()->customPhysics.bodyType = CustomPhysics::STATIC;
                button.back()->customPhysics.bodyShape = CustomPhysics::BOX;
                button.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &button.back()->transform);
                button.back()->SetColor(Support::Color::OBSTACLE_RED);
                button.back()->SetData();
                button.back()->SetWorld(this, &camera);
                button.back()->SetType(ButtonType::DRAG_DROP);
                button.back()->SetObjectType(ObjectType::DANGER);
            }

            //GOAL------------------------------------------------------------------
            else if (ObjectType == 5)
            {
                button.push_back(new Button());
                button.back()->SetName("Button");
                button.back()->transform.position.Set(position.x, position.y, position.z);
                button.back()->transform.SetScale(scale.x, scale.y);
                button.back()->transform.rotation = rotation;

                button.back()->sprite.LoadImage("texture/rect.png", m_renderer);
                button.back()->customPhysics.bodyType = CustomPhysics::STATIC;
                button.back()->customPhysics.bodyShape = CustomPhysics::BOX;
                button.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &button.back()->transform);
                button.back()->SetColor(Support::Color::BLUE);
                button.back()->SetData();
                button.back()->SetWorld(this, &camera);
                button.back()->SetType(ButtonType::DRAG_DROP);
                button.back()->SetObjectType(ObjectType::GOAL);
            }
        }
        Check_Level_number++;
        fclose(pFile);
    }

}
