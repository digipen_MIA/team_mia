/******************************************************************************/
/*!
\file   FinishCredit.cpp
\project ColorLeon
\author primary : Choi Jinhyun

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "FinishCredit.h"
#include <iostream>
#include "Game.h"
#include "EngineSupport.h"

void FinishCredit::Initialize()
{
    camera_y = 0;
    close_time = 0;

    m_backgroundColor = { 0, 0, 0, 255};
    camera.Initialize(int(State::m_width), int(State::m_height));
    camera.position.Set(0, 0, 0.0f);

    State::m_pBGM->LoadMusic("sound/My Song.wav");
    m_pBGM->Play();

    b2Vec2 gravity(0.f, 0.f);      // 0, 200
    SetCustomPhysicsWorld(gravity);

    Credit_Papaer.SetName("Credit");
    Credit_Papaer.transform.position.Set(0.0, -1300.0f, 0.0f);
    Credit_Papaer.transform.SetScale(State::m_width, 2000);
    Credit_Papaer.sprite.color = SDL_Color{ 255, 255, 255, 255 };
    Credit_Papaer.sprite.LoadImage("texture/CreditLevel.png", m_renderer);
    Credit_Papaer.IsPhysics = false;

    AddObject(&Credit_Papaer);

    Cursor = Support::MakeCursor(this);
}

void FinishCredit::Update(float dt)
{
    Support::UpdateCursor(this, Cursor);

    Render(dt);

    if(camera.position.y <= -2100 )
    {
        close_time += dt;

        if (close_time >= 3.0f)
        {
            m_game->Change(LV_MainMenu);
        }
    }
    else
    {
        camera_y -= dt * 100;
        camera.position.y = camera_y;
    }

    if(m_input->IsTriggered(SDL_SCANCODE_N))
    {
        m_game->Change(LV_MainMenu);
    }
}

void FinishCredit::Close()
{
    RemoveCustomPhysicsWorld();

    // Wrap up state
    ClearBaseState(false);

    delete Cursor;
}
