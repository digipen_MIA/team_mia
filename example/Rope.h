/******************************************************************************/
/*!
\file   Rope.h
\project ColorLeon
\author primary : Choi Jinhyun

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once

#include "MainLevel.h"
#include <cmath>

class RayCast;

namespace Math
{
    const float PI = 4.0f * std::atan(1.0f);

    inline float To_Degree(float radian)
    {
        float temp;
        temp = radian * 180.0f / PI;
        return temp;
    }
}

class Rope
{
public:
    Rope(Player* player, CustomBaseObject* object, b2Vec2 set_anchor, float32 lenght, b2World* myWorld);

    void DeleteRope(b2World* myWorld, Rope** original);

    float32 Distance(Player* player, CustomBaseObject* object);
    b2Vec2 Specific_Point(Player* player, b2Vec2 point);
    float32 Theta(b2Vec2 point);

    b2DistanceJoint* GetRopeInfo();

    CustomBaseObject* save_rope_obj;
    b2Vec2 rope_set_anchor_position;
    b2Vec2 rope_render_position;

    void Render(CustomBaseObject* rope_obj, Player* player);

private:
    b2DistanceJoint * rope_joint;
    b2DistanceJointDef rope_joint_def;
    float32 ropeLength = 0;
   // b2DistanceJoint* rope_joint;
   // b2DistanceJointDef rope_joint_def;
};