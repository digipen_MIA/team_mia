/******************************************************************************/
/*!
\file   Physics.cpp
\project ColorLeon
\author primary : Jung Juyong

All content © 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "Physics.h"
#include "engine\Transform.h"
#include "Box2D\Dynamics\b2Body.h"

Physics::Physics()
{}

Physics::~Physics()
{}


void Physics::Free()
{
}

void Physics::SetVelocity(float x, float y)
{
	m_velocity.Set(x, y);
	m_body->SetLinearVelocity(m_velocity);
}

void Physics::SetVelocity(const b2Vec2& vel)
{
	m_velocity = vel;
	m_body->SetLinearVelocity(m_velocity);
}

const b2Vec2& Physics::GetVelocity() const
{
	return m_velocity;
}

void Physics::SetDensity(float dens)
{
    m_fixtureDef.density = dens;
}

float Physics::GetDensity()
{
    return m_fixtureDef.density;
}

void Physics::SetRestitution(float res)
{
    m_fixtureDef.restitution = res;
}

float Physics::GetRestitution()
{
    return m_fixtureDef.restitution;
}

void Physics::ActivePhysics(bool toggle)
{
	m_active = toggle;
	m_body->SetAwake(m_active);
}

bool Physics::IsActive() const
{
	return m_active;
}

void Physics::AllocateBody(b2World* world)
{
	if (!m_body) {

		// Set the starting position
		m_bodyType.position.Set(
			pTransform->position.x,
			pTransform->position.y);

		// Set the starting angle
		m_bodyType.angle = pTransform->rotation;

		// Set box info
                if (bodyShape == BOX) {
                    m_bodyBox.SetAsBox(
                        pTransform->scale.x * .5f,
                        pTransform->scale.y * .5f);
                    m_fixtureDef.shape = &(m_bodyBox);
                }
                // Set circle info
                else if (bodyShape == CIRCLE) {
                    m_bodyBall.m_radius = radius;
                    m_fixtureDef.shape = &(m_bodyBall);
                }

		// Set body type
		if (bodyType == DYNAMIC)
			m_bodyType.type = b2_dynamicBody;
		else if (bodyType == STATIC)
			m_bodyType.type = b2_staticBody;

    	        // Create a body and allocate to the pointer to body of physics component
		m_body = world->CreateBody(&(m_bodyType));
		m_body->CreateFixture(&(m_fixtureDef));
	}
}
