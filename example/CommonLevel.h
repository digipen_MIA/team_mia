/******************************************************************************/
/*!
\file   CommonLevel.h
\project ColorLeon
\author primary : Jeong Juyong

All content © 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "engine\Input.h"
#include "SDL2/SDL_image.h"
#include "Box2D\Common\b2Math.h"
#include <cmath>
#include <cstdlib>
#include <iostream>

#include "Game.h"

#include "Box2D\Collision\Shapes\b2PolygonShape.h"
#include "Box2D\Dynamics\b2Body.h"
#include "Box2D\Dynamics\b2Fixture.h"
#include "Box2D\Dynamics\b2World.h"