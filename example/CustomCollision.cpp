﻿/******************************************************************************/
/*!
\file   CustomCollision.cpp
\project ColorLeon
\author primary : Kim Minsuk

All content © 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "CustomCollision.h"
#include "CustomBaseObject.h"
#include "Player.h"
#include <iostream>
#include "EngineSupport.h"
#include "Item.h"

void CustomCollision::BeginContact(b2Contact* contact)
{
    //player data
    void* bodyUserData = contact->GetFixtureA()->GetBody()->GetUserData();
    //objects that player collide with
    void* otherbodyUserData = contact->GetFixtureB()->GetBody()->GetUserData();
    //player Sensor collision
    void* fixtureUserData = contact->GetFixtureA()->GetUserData();

    if (!otherbodyUserData)
    {
        return;
    }

    DealBeginCollision(bodyUserData, false, otherbodyUserData);

    if (static_cast<CustomBaseObject *>(otherbodyUserData)->IsPhysics)
    {
        DealBeginCollision(fixtureUserData);
    }

    bodyUserData = contact->GetFixtureB()->GetBody()->GetUserData();
    otherbodyUserData = contact->GetFixtureA()->GetBody()->GetUserData();
    fixtureUserData = contact->GetFixtureB()->GetUserData();

    if (!otherbodyUserData)
    {
        return;
    }

    DealBeginCollision(bodyUserData, false, otherbodyUserData);

    if (static_cast<CustomBaseObject *>(otherbodyUserData)->IsPhysics)
    {
        DealBeginCollision(fixtureUserData);
    }
}

void CustomCollision::EndContact(b2Contact* contact)
{
    void* bodyUserData = contact->GetFixtureA()->GetBody()->GetUserData();
    void* otherbodyUserData = contact->GetFixtureB()->GetBody()->GetUserData();
    void* fixtureUserData = contact->GetFixtureA()->GetUserData();

    if (!otherbodyUserData)
    {
        return;
    }

    DealEndCollision(bodyUserData, false, otherbodyUserData);

    if (static_cast<CustomBaseObject *>(otherbodyUserData)->IsPhysics)
    {
        DealEndCollision(fixtureUserData);
    }

    bodyUserData = contact->GetFixtureB()->GetBody()->GetUserData();
    otherbodyUserData = contact->GetFixtureA()->GetBody()->GetUserData();
    fixtureUserData = contact->GetFixtureB()->GetUserData();

    if (!otherbodyUserData)
    {
        return;
    }

    DealEndCollision(bodyUserData, false, otherbodyUserData);

    if (static_cast<CustomBaseObject *>(otherbodyUserData)->IsPhysics)
    {
        DealEndCollision(fixtureUserData);
    }
}

void CustomCollision::DealBeginCollision(void* objA, bool IsFixture, void* objB)
{
    if (!IsFixture)
    {
        if (objA)
        {
            if (static_cast<CustomBaseObject *>(objA)->GetName() == "Player")
            {
                Player * tempPlayer = static_cast<Player *>(objA);


                //does not play the bounce sound when player detect with item
                if(dynamic_cast<Item *>(static_cast<CustomBaseObject *>(objB)) == NULL)
                {
                    //when player contact with other object, play the bounce sound
                    tempPlayer->PlayBounce();
                }

                bool IsRepeated = false;
                for (int i = 0; i < (int)tempPlayer->CollisionList.size(); ++i)
                {
                    if (tempPlayer->CollisionList.at(i) == objB)
                    {
                        IsRepeated = true;
                        break;
                    }
                }
                if (!IsRepeated)
                {
                    tempPlayer->CollisionList.push_back(objB);
                    //tempPlayer->UpdateColor();
                }
                //TODO: Make this!!!!
            }
        }
    }
    else
    {
        if (objA)
        {
            Sensor * tempSensor = static_cast<Sensor *>(objA);
            tempSensor->IsAlarm = true;
            ++tempSensor->CollisionNum;

        }
    }
}

void CustomCollision::DealEndCollision(void* objA, bool IsFixture, void* objB)
{
    if (!IsFixture)
    {
        if (objA)
        {
            if (static_cast<CustomBaseObject *>(objA)->GetName() == "Player")//This is for persisted collision
            {
                Player * tempPlayer = static_cast<Player *>(objA);
                for (auto i = tempPlayer->CollisionList.begin();
                    i != tempPlayer->CollisionList.end(); ++i)
                {
                    if (*i == objB)
                    {
                        tempPlayer->CollisionList.erase(i);
                        break;
                    }
                }
            }
        }
    }
    else
    {
        if (objA)
        {
            Sensor * tempSensor = static_cast<Sensor *>(objA);
            --tempSensor->CollisionNum;
            if (!tempSensor->CollisionNum)
            {
                tempSensor->IsAlarm = false;
            }
        }
    }
}

void CustomCollision::PreSolve(b2Contact* contact, const b2Manifold* /*oldManifold*/)
{
    //BeginContact(contact);

    void* bodyUserData = contact->GetFixtureA()->GetBody()->GetUserData();
    void* otherbodyUserData = contact->GetFixtureB()->GetBody()->GetUserData();

    if (!otherbodyUserData)
    {
        return;
    }

    if (bodyUserData)
    {
        if (static_cast<CustomBaseObject *>(bodyUserData)->GetName() == "Player")
        {
            Player * tempPlayer = static_cast<Player *>(bodyUserData);
            CustomBaseObject * OtherObject = static_cast<CustomBaseObject *>(otherbodyUserData);
            if (!Support::Color::Compare_Color(tempPlayer->sprite.color, OtherObject->sprite.color))
            {
                if (Support::Color::Compare_Color(OtherObject->sprite.color, Support::Color::RED))
                {
                    OtherObject->customPhysics.GetFixture()->SetRestitution(0.0f);
                    contact->SetRestitution(0.0f);
                }
            }
        }
    }

    otherbodyUserData = contact->GetFixtureA()->GetBody()->GetUserData();
    bodyUserData = contact->GetFixtureB()->GetBody()->GetUserData();
    if (bodyUserData)
    {
        if (static_cast<CustomBaseObject *>(bodyUserData)->GetName() == "Player")
        {
            Player * tempPlayer = static_cast<Player *>(bodyUserData);
            CustomBaseObject * OtherObject = static_cast<CustomBaseObject *>(otherbodyUserData);
            if (!Support::Color::Compare_Color(tempPlayer->sprite.color, OtherObject->sprite.color))
            {
                if (Support::Color::Compare_Color(OtherObject->sprite.color, Support::Color::RED))
                {
                    OtherObject->customPhysics.GetFixture()->SetRestitution(0.0f);
                    contact->SetRestitution(0.0f);
                }
            }
        }
    }
}

