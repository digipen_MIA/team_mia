/******************************************************************************/
/*!
\file   Sensor.h
\project ColorLeon
\author primary : Kim Minsuk

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "third_party/engine/Object.h"
#include "CustomBaseObject.h"

class Sensor
{
public:
    Sensor(std::string name, float x, float y, float w, float h, void * parent);
    ~Sensor();
    b2PolygonShape SensorShape;
    b2FixtureDef * GetSensorFixtureDef();
    b2Fixture * SensorFixture;
    b2FixtureDef SensorFixtureDef;
    std::string Name;
    bool IsAlarm;
    void * Parent;

    int CollisionNum = 0;

    void SetSensorData(b2Body * body);
};

