/******************************************************************************/
/*!
\file   OptionState.cpp
\project ColorLeon
\author primary : Choi Jinhyun

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#include "CommonLevel.h"
#include "EngineSupport.h"
#include "Application.h"

bool Option::PrintHowToPlay = false;

void Option::Initialize()
{
    m_backgroundColor = { 0Xff, 0xff, 0xff };

    camera.Initialize(int(State::m_width), int(State::m_height));
    camera.position.Set(0, 0, 0.0f);

    blank_paper.SetName("BlankPaper");
    blank_paper.transform.position.Set(0.0f, 0.0f, 0.0f);
    blank_paper.transform.SetScale(m_width, m_height);
    blank_paper.sprite.color = SDL_Color{ 0, 0, 0, 0 };
    blank_paper.sprite.LoadImage("texture/rect.png", m_renderer);
    blank_paper.IsPhysics = false;

    //backgroundMusic.LoadMusic("sound/My Song.wav");
    //backgroundMusic.Play();
    //backgroundMusic.SetVolume(Application::setting.volume);

    m_useBuiltInPhysics = false;

    b2Vec2 gravity(0.f, 0.f);      // 0, 200
    SetCustomPhysicsWorld(gravity);

    button.push_back(new Button());
    button.back()->SetName("FullScreen");
    button.back()->transform.position.Set(-450.0f, -250.0f, 0.0f);
    button.back()->transform.SetScale(80.0f, 80.0f);
    if (Application::setting.isFullscreen)
    {
        button.back()->sprite.LoadImage("texture/On.png", m_renderer);
    }
    else
    {
        button.back()->sprite.LoadImage("texture/Off.png", m_renderer);
    }
    button.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    button.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    button.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &button.back()->transform);
    button.back()->SetColor(SDL_Color{ 0xff, 0xff, 0xff, 0xff });
    button.back()->SetData();
    button.back()->SetWorld(this, &camera);
    button.back()->SetClickEvent(Support::Event::ToggleFullScreen, button.back());

    fullscrean_text.SetName("fullscrean_text");
    fullscrean_text.transform.position.Set(-450.0f, -150.0f, 0.0f);
    fullscrean_text.transform.SetScale(320.0f, 60.0f);
    fullscrean_text.sprite.color = SDL_Color{ 255, 255, 255, 255 };
    fullscrean_text.sprite.LoadImage("texture/FULLSCREEN.png", m_renderer);
    fullscrean_text.IsPhysics = false;

    button.push_back(new Button());
    button.back()->SetName("VolumeBar");
    button.back()->transform.position.Set(0.0f, -250.0f, 1.0f);
    button.back()->transform.SetScale(40.0f, 80.0f);
    button.back()->sprite.LoadImage("texture/rect.png", m_renderer);
    button.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    button.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    button.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &button.back()->transform);
    button.back()->SetDragAndDropLimit(10.0f);
    button.back()->SetType(ButtonType::DRAG_DROP);
    button.back()->SetColor(SDL_Color{ 255, 0, 0, 0xff });
    button.back()->SetData();
    button.back()->SetStore(&Application::setting.volume, b2Vec2(0.0f, 128.0f));
    button.back()->SetWorld(this, &camera);

    volume_text.SetName("volume_text");
    volume_text.transform.position.Set(0.0f, -150.0f, 0.0f);
    volume_text.transform.SetScale(320.0f, 40.0f);
    volume_text.sprite.color = SDL_Color{ 255, 255, 255, 255 };
    volume_text.sprite.LoadImage("texture/VOLUME.png", m_renderer);
    volume_text.IsPhysics = false;

    volumebar.SetName("VolumeBar_Long");
    volumebar.transform.position.Set(0.0f, -250.0f, 0.0f);
    volumebar.transform.SetScale(320.0f, 40.0f);
    volumebar.sprite.color = SDL_Color{ 255, 255, 255, 255 };
    volumebar.sprite.LoadImage("texture/rect.png", m_renderer);
    volumebar.IsPhysics = false;

    button.push_back(new Button());
    button.back()->SetName("MuteSounds");
    button.back()->transform.position.Set(450.0f, -250.0f, 0.0f);
    button.back()->transform.SetScale(80.0f, 80.0f);
    if (Application::setting.isMute)
    {
        button.back()->sprite.LoadImage("texture/On.png", m_renderer);
    }
    else
    {
        button.back()->sprite.LoadImage("texture/Off.png", m_renderer);
    }
    button.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    button.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    button.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &button.back()->transform);
    button.back()->SetColor(SDL_Color{ 0xff, 0xff, 0xff, 0xff });
    button.back()->SetData();
    button.back()->SetWorld(this, &camera);
    button.back()->SetClickEvent(Support::Event::ToggleMute, button.back());

    mute_text.SetName("mute_text");
    mute_text.transform.position.Set(450.0f, -150.0f, 0.0f);
    mute_text.transform.SetScale(300.0f, 40.0f);
    mute_text.sprite.color = SDL_Color{ 255, 255, 255, 255 };
    mute_text.sprite.LoadImage("texture/MUTE.png", m_renderer);
    mute_text.IsPhysics = false;

    button.push_back(new Button());
    button.back()->SetName("MainMenu");
    button.back()->transform.position.Set(250.0f, -40.0f, 0.0f);
    button.back()->transform.SetScale(150.0f, 150.0f);
    button.back()->sprite.LoadImage("texture/MainMenu.png", m_renderer);
    button.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    button.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    button.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &button.back()->transform);
    button.back()->SetColor(SDL_Color{ 0xff, 0xff, 0x00, 0xff });
    button.back()->SetData();
    button.back()->SetWorld(this, &camera);
    button.back()->SetClickEvent(Support::Event::ResumeAndMainMenuEvent, this);
    button.back()->sprite.isHud = true;

    button.push_back(new Button());
    button.back()->SetName("Back1");
    button.back()->transform.position.Set(-250.0f, -40.0f, 0.0f);
    button.back()->transform.SetScale(80.0f, 80.0f);
    button.back()->sprite.LoadImage("texture/BACK.png", m_renderer);
    button.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    button.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    button.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &button.back()->transform);
    button.back()->SetColor(SDL_Color{ 0xff, 0xff, 0x00, 0xff });
    button.back()->SetData();
    button.back()->SetWorld(this, &camera);
    button.back()->SetClickEvent(Support::Event::ResumeEvent, this);
    button.back()->sprite.isHud = true;

    button.push_back(new Button());
    button.back()->SetName("HowToPlay");
    button.back()->transform.position.Set(0.0f, -40.0f, 0.0f);
    button.back()->transform.SetScale(80.0f, 80.0f);
    button.back()->sprite.LoadImage("texture/HowToPlayButton.png", m_renderer);
    button.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    button.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    button.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &button.back()->transform);
    button.back()->SetColor(SDL_Color{ 0xff, 0xff, 0, 0xff });
    button.back()->SetWorld(this, &camera);
    button.back()->SetClickEvent(Support::Event::Option_HowToPlay_Click, this);
    button.back()->SetData();
    button.back()->sprite.isHud = true;

    button.push_back(new Button());
    button.back()->SetName("Back2");
    button.back()->transform.position.Set(520.0f, -330.0f, 0.0f);
    button.back()->transform.SetScale(150.0f, 50.0f);
    button.back()->sprite.LoadImage("texture/BACK.png", m_renderer);
    button.back()->customPhysics.bodyType = CustomPhysics::STATIC;
    button.back()->customPhysics.bodyShape = CustomPhysics::BOX;
    button.back()->customPhysics.GenerateBody(GetCustomPhysicsWorld(), &button.back()->transform);
    button.back()->SetColor(SDL_Color{ 255,255,0,0 });
    button.back()->SetWorld(this, &camera);
    button.back()->SetClickEvent(Support::Event::MakeInvisible_BUTTON, button.back());
    button.back()->SetData();
    button.back()->sprite.isHud = true;

    wallPaper.SetName("WallPaper");
    wallPaper.transform.position.Set(0.0f, 0.0f, 0.0f);
    wallPaper.transform.SetScale(m_width, m_height);
    wallPaper.sprite.color = SDL_Color{ 255, 255, 255, 255 };
    wallPaper.sprite.LoadImage("texture/TitleImage.png", m_renderer);
    wallPaper.IsPhysics = false;
    //WallPaper.sprite.isHud = true;

    howtoplay_print.SetName("howtoplay");
    howtoplay_print.transform.position.Set(-200.0f, 0.0f, 0.0f);
    howtoplay_print.transform.SetScale(static_cast<float>(m_width / 1.5), static_cast<float>(m_height));
    howtoplay_print.sprite.color = SDL_Color{ 255, 255, 255, 0 };
    howtoplay_print.sprite.LoadImage("texture/HowToPlay.png", m_renderer);
    howtoplay_print.IsPhysics = false;

    AddObject(&wallPaper);
    AddObject(&volumebar);
    AddObject(&fullscrean_text);
    AddObject(&mute_text);
    AddObject(&volume_text);

    for (auto i : button)
    {
        AddObject(i);
        AddCustomPhysicsComponent(i);
    }

    AddObject(&blank_paper);
    AddObject(&howtoplay_print);

    Cursor = Support::MakeCursor(this);
}

void Option::Update(float dt)
{
    Support::UpdateCursor(this, Cursor);

    if (PrintHowToPlay)
    {
        button.at(3)->sprite.color.a = 0;
        button.at(4)->sprite.color.a = 0;
        button.at(5)->sprite.color.a = 0;

        button.at(6)->Color.a = 255;

        howtoplay_print.sprite.color.a = 255;
        blank_paper.sprite.color.a = 255;
    }
    else
    {
        button.at(3)->sprite.color.a = 255;
        button.at(4)->sprite.color.a = 255;
        button.at(5)->sprite.color.a = 255;

        howtoplay_print.sprite.color.a = 0;
        blank_paper.sprite.color.a = 0;
    }

    if (Application::setting.isMute)
    {
        backgroundMusic.SetVolume(0);
    }
    else
    {
        backgroundMusic.SetVolume(Application::setting.volume);
    }

    if (State::m_input->IsTriggered(SDL_SCANCODE_A))
    {
        Application::setting.volume = 128;
        backgroundMusic.SetVolume(Application::setting.volume);
    }
    if (State::m_input->IsTriggered(SDL_SCANCODE_S))
    {
        Application::setting.volume = 64;
        backgroundMusic.SetVolume(Application::setting.volume);
    }
    if (State::m_input->IsTriggered(SDL_SCANCODE_D))
    {
        Application::setting.volume = 0;
        backgroundMusic.SetVolume(Application::setting.volume);
    }

    for (auto i : button)
    {
        if (i->GetName() == "Back2")
        {
            if (PrintHowToPlay)
            {
                i->Update(dt);
            }
        }

        if(!PrintHowToPlay)
        {
            i->Update(dt);
        }
    }

    // Must be one of the last functions called at the end of State Update
    UpdateCustomPhysics(dt);	// Update custom physics system here
    Render(dt);

    DealTheResume();
}

void Option::Close()
{
    // Deallocate custom physics world
    RemoveCustomPhysicsWorld();

    // Wrap up state
    ClearBaseState(false);

    for (auto i : button)
    {
        delete i;
        i = NULL;
    }
    button.clear();

    delete Cursor;
}

void Option::DelayForResume()
{
    ShouldResume = true;
}

void Option::DelayForMainMenu()
{
    ShouldGoMainMenu = true;
}

void Option::DealTheResume()
{
    if(ShouldResume)
    {
        m_game->Resume();
        ShouldResume = false;
    }
    if(ShouldGoMainMenu)
    {
        m_game->Change(LV_MainMenu);
        ShouldGoMainMenu = false;
    }
}
