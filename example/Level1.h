/******************************************************************************/
/*!
\file   Level1.h
\project ColorLeon
\author primary : Kim Hyunseok

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once

#include "CommonLevel.h"

class Rope;

class Level_1 : public State
{
    friend class Game;

public:

    SDL_Rect rect;


protected:

    Level_1() : State() {};
    ~Level_1() override {};

	// Derived initialize function
	void Initialize() override;
	// Derived Update function
	void Update(float dt) override;
	// Derived Close function
	void Close() override;


private:

    // Objects which contains custom physics
    CustomCollision myCustomCollision1;

    Player *				player;
    std::vector<CustomBaseObject*>	Object;

    CustomBaseObject * Cursor;
};