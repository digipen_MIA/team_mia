/******************************************************************************/
/*!
\file   SettingLevel.h
\project ColorLeon
\author primary : Kim Minsuk, secondary : Choi Jinhyun

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include "engine\State.h"
#include "CustomBaseObject.h"
#include "Player.h"
#include "CustomCollision.h"

class SettingLevel : public State
{
    friend class Game;

public:
    SDL_Rect rect;

protected:
    SettingLevel() : State() {};
    ~SettingLevel() override {};

    // Derived initialize function
    void Initialize() override;
    // Derived Update function
    void Update(float dt) override;
    // Derived Close function
    void Close() override;
private:
    // Objects which contains custom physics
    CustomBaseObject	    wallPaper;
    CustomBaseObject        volumebar;

    CustomBaseObject        fullscrean_text;
    CustomBaseObject        mute_text;
    CustomBaseObject        volume_text;

    std::vector<Button *>   button;

    CustomBaseObject * Cursor;
};
