/******************************************************************************/
/*!
\file   Physics.h
\author Juyong Jeong
\par    email: jeykop14\@digipen.edu
\par    GAM150 demo
\date   2018/03/17

Containing object's velocity, direction, gravity.
This component can be editted by students
*/
/******************************************************************************/
#pragma once
#include "Box2D/Box2D.h"

class Transform;
class b2World;

class Physics {

	friend class State;

private:
	// Box2D Info
	b2FixtureDef	m_fixtureDef;
        b2CircleShape   m_bodyBall;
	b2PolygonShape	m_bodyBox;
	b2BodyDef	m_bodyType;
	b2Body		*m_body = nullptr;

	// Physics info
	b2Vec2	m_velocity;
	bool	m_active = true;
        float   m_density;

public:

        enum BodyShape  { CIRCLE, BOX };
	enum BodyType   { STATIC, DYNAMIC };

        BodyShape       bodyShape = BOX;        // Either box or ball
        BodyType	bodyType = DYNAMIC;	// Either dynamic or static
	Transform	*pTransform = nullptr;	// Pointer to transform component
        float           radius = 1.f;

        Physics();
        ~Physics();

	// Helper functions
	// Initialize the physics info 
	void AllocateBody(b2World* world);
        // Decallocate the body
        void Free();
	// Set velocity with floats
	void SetVelocity(float x, float y);
	// Set velocity with vec2
	void SetVelocity(const b2Vec2& vel);
	// Get velocity as vec2
	const b2Vec2& GetVelocity() const;
        // Set density 
        void SetDensity(float dens);
        // Get density
        float GetDensity();
        // Set restitution
        void SetRestitution(float res);
        // Get restitution
        float GetRestitution();
	// Set physics toggle (either let it work or not)
	void ActivePhysics(bool toggle);
	// Get toggle
	bool IsActive() const;
	
};