/******************************************************************************/
/*!
\file   EditLevel.h
\project ColorLeon
\author primary : Kim Minsuk, secondary : Kim Hyunseong

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once
#include <engine/State.h>
#include "CustomBaseObject.h"
#include "Button.h"
#include "CustomCollision.h"

enum CurrentState {
    Level_Select = 1,
    Level
};

class EditLevel : public State {
    friend class Game;

public:
    int HowManyObject[10] = { 0 };
    SDL_Rect rect;
    CurrentState State = Level_Select;
    int LevelNumber = 1;
    static int Check_Level_number;

protected:
    EditLevel() : State() {};
    ~EditLevel() override {};

    void SetRopeLength(float32 length);
    float32 GetRopeLength();

    // Derived initialize function
    void Initialize() override;
    // Derived Update function
    void Update(float dt) override;
    // Derived Close function
    void Close() override;
    void MakeButton(ButtonType type, ObjectType objectttype);

    void SaveFromFile();
    void LoadFromFile();

private:
    std::vector<Button *> button;
    float32 ropeLength;

    CustomBaseObject * Cursor;
};
