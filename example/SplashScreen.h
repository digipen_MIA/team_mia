/******************************************************************************/
/*!
\file   SplashScreen.h
\project ColorLeon
\author primary : Choi Jinhyun

All content � 2018 DigiPen (USA) Corporation, all rights reserved.
*/
/******************************************************************************/
#pragma once

#include <engine/State.h>
#include "CustomBaseObject.h"

class SplashScreen : public State
{
    friend class Game;

public:
    SplashScreen() : State() {};
    ~SplashScreen() override {};

    void Initialize() override;
    void Update(float dt) override;
    void Close() override;

private:
    CustomBaseObject pictureObject;
    CustomBaseObject MIA_picture;
    float frame_count_A = 0;
    float frame_count = 0;

    CustomBaseObject * Cursor;
};
